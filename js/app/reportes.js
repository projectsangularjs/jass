'use strict';
var app = angular.module('app',
        [
        'ui.router',
        'oc.lazyLoad',
        'ngResource',
        'ngAnimate',
        'ngAria',
        'ngSanitize',
        'LocalStorageModule',
    ]);
