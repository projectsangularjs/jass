angular.module('app').config(function ($stateProvider, $locationProvider, $urlRouterProvider, $ocLazyLoadProvider) {
    $urlRouterProvider.otherwise("/app");
//            $locationProvider.hashPrefix('!');
    $stateProvider
            .state('app', {
                url: '/app',
                data: {page: 'Sistema JASS'},
                views: {
                    '': {
                        templateUrl: 'views/layout.html'
                    },
                    'aside': {
                        templateUrl: 'views/aside.html',
                        controller: 'AsideCtrl'
                    },
                    'content': {
                        templateUrl: 'views/content.html',
                        controller: 'AppCtrl'
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/AppCtrl.js');
                    }]
                }
            })
            .state("app.perfil", {
                url: "/perfil",
                data: {section: 'App', page: 'Perfil'},
                templateUrl: "views/perfil/index.html",
                controller: 'Perfil',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load(
                            ['js/controllers/app.perfil.js','recursos/styles/perfil.css']
                            );
                    }]
                }
            })

            .state("app.continentes", {
                url: "/continentes",
                data: {section: 'App', page: 'Continentes'},
                templateUrl: "views/continentes/index.html",
                controller: 'Continentes',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.continentes.js');
                    }]
                }
            })
            .state("app.paises", {
                url: "/paises",
                data: {section: 'App', page: 'Países'},
                templateUrl: "views/paises/index.html",
                controller: 'Paises',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.paises.js');
                    }]
                }
            })
            .state("app.personas", {
                url: "/personas",
                data: {section: 'App', page: 'Personas'},
                templateUrl: "views/personas/index.html",
                controller: 'Personas',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.personas.js');
                    }]
                }
            })
            .state("app.tiposentidad", {
                url: "/tiposentidad",
                data: {section: 'App', page: 'Tipos de entidad'},
                templateUrl: "views/tiposentidad/index.html",
                controller: 'TiposEntidad',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.tiposentidad.js');
                    }]
                }
            })
            .state("app.entidades", {
                url: "/entidades",
                data: {section: 'App', page: 'Entidades'},
                templateUrl: "views/entidades/index.html",
                controller: 'Entidades',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.entidades.js');
                    }]
                }
            })
            .state("login", {
                url: "/login",
                data: {section: 'App', page: 'Acceso al sistema'},
                templateUrl: "views/seguridad/login/index.html",
                controller: 'Login',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/pages/login.js');
                    }]
                }
            }).state("app.monedas", {
                url: "/monedas",
                data: {section: 'App', page: 'Tipos de moneda'},
                templateUrl: "views/monedas/index.html",
                controller: 'Monedas',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.monedas.js');
                    }]
                }
            }).state("app.ubigeos", {
                url: "/ubigeos",
                data: {section: 'App', page: 'Ubigeos'},
                templateUrl: "views/ubigeos/index.html",
                controller: 'Ubigeos',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.ubigeos.js');
                    }]
                }
            }).state("app.permisos", {
                url: "/permisos",
                data: {section: 'App', page: 'Permisos'},
                templateUrl: "views/permisos/index.html",
                controller: 'Permisos',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/seguridad/app.permisos.js');
                    }]
                }
            }).state("app.roles", {
                url: "/roles",
                data: {section: 'App', page: 'Roles'},
                templateUrl: "views/roles/index.html",
                controller: 'Roles',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/seguridad/app.roles.js');
                    }]
                }
            }).state("app.users", {
                url: "/users",
                data: {section: 'App', page: 'Users'},
                templateUrl: "views/users/index.html",
                controller: 'Users',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/seguridad/app.users.js');
                    }]
                }
            }).state("app.modulos", {
                url: "/modulos",
                data: {section: 'App', page: 'Módulos'},
                templateUrl: "views/modulos/index.html",
                controller: 'Modulos',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/seguridad/app.modulos.js');
                    }]
                }
            }).state("app.tiposmenu", {
                url: "/tiposmenu",
                data: {section: 'App', page: 'Tipos de menú'},
                templateUrl: "views/tiposmenu/index.html",
                controller: 'Tiposmenu',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/seguridad/app.tiposmenu.js');
                    }]
                }
            }).state("app.menus", {
                url: "/menus",
                data: {section: 'App', page: 'Tipos de menú'},
                templateUrl: "views/menus/index.html",
                controller: 'Menus',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/seguridad/app.menus.js');
                    }]
                }
            }).state("app.unidadesmedida", {
                url: "/unidadesmedida",
                data: {section: 'App', page: 'Unidades de medida'},
                templateUrl: "views/unidadesmedida/index.html",
                controller: 'Unidadesmedida',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.unidadesmedida.js');
                    }]
                }
            }).state("app.unidades", {
                url: "/unidades",
                data: {section: 'App', page: 'Unidades'},
                templateUrl: "views/unidades/index.html",
                controller: 'Unidades',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.unidades.js');
                    }]
                }
            }).state("app.productosservicio", {
                url: "/productosservicio",
                data: {section: 'App', page: 'Productos y servicios'},
                templateUrl: "views/productosservicio/index.html",
                controller: 'Productosservicio',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.productosservicio.js');
                    }]
                }
            }).state("app.productos", {
                url: "/productos",
                data: {section: 'App', page: 'Productos'},
                templateUrl: "views/productos/index.html",
                controller: 'Productos',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.productos.js');
                    }]
                }
            }).state("app.clasificacionesiiu", {
                url: "/clasificacionesiiu",
                data: {section: 'App', page: 'Clasificaciones iiu'},
                templateUrl: "views/clasificacionesiiu/index.html",
                controller: 'ClasificacionesIiu',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.clasificacionesiiu.js');
                    }]
                }
            }).state("app.servicios", {
                url: "/servicios",
                data: {section: 'App', page: 'Servicios'},
                templateUrl: "views/servicios/index.html",
                controller: 'Servicio',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.servicios.js');
                    }]
                }
            }).state("app.zonas", {
                url: "/zonas",
                data: {section: 'App', page: 'Zonas'},
                templateUrl: "views/zonas/index.html",
                controller: 'Zonas',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.zonas.js');
                    }]
                }
            }).state("app.predios", {
                url: "/predios",
                data: {section: 'App', page: 'Predios'},
                templateUrl: "views/predios/index.html",
                controller: 'Predios',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.predios.js');
                    }]
                }
            }).state("app.comprobantes", {
                url: "/comprobantes",
                data: {section: 'App', page: 'comprobantes'},
                templateUrl: "views/comprobantes/index.html",
                controller: 'Comprobantes',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.comprobantes.js');
                    }]
                }
            }).state("app.comprobantesconfig", {
                url: "/comprobantesconfig",
                data: {section: 'App', page: 'comprobantesconfig'},
                templateUrl: "views/comprobantesconfig/index.html",
                controller: 'Comprobantesconfig',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.comprobantesconfig.js');
                    }]
                }
            }).state("app.motivosmovimientocuenta", {
                url: "/motivosmovimientocuenta",
                data: {section: 'App', page: 'Motivos de movimiento'},
                templateUrl: "views/motivosmovimientocuenta/index.html",
                controller: 'Motivosmovimientocuenta',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.motivosmovimientocuenta.js');
                    }]
                }
            }).state("app.cronogramascobranza", {
                url: "/cronogramascobranza",
                data: {section: 'App', page: 'Cronogramas de Cobranza'},
                templateUrl: "views/cronogramascobranza/index.html",
                controller: 'Cronogramascobranza',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.cronogramascobranza.js');
                    }]
                }
            })
            .state("app.contratos", {
                url: "/contratos",
                data: {section: 'App', page: 'Contratos'},
                templateUrl: "views/contratos/index.html",
                controller: 'Contratos',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.contratos.js');
                    }]
                }
            }).state("app.contrato", {
                url: "/contrato",
                data: {section: 'App', page: 'Contrato'},
                params: { id: null },
                templateUrl: "views/contratos/contrato.html",
                controller: 'Contrato',
                controllerAs: "form",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.contrato.js');
                    }]
                }
            }).state("app.addcontrato", {
                url: "/addcontrato",
                data: {section: 'App', page: 'Registrar contrato'},
                templateUrl: "views/contratos/contrato/index.html",
                controller: 'ContratoIndex',
                controllerAs: "inde",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load(
                            ['js/controllers/contrato/app.contrato.index.js']);
                    }]
                }
            }).state("app.addcontrato.registro", {
                url: "/registro",
                data: {section: 'App', page: 'Registrar contrato'},
                templateUrl: "views/contratos/contrato/registro.html",
                controller: 'ContratoRegistro',
                controllerAs: "form",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load(
                            ['js/controllers/contrato/app.contrato.registro.js']);
                    }]
                }
            }).state("app.addcontrato.pago", {
                url: "/pago/:id",
                data: {section: 'App', page: 'Pagar contrato'},
                params: { id: null },
                templateUrl: "views/contratos/contrato/pago.html",
                controller: 'ContratoPago',
                controllerAs: "form",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load(
                            ['js/controllers/contrato/app.contrato.pago.js']);
                    }]
                }
            }).state("app.addcontrato.documento", {
                url: "/documento/:id",
                data: {section: 'App', page: 'Contrato documento'},
                params: { id: null },
                templateUrl: "views/contratos/contrato/documento.html",
                controller: 'ContratoDocumento',
                controllerAs: "form",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load(
                            ['js/controllers/contrato/app.contrato.documento.js']);
                    }]
                }
            }).state("app.recibos", {
                url: "/recibos",
                data: {section: 'App', page: 'Recibos'},
                params: { id: null },
                templateUrl: "views/recibos/index.html",
                controller: 'Recibos',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.recibos.js');
                    }]
                }
            }).state("app.caja", {
                url: "/caja",
                data: { section: 'Caja'},
                template: '<div ui-view></div>'
            })
            .state("app.caja.registropagos", {
                url: "/registropagos",
                data: {section: 'Caja', page: 'Registro de pagos'},
                templateUrl: "views/caja/movimientoscuenta/registropagos.html",
                controller: 'CajaRegistropagos',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/caja/caja.registropagos.js');
                    }]
                }
            })
            .state("app.caja.registrooperacion", {
                url: "/registrooperacion",
                data: {section: 'Caja', page: 'Registrar operación'},
                templateUrl: "views/caja/registrooperacion/index.html",
                controller: 'CajaRegistrooperacion',
                controllerAs: "form",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/caja/caja.registrooperacion.js');
                    }]
                }
            })
            .state("app.caja.anularpagos", {
                url: "/anularpagos",
                data: {section: 'Caja', page: 'Anular pagos'},
                templateUrl: "views/caja/movimientoscuenta/anularpagos.html",
                controller: 'CajaAnularpagos',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/caja/caja.anularpagos.js');
                    }]
                }
            }).state("app.caja.misoperaciones", {
                url: "/misoperaciones",
                data: {section: 'Caja', page: 'Mis operaciones en caja'},
                templateUrl: "views/caja/misoperaciones/index.html",
                controller: 'CajaMisoperaciones',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/caja/caja.misoperaciones.js');
                    }]
                }
            }).state("app.caja.operacionesentidad", {
                url: "/operacionesentidad",
                data: {section: 'Caja', page: 'Operaciones entidad'},
                templateUrl: "views/caja/operacionesentidad/index.html",
                controller: 'CajaOperacionesentidad',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/caja/caja.operacionesentidad.js');
                    }]
                }
            }).state("app.cronogramaspago", {
                url: "/cronogramaspago",
                data: {section: 'App', page: 'Cronogramas de Pago'},
                templateUrl: "views/cronogramaspago/index.html",
                controller: 'cronogramaspago',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.cronogramaspago.js');
                    }]
                }

            }).state("app.caja.cuentasentidad", {
                url: "/cuentasentidad",
                data: {section: 'Caja', page: 'Cuentas de entidad'},
                templateUrl: "views/caja/cuentasentidad/index.html",
                controller: 'CajaCuentasentidad',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/caja/caja.cuentasentidad.js');
                    }]
                }
            }).state("app.cargosentidad", {
                url: "/cargosentidad",
                data: {section: 'Caja', page: 'Cargos en Entidad'},
                templateUrl: "views/cargosentidad/index.html",
                controller: 'CargosEntidad',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.cargosentidad.js');
                    }]
                }
            }).state("app.estadoscuenta", {
                url: "/estadoscuenta",
                data: {section: 'Reporte', page: 'Estados de cuenta'},
                templateUrl: "views/estadoscuenta/index.html",
                controller: 'EstadosCuenta',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.estadoscuenta.js');
                        }]
                }
            }).state("app.afiliaciones", {
                url: "/afiliaciones",
                data: {section: 'App', page: 'Afiliaciones'},
                templateUrl: "views/Afiliaciones/index.html",
                controller: 'afiliaciones',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.afiliaciones.js');
                    }]
                }
            }).state("app.periodos", {
                url: "/periodos",
                data: {section: 'App', page: 'Periodos'},
                templateUrl: "views/periodos/index.html",
                controller: 'Periodos',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.periodos.js');
                    }]
                }
            }).state("app.misrecibos", {
                url: "/misrecibos",
                data: {section: 'App', page: 'Mis recibos'},
                templateUrl: "views/recibos/misrecibos.html",
                controller: 'Misrecibos',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.recibos.misrecibos.js');
                         }]
                }
            }).state("app.eventos", {
                url: "/eventos",
                data: {section: 'App', page: 'Eventos'},
                templateUrl: "views/eventos/index.html",
                controller: 'Eventos',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/app.eventos.js');
                    }]
                }
            }).state("app.asistenciasevento", {
                url: "/evento/asistencias/:id",
                data: {section: 'App', page: 'Vista asistencia'},
                params: { id: null },
                /*params: {evento:null}, enviar objeto*/
                templateUrl: "views/eventos/asistencias/asistenciaevento.html",
                controller: 'AsistenciaEvento',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load(
                            ['js/controllers/asistencias/app.asistenciaevento.js']);
                    }]
                }
            }).state("app.asistencias", {
                url: "/asistencias",
                data: {section: 'Reporte', page: 'Asistencias'},
                templateUrl: "views/asistencias/index.html",
                controller: 'Asistencias',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/asistencias/app.asistencias.js');
                        }]
                    }
            }).state("app.reportescontratos", {
                url: "/reportescontratos",
                data: {section: 'App', page: 'Contratos'},
                templateUrl: "views/contratos/reporte.html",
                controller: 'Contratosreporte',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load(
                            ['js/controllers/app.contratos.reporte.js']);
                    }]
                }
            });
    $locationProvider.html5Mode(false);
});
