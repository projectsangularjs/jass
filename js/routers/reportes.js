angular.module('app').config(function ($stateProvider, $locationProvider, $urlRouterProvider, $ocLazyLoadProvider) {
    $urlRouterProvider.otherwise("/app");
//            $locationProvider.hashPrefix('!');
    $stateProvider
            .state('app', {
                url: '/app',
                template: '<div ui-view></div>',
                data: {page: 'REPORTES'},
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/AppCtrlReportes.js');
                    }]
                }
            })
            .state("app.reportes", {
                url: "/reportes",
                data: {section: 'App', page: 'Reportes'},
                templateUrl: "views/reportes/index.html"
            })
            .state("app.operacionesentidad", {
                url: "/operacionesentidad/:fecha_inicial/:fecha_final",
                data: {section: 'Entradas', page: 'Operaciones entidad'},
                params: {fecha_inicial: null, fecha_final: null},
                templateUrl: "views/reportes/operacionesentidad.html",
                controller: 'Operacionesentidad',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/reportes/app.operacionesentidad.js');
                    }]
                }
            }).state("app.401", {
                url: "/401",
                data: {section: 'Reportes', page: '401'},
                params: {id: null},
                templateUrl: "views/reportes/401.html",
            }).state("app.programacionoperativa", {
                url: "/programacionoperativa/:id",
                data: {section: 'Reportes', page: 'Programacion Operativa'},
                params: {id: null},
                templateUrl: "views/reportes/programacionoperativa.html",
                controller: 'Programacionoperativa',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/reportes/app.programacionoperativa.js');
                    }]
                }
            }).state("app.asignacionrecursos", {
                url: "/asignacionrecursos/:id",
                data: {section: 'Reportes', page: 'Asignación de recursos'},
                params: {id: null},
                templateUrl: "views/reportes/asignacionrecursos.html",
                controller: 'Asignacionrecursos',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/reportes/app.asignacionrecursos.js');
                    }]
                }
            });


    $locationProvider.html5Mode(false);
});
