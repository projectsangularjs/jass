angular.module('app').config(function ($stateProvider, $locationProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/login");
    $stateProvider
            .state("login", {
                url: "/login",
                data: {section: 'App', page: 'Acceso al sistema'},
                templateUrl: "views/seguridad/login/index.html",
                controller: 'Login',
                controllerAs: "ctrl",
                resolve: {
                    loadFormatosCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/pages/login.js');
                    }]
                }
            });

    $locationProvider.html5Mode(false);
});
