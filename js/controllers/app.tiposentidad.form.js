angular.module('app').controller('TiposentidadForm', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;

    if (obj){
       CONEX('tiposentidad').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r['data'];
            self.cargado = true;
        }, function (err) {
        }); 
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.guardando = true;
        if(self.obj.id){
            CONEX('tiposentidad').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r.data){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r.data);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('tiposentidad').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r.data['id']){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r.data);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

});