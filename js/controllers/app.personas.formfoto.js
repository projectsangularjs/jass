angular.module('app').controller('PersonasFormfoto', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.obj['hacer'] = "Agregar";
    self.cargado = true;
    self.guardando = false;

    // if (obj){
    //    CONEX('entidades/edit').GET({id: obj['id']}).$promise.then(function (r) {
    //         self.obj = r['data'];
    //         self.cargado = true;
    //     }, function (err) {
    //     }); 
    // }else{
    //     self.obj['estado'] = '1';
    //     self.cargado = true;
    // }

    function dataURItoBlob(dataURI) {
      var byteString = atob(dataURI.split(',')[1]);
      var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
      var ab = new ArrayBuffer(byteString.length);
      var ia = new Uint8Array(ab);
      for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }

      var blob = new Blob([ab], { type: mimeString });
      return blob;
    }

    self.save_or_update = function () {
        self.guardando = true;
        self.obj['persona_id'] = self.obj_['id'];            

        if(self.obj.hacer === "Agregar"){
            var imageBase64 = self.cropper.croppedImage;
            if(imageBase64){
                var blob = dataURItoBlob(imageBase64);
                var image = new File([blob], 'image.png')

                self.obj['logo_new'] = image;

                CONEXP('comun','personas/savefoto').POST_FILE(self.obj).$promise.then(function (r) {
                    self.guardando = false;
                    if(r){
                        toastr.success("Registrado correctamente","Subido/a");
                         $mdDialog.hide(r);
                    }
                }, function (err) {
                    self.guardando = false;
                    toastr.error(err.data['message'],"Error");
                });
            }else{
                toastr.error("Debe seleccionar antes una imagen","Error");
                self.guardando = false;
            }
        }else{
            CONEXP('comun','personas/deletefoto').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Eliminado correctamente","Correcto");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error(err.data['message'],"Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.cropper = {};
        self.cropper.sourceImage = null;
        self.cropper.croppedImage   = null;
        self.bounds = {};
        self.bounds.left = 0;
        self.bounds.right = 0;
        self.bounds.top = 0;
        self.bounds.bottom = 0;

    
});