angular.module('app').controller('EstadosCuentaMovimiento', function (persona, $scope, $mdDialog,
    toastr, CONEXP, localStorageService) {
    var self = this;
    self.cargado = false;
    var entidad_id = localStorageService.get('limon_entidad');

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.consultar = function(obj){
    	console.log("NUEVOS CAMBIOS: ", obj);
    };

    self.listado = [];
    self.total_pago = 0;
    self.total_cronog = 0;
    self.persona = persona;
    self.estcue = {};

    self.list_estado_cuenta = function () {
    	self.cargado = false;
        var dat = { 'persona_id':persona['id'], 'entidad_id': entidad_id};
        CONEXP('caja','estadoscuenta/reportmovimientos').GET(dat).$promise.then(function (r) {
            self.listado = r['listado'];
            self.total_pago = r['total_pago'];
    		self.total_cronog = r['total_cronog'];
    		self.cargado = true;
    		self.estcue = r['estado_cuenta'];
        }, function (err) {
        	self.cargado = true;
        });
    };

    self.list_estado_cuenta();



});
