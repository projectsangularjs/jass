
angular.module('app').controller('PersonasForm', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP, $ocLazyLoad, localStorageService) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;
    self.tipospersona = [];
    self.sexos = [];
    self.tiposcontribuyente = [];
    self.tiposfecha = [];
    self.codigo_per_nat = '01';
    self.codigo_per_jur = '02';
    self.cargo_per_nat = false;
    self.cargo_per_jur = false;

    self.meses = [];
    self.dias = [];
    self.tiposdocumento = [];
    self.list_documentos = [];
    self.list_direcciones = [];
    self.tiposdireccion = [];
    self.tiposvia = [];
    self.tiposnumerovia = [];
    self.tiposzona = [];
    self.list_contactos = [];
    self.tiposcontacto = [];
    self.list_profesionesoficio = [];
    self.tiposambito = [];
    self.list_actividades = [];
    self.list_cuentas = [];

    var entidad_id = localStorageService.get('limon_entidad');


    self.add_documento = function(){
        self.list_documentos.push({});
    };

    self.add_direccion = function(){
        self.list_direcciones.push({});
    };

    self.add_contacto = function(){
        self.list_contactos.push({});
    };

    self.add_profesionoficio = function(){
        self.list_profesionesoficio.push({});
    };

    self.add_actividad = function(){
        self.list_actividades.push({});
    };

    self.add_cuenta = function(){
        self.list_cuentas.push({});
    };

    if (obj){
       CONEXP('comun','personas/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r['data_person'];
            self.list_documentos = r['documentos'];
            self.list_direcciones = r['direcciones'];
            self.list_contactos = r['contactos'];
            self.list_profesionesoficio = r['profesionesoficio'];
            self.list_actividades = r['actividades'];
            self.list_cuentas = r['cuentas'];

            self.cargar_inicial(r['data_person']['tipo_persona_id']);
            self.cargado = true;
        }, function (err) {
        });
    }else{
        self.obj['estado'] = '1';
    }

    self.list_tipospersona = function () {
        CONEXP('comun','tipospersona/searchform').GET_LIST({f:{estado:"1"}}).$promise.then(function (r) {
            self.tipospersona = r;
            if(!obj && r.length > 0){
                self.cargar_inicial(r[0]['codigo']);
                self.cargado = true;
            }

        }, function (err) {
        });
    };
    self.list_tipospersona();

    self.list_sexos = function () {
        CONEXP('comun','choices/sexos').GET_LIST().$promise.then(function (r) {
            self.sexos = r;
        }, function (err) {
        });
    };

    self.list_tiposcontribuyente = function () {
        var dat = { 'id':'01'};
        CONEXP('comun','tiposcontribuyente/tipopersona').GET_LIST(dat).$promise.then(function (r) {
            self.tiposcontribuyente = r;
        }, function (err) {
        });
    };

    self.list_tiposcontribuyente_pj = function () {
        var dat = { 'id':'02'};
        CONEXP('comun','tiposcontribuyente/tipopersona').GET_LIST(dat).$promise.then(function (r) {
            self.tiposcontribuyente = r;
        }, function (err) {
        });
    };


    self.list_tiposfecha = function (tipo_persona) {
        var dat = {limit:'10',ordering:'orden',
        f:{"tipo_persona__codigo":tipo_persona,'estado':'1'}};
        CONEXP('comun','tiposfecha/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposfecha = r;
        }, function (err) {
        });
    };

    self.list_meses = function () {
        var dat = { 'ordering':'id'};
        CONEXP('comun','meses/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.meses = r;
        }, function (err) {
        });
    };
    self.list_meses();

    self.list_dias = function () {
        CONEXP('comun','choices/dias').GET_LIST().$promise.then(function (r) {
            self.dias = r;
        }, function (err) {
        });
    };
    self.list_dias();

    self.list_tiposdocumento = function (tipo_persona) {
        var dat = {'ordering':'orden',
        'f':{'config_documento_tipo_persona_tipo_direcciones__tipo_persona__codigo':tipo_persona}};
        CONEXP('comun','tiposdocumentos/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposdocumento = r;
        }, function (err) {
        });
    };

    self.list_tiposdireccion = function (tipo_persona) {
        var dat = { 'ordering':'orden',
        'f':{"tipo_direccion_config_direccion_tipo_persona__tipo_persona__codigo":tipo_persona,
            "estado":"1"}};
        CONEXP('comun','tiposdireccion/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposdireccion = r;
        }, function (err) {
        });
    };

    self.list_entidadfinancieras = function(){
        var dat = { 'ordering':'tipo_ent_finan__concatenar,nombre',
        'f':{"estado":"1"}};
        CONEXP('caja','entidadesfinanciera/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.entidadfinancieras = r;
        }, function (err) {
        });
    };

    self.list_tiposcuentafinan = function(){
        var dat = { 'ordering':'id',
        'f':{"estado":"1"}};
        CONEXP('caja','tiposcuentafinan/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposcuentafinan = r;
        }, function (err) {
        });
    };

    self.change_tipo_doc = function(obj, persona_id){

        if(obj.tipo_documento_id){
            angular.forEach(self.tiposdocumento, function(item) {
                if(item.id ==obj.tipo_documento_id ){
                    obj['patron_angular'] = item['patron_angular'];
                    obj['codigo'] = item['codigo'];
                    self.verificar_nro(obj, persona_id);
                }
            }, console.log);
        }else{
            obj['patron_angular'] = null;
            obj['codigo'] = null;
        }
    };
    self.list_del_documentos = [];
    self.del_documento = function(tiposdocumento, index, item){
        tiposdocumento.splice(index, 1);
        if(item['id']){
            var obj ={ id: item['id']};
            self.list_del_documentos.push(obj);
        };
    };

    self.new_tiposdocumento = function(obj) {

        var arrayNew = [];
        angular.forEach(self.tiposdocumento, function(item) {
            if(item.id == obj.tipo_documento_id){
                arrayNew.push(item);
            }else{
                var index = self.list_documentos.findIndex(x=>x.tipo_documento_id === item.id);

                if(index < 0){
                    arrayNew.push(item);
                }
            }
        }, console.log);
        return arrayNew;
    };

    self.new_tiposdireccion = function(obj) {

        var arrayNew = [];
        angular.forEach(self.tiposdireccion, function(item) {
            if(item.id == obj.tipo_direccion_id){
                arrayNew.push(item);
            }else{
                var index = self.list_direcciones.findIndex(x=>x.tipo_direccion_id === item.id);

                if(index < 0){
                    arrayNew.push(item);
                }
            }
        }, console.log);
        return arrayNew;
    };

    self.list_del_direcciones = [];
    self.del_direccion = function(tiposdireccion, index, item){
        tiposdireccion.splice(index, 1);
        if(item['id']){
            var obj ={ id: item['id']};
            self.list_del_direcciones.push(obj);
        };
    };

    self.list_del_contactos = [];
    self.del_contacto = function(list_contactos, index, item){
        list_contactos.splice(index, 1);
        if(item['id']){
            var obj ={ id: item['id']};
            self.list_del_contactos.push(obj);
        };
    };

    self.list_del_profesionesoficio = [];
    self.del_profesionoficio = function(list_profesionesoficio, index, item){
        list_profesionesoficio.splice(index, 1);
        if(item['id']){
            var obj ={ id: item['id']};
            self.list_del_profesionesoficio.push(obj);
        };
    };

    self.list_del_actividades = [];
    self.del_actividad = function(list_actividades, index, item){
        list_actividades.splice(index, 1);
        if(item['id']){
            var obj ={ id: item['id']};
            self.list_del_actividades.push(obj);
        };
    };

    self.list_del_cuentas = [];
    self.del_cuenta = function(list_cuentas, index, item){
        list_cuentas.splice(index, 1);
        if(item['id']){
            var obj ={ id: item['id']};
            self.list_del_cuentas.push(obj);
        };
    };

    self.verif_RUC = function(event, obj){
        $ocLazyLoad.load([
            'js/controllers/app.personas.verif_ruc.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasVerifRuc",
                controllerAs: "form",
                templateUrl: 'views/personas/verif_ruc.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj},
                multiple:true
            }).then(function (result) {
            }, function () {
            });
        });
    };

    self.add_punto = function(event, obj){
        $ocLazyLoad.load([
            'js/controllers/app.personas.puntodir.js',
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasPuntodir",
                controllerAs: "form",
                templateUrl: 'views/personas/puntodir.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj},
                multiple:true,
                fullscreen: true,
            }).then(function (result) {
                console.log("DATOS: ", result);
                obj['punto_lat'] = result['punto_lat'];
                obj['punto_long'] = result['punto_long'];
            }, function () {
            });
        });
    };

    self.list_tiposvia = function () {
        var dat = {'limit':20};
        CONEXP('comun','tiposvia/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposvia = r;
        }, function (err) {
        });
    };

    self.list_tiposvia();



    self.list_tiposnumerovia = function () {
        var dat = {'limit':50};
        CONEXP('comun','tiposnumerovia/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposnumerovia = r;
        }, function (err) {
        });
    };
    self.list_tiposnumerovia();


    self.list_tiposzona = function () {
        var dat = {'limit':50};
        CONEXP('comun','tiposzona/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposzona = r;
        }, function (err) {
        });
    };
    self.list_tiposzona();

    self.list_tiposcontacto = function () {
        var dat = { 'ordering':'orden',"f":{"estado":"1"}};
        CONEXP('comun','tiposcontacto/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposcontacto = r;
        }, function (err) {
        });
    };
    self.list_tiposcontacto();

    self.change_tipo_contacto = function(obj){
        obj['cuenta_contacto_id'] = null;
        obj['etiqueta_contacto_id'] = null;
        obj.etiquetascontacto = [];
        obj.cuentascontacto = [];
        if(obj.tipo_contacto_id){
            var dat = {ordering:"orden", f:{"estado":"1","tipo_contacto_id":obj.tipo_contacto_id}};
            CONEXP('comun','cuentascontacto/searchform').GET_LIST(dat).$promise.then(function (r) {
                obj.cuentascontacto = r;
            }, function (err) {
            });

            CONEXP('comun','etiquetascontacto/searchform').GET_LIST(dat).$promise.then(function (r) {
                obj.etiquetascontacto = r;
            }, function (err) {
            });

            angular.forEach(self.tiposcontacto, function(item) {
                if(obj.tipo_contacto_id === item.id){
                    obj['tipo_contacto_codigo'] = item['codigo'];
                    obj['denom_valor'] = item['denom_valor'];
                }
            }, console.log);

        }else{
            obj.cuentascontacto = [];
            obj.etiquetascontacto = [];
            obj['tipo_contacto_codigo'] = null;
            obj['denom_valor'] = null;
            obj['tiene_anexo'] = null;
        }
    };

    self.change_etiqueta = function(obj){
        angular.forEach(obj.etiquetascontacto, function(item) {
            if(obj.etiqueta_contacto_id === item.id){
                obj['tiene_cod_ubigeo'] = item['tiene_cod_ubigeo'];
                obj['tiene_anexo'] = item['tiene_anexo'];
            }
        }, console.log);
    };

    self.change_pais_cont = function(obj){
        obj['ubigeo_id'] = null;
        if(obj.pais){
            var data = {ordering:'id',f:{"tipo_ubigeo__codigo":"DEPA","pais_id":obj.pais.id}};
            CONEXP('comun','ubigeos/searchform').GET_LIST(data).$promise.then(function (r) {
                obj.ubigeos = r;
            }, function (err) {
            });
        }else{
            obj.ubigeos = [];
        }
    };

    self.change_ubigeo_cont = function(obj){
        obj['codigo_telefono'] = null;
        angular.forEach(obj.ubigeos, function(item) {
            if(obj.ubigeo_id === item.id){
                obj['codigo_telefono'] = item['codigo_telefono'];
            }
        }, console.log);
    };

    self.list_tiposambito = function () {
        var dat = { 'ordering':'nombre','f':{'estado':'1'}};
        CONEXP('comun','tiposambito/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposambito = r;
        }, function (err) {
        });
    };
    self.list_tiposambito();

    self.cargar_inicial = function(tipo_persona){
        self.obj['tipo_persona_id'] = tipo_persona;

        if(tipo_persona == self.codigo_per_nat && !self.cargo_per_nat){
            self.list_sexos();
        }

        self.list_tiposfecha(tipo_persona);
        self.list_tiposdocumento(tipo_persona);
        self.list_tiposdireccion(tipo_persona);
        self.list_entidadfinancieras();
        self.list_tiposcuentafinan();

        if(tipo_persona == self.codigo_per_nat){
            self.list_tiposcontribuyente();
            self.cargo_per_nat = true;
        }
        if(tipo_persona == self.codigo_per_jur){
            self.list_tiposcontribuyente_pj();
            self.cargo_per_jur = true;
        }
        if(!obj){
            self.list_documentos = [];
            self.add_documento();
            self.list_direcciones = [];
            self.add_direccion();
            self.list_contactos = [];
            self.list_profesionesoficio = [];
            self.list_actividades = [];
            self.list_cuentas = [];
            self.obj['tipo_contribuyente_id'] = null;
            self.obj['tipo_fecha_id'] = null;
            self.obj['anio'] = null;
            self.obj['mes_id'] = null;
            self.obj['dia'] = null;
        }
    };

    self.change_tipo_per = function(tipo){
        self.cargar_inicial(tipo);
    };



    self.save_or_update = function () {
        self.guardando = true;
        self.obj['entidad_id'] = entidad_id;
        if(self.obj['nacionalidad']){
            self.obj['nacionalidad_id'] = self.obj.nacionalidad['id'];
        }else{
            self.obj['nacionalidad_id'] = null;
        }

        if(self.obj['repres_legal'] && self.obj['tiene_res_legal']){
            self.obj['repres_legal_id'] = self.obj.repres_legal['id'];
        }else{
            self.obj['repres_legal_id'] = null;
        }

        self.obj['documentos'] = self.list_documentos||[];
        self.obj['direcciones'] = self.list_direcciones||[];
        self.obj['contactos'] = self.list_contactos||[];
        self.obj['profesionesoficio'] = self.list_profesionesoficio||[];
        self.obj['actividades'] = self.list_actividades||[];
        self.obj['cuentas'] = self.list_cuentas||[];

        self.obj['del_documentos'] = self.list_del_documentos||[];
        self.obj['del_direcciones'] = self.list_del_direcciones||[];
        self.obj['del_contactos'] = self.list_del_contactos||[];
        self.obj['del_profesionesoficio'] = self.list_del_profesionesoficio||[];
        self.obj['del_actividades'] = self.list_del_actividades||[];
        self.obj['del_cuentas'] = self.list_del_cuentas||[];


        if(self.obj.id){
            CONEXP('comun','personas/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('comun','personas/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};
    self.BuscarPais = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('comun','paises/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarUbigeo = function(search, pais_id){
        filtros = {};
        filtros['q'] = {codigo__icontains: search, nombre__icontains:search};
        filtros['f'] = {pais_id:pais_id, tipo_ubigeo__codigo:'DIST'};
        filtros['limit'] = 10;
        return CONEXP('comun','ubigeos/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarProfesionOficio = function(search){
        filtros = {};
        filtros['q'] = {'nombre__icontains':search,'codigo__icontains':search}
        filtros['f'] = {estado:'1'};
        filtros['ordering'] = 'nombre';
        filtros['limit'] = 10;
        return CONEXP('comun','profesionesoficio/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarPersonaDocumento = function(search){
        filtros = {};
        filtros['q'] = {'nombre_completo__icontains':search, 'codigo__icontains':search,
                        'persona_persona_documentos__numero__icontains':search};
        filtros['limit'] = 10;
        filtros['ordering'] = 'nombre_completo';
        filtros['f'] = {'estado':'1','tipo_persona__codigo':'01'};
        return CONEXP('comun','personas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarClasificacionIiu = function(search){
        filtros = {};
        filtros['q'] = {'nombre__icontains':search,'codigo__icontains':search}
        filtros['f'] = {estado:'1'};
        filtros['ordering'] = 'nombre';
        filtros['limit'] = 10;
        return CONEXP('comun','clasificacionesiiu/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.buscardatos = function(ob){
        self.cargando = true;
        if(ob['codigo'] && ob['tipo_documento_id'] && ob['numero']){
            if (ob['codigo'] ==='1' || ob['codigo'] ==='RUC'){
                // codigos: dni 1, ruc RUC
                var data = {numero: ob['numero']};
                var url = 'datosdni';
                if (ob['codigo'] == 'RUC'){
                    url = 'datosruc';
                }
                CONEXP('caja','functions/'+url).GET(data).$promise.then(function (r) {
                    self.cargando = false;
                    if (r.success && ob['codigo'] !== 'RUC'){
                        self.obj['nombres'] = r['result']['nombre'];
                        self.obj['papellido'] = r['result']['papellido'];
                        self.obj['sapellido'] = r['result']['sapellido'];
                        self.list_direcciones[0]['direccion_detalle'] = r['result']['direccion'];
                    }
                    if(r.success && ob['codigo'] === 'RUC'){
                        self.obj['razon_social'] = r['result']['razon_social'];
                        self.obj['nombre_comercial'] = r['result']['nombre_comercial'];
                        self.list_direcciones[0]['direccion_detalle'] = r['result']['direccion'];
                    }
                    if (!r.success){
                        self.obj['nombres'] = null;
                        self.obj['papellido'] = null;
                        self.obj['sapellido'] = null;
                        self.obj['razon_social'] = null;
                        self.obj['nombre_comercial'] = null;

                    }
                }, function (err) {
                });
            }else{
                self.cargando = false;
                if (!obj){
                    self.obj['nombres'] = null;
                    self.obj['papellido'] = null;
                    self.obj['sapellido'] = null;
                    self.obj['razon_social'] = null;
                    self.obj['nombre_comercial'] = null;
                }
            }
        }else{
            self.cargando = false;
        }
    };

    self.cargando = false;
    self.verificar_nro = function(ob, persona_id){
        self.cargando = true;
        ob['persona_id'] = persona_id||'';
        CONEXP('comun','personas/verificarnro').GET(ob).$promise.then(function (r) {
            ob.existe = r.existe;
            self.buscardatos(ob);

        }, function (err) {
        });
    };

});