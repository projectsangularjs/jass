angular.module('app').controller('AppCtrl', function ($scope, config, data_app,
        $document, $window, $state, $mdSidenav, $timeout, $log, localStorageService, $rootScope, $http,
        CONEX, CONEXP, toastr, $ocLazyLoad, $mdDialog) {

    $scope.$on('ocLazyLoad.moduleLoaded', function (e, params) {
    });

    $scope.$on('ocLazyLoad.componentLoaded', function (e, params) {
    });

    $scope.$on('ocLazyLoad.fileLoaded', function (e, file) {
    });

    $rootScope.state = $state;
    $rootScope.Indeterminado = true;
    $scope.url_api_storage = config.url_api_storage;
    var data_user = localStorageService.get('data_user');
    $scope.data_user = data_user;
    
    $scope.app = data_app;

    function openPage() {
        $scope.closeAside();
    }

    $scope.goBack = function () {
        $window.history.back();
    };

    $scope.openAside = function () {
        console.log("open aside");
        $timeout(function () {
            $mdSidenav('aside').open();
        });
    };
    $scope.closeAside = function () {
        console.log("close aside");
        $timeout(function () {
            $document.find('#aside').length && $mdSidenav('aside').close();
        });
    };

    $scope.aside_folder = function () {
        if ($scope.app.setting.asideFolded) {
            $scope.app.setting.asideFolded = false;
        } else {
            $scope.app.setting.asideFolded = true;
        }
    };

    $rootScope.CerrarSesion = function(){
        var data = {};
        CONEXP('segur','functions/clienteapp').POST(config.oauth2).$promise.then(function (r) {
                data['token'] = localStorageService.get('limon_acctok');
                data['client_id'] = r.client_id;
                data['client_secret'] = r.client_secret;

                CONEXP('aouth2','revoke_token').POST_OAUTH(data).$promise.then(function (r) {
                        toastr.info("Sesión cerrada correctamente","Vuelva pronto");
                        localStorageService.clearAll();
                        $window.location = config.url_login;
                }, function (err) {
                    self.guardando = false;
                    toastr.error("No se pudo cerrar sesión","Error");
                });

        }, function (err) {
            
        });
    };

    $scope.ir_estado = function (estado) {
        $state.go(estado + "");
    };

    self.get_datos = function(){
         CONEXP('segur','functions/datosusuario').GET().$promise.then(function (r) {
            $scope.user = r;
            localStorageService.set('limon_per_id', r['per_id']);
            self.cargado = true;
        }, function (err) {
        }); 
     };
    
    self.get_datos();

    $scope.AbrirToogle = correrToogle('sid_config_app');

    function correrToogle(navID) {
        return function () {
            $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
        };
    }
    $scope.close_sidenav = function (idNav) {
        $mdSidenav(idNav).close()
                .then(function () {
                    $log.debug("close RIGHT is done");
                });
    };

    $scope.Palettes = [
        {name: 'Inicial', color: '#FF7E00', theme: 'Amber'},
        {name: 'Red', color: 'red', theme: 'Red'},
        {name: 'Deep purple', color: '#291694', theme: 'DeepPurple'},
        {name: 'Indigo', color: 'indigo', theme: 'Indigo'},
        {name: 'Blue', color: 'blue', theme: 'Blue'},
        {name: 'Light blue', color: 'rgb(3,169,244)', theme: 'LightBlue'},
        {name: 'Cyan', color: 'cyan', theme: 'Cyan'},
        {name: 'Teal', color: 'teal', theme: 'Teal'},
        {name: 'Purle', color: 'purple', theme: 'Purple'},
        {name: 'Green', color: 'green', theme: 'Green'},
        {name: 'Light green', color: 'rgb(139,195,74)', theme: 'LightGreen'},
        {name: 'Lime', color: 'lime', theme: 'Lime'},
        {name: 'Yellow', color: 'yellow', theme: 'Yellow'},
        {name: 'Amber', color: '#FF7E00', theme: 'Amber'},
        {name: 'Orange', color: 'orange', theme: 'Orange'},
        {name: 'Deep orange', color: '#ff9900', theme: 'DeepOrange'},
        {name: 'Brown', color: 'brown', theme: 'Brown'},
        {name: 'Grey', color: 'grey', theme: 'Grey'},
        {name: 'Blue grey', color: '#6699CC', theme: 'BlueGrey'}
    ];

    $scope.cargar_theme = function () {
        var theme = localStorageService.get('tema_app');
        if (theme) {
            $rootScope.dynamicTheme = theme;
        }
    };

    $scope.setTheme = function (theme) {
        localStorageService.set('tema_app', theme);
        $scope.cargar_theme();
    };

    $scope.cargar_theme();

    $rootScope.ejecutar_ = function(){
        console.log("EJECUTAR*****");
    }

});
angular.module('app').controller('SettingsCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
        $mdSidenav('sid_config_app').close()
                .then(function () {
                    $log.debug("close RIGHT is done");
                });
    };
});
angular.module('app').controller('AsideCtrl', function ($scope, $log, config, data_app,
    $http, $mdSidenav, CONEXP, ssSideNav, $timeout, $ocLazyLoad, $mdDialog, localStorageService, 
    $rootScope, $state) {

    $scope.app = data_app;
    $scope.menu = [];
    $scope.cargo_menu = false;
    $scope.cargo_entidad = true;
    $scope.url_api_storage = config.url_api_storage;

    $scope.list_menu = function (id) {
        $scope.cargo_menu = false;
        CONEXP('segur','functions/menu').GET_LIST().$promise.then(function (r) {
            ssSideNav.sections = r;
            $scope.menu = ssSideNav;
            $scope.cargo_menu = true;
        }, function (err) {
            $scope.cargo_menu = true;
        });
    };

   
    
    // $scope.listar('');

    var limon_entidad = localStorageService.get('limon_entidad');

    $scope.get_entidad = function(id){
        $scope.cargo_entidad = false;
        CONEXP('segur','functions/datosentidad').GET({id: id}).$promise.then(function (r) {
            $scope.ent = r;
            $scope.cargo_entidad = true;
             $scope.list_menu();
        }, function (err) {
        }); 
    }

    $rootScope.selentidad = function () {
        $ocLazyLoad.load([
            'js/controllers/selentidad.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "Selentidad",
                controllerAs: "form",
                templateUrl: 'views/selentidad.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                escapeToClose: false
            }).then(function (result) {
                localStorageService.set('limon_entidad', result);
                $scope.get_entidad(result);
                $state.go("app");
            }, function () {
                $scope.CerrarSesion();
            });
        });
    };

    if(limon_entidad){
        $scope.get_entidad(limon_entidad);
    }else{
        CONEXP('segur','functions/misentidades').GET_LIST().$promise.then(function (r) {
            if(r.length === 1){
                localStorageService.set('limon_entidad', r[0]['id']);
                $scope.get_entidad(r[0]['id']);
            }else{
                $rootScope.selentidad();
            }
        }, function (err) {
            self.cargado = true;
        });
        
    }
    
    $scope.menu2 = [];

    $scope.close_sidenav = function (idNav) {
        $mdSidenav(idNav).close()
                .then(function () {
                    $log.debug("close RIGHT is done");
                });
    };

});
