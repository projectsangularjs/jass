angular.module('app').controller('ComprobantesConfigForm', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP, localStorageService) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;

    if (obj){
       CONEXP('procesos', 'comprobantesconfig/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            self.cargado = true;
        }, function (err) {
        });
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }

    var entidad_id = localStorageService.get('limon_entidad');

    self.save_or_update = function () {
        self.guardando = true;
        self.obj['entidad_id'] = entidad_id;
        self.obj['comprobante_id'] = self.obj['comprobante']['id'];

        if(self.obj.id){
            CONEXP('procesos', 'comprobantesconfig/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(self.obj);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('procesos', 'comprobantesconfig/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    console.log(r)
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};

    self.BuscarComprobante = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('procesos','comprobantes/searchform').GET_LIST(filtros).$promise.then(function (r) {
            console.log(r);
            return r;
        });
    };

    /*self.BuscarEntidad = function(search, persona_id){*/
    self.BuscarEntidad = function(search, persona_id){
        filtros = {};
        filtros['q'] = {persona__nombre_completo__icontains: search};
        filtros['f'] = {estado:'1'};
        filtros['limit'] = 10;
        return CONEXP('comun','entidades/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

});