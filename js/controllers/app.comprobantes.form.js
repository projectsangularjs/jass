angular.module('app').controller('ComprobanteForm', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;
    self.errores = [];

    if (obj){
       CONEXP('procesos','comprobantes/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            self.cargado = true;
        }, function (err) {
        });
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.errores = [];
        self.guardando = true;
        if(self.obj.id){
            CONEXP('procesos','comprobantes/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('procesos','comprobantes/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                self.errores = err.data;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

});