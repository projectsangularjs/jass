angular.module('app').controller('MesRecibosForm', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP, localStorageService) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;
    self.errores = [];
    var entidad_id = localStorageService.get('limon_entidad');
    self.list_recomendaciones = [];

    if (obj){
       CONEXP('procesos','mesrecibos/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            self.obj['fecha_emision_'] = moment(r.fecha_emision, 'YYYY-MM-DD').toDate();
            self.obj['fecha_vencimiento_'] = moment(r.fecha_vencimiento, 'YYYY-MM-DD').toDate();
            self.obj['fecha_corte_'] = moment(r.fecha_corte, 'YYYY-MM-DD').toDate();
            self.list_recomendaciones = r['recomendaciones'];

            self.cargado = true;
        }, function (err) {
        }); 
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.errores = [];
        self.guardando = true;
        self.obj['fecha_emision'] = moment(self.obj['fecha_emision_']).format("YYYY-MM-DD");
        self.obj['fecha_vencimiento'] = moment(self.obj['fecha_vencimiento_']).format("YYYY-MM-DD");
        self.obj['fecha_corte'] = moment(self.obj['fecha_corte_']).format("YYYY-MM-DD");
        self.obj['entidad_id'] = entidad_id;

        self.obj['recomendaciones'] = self.list_recomendaciones||[];
        self.obj['del_recomendaciones'] = self.list_del_recomendaciones||[];

        if(self.obj.id){
            CONEXP('procesos','mesrecibos/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('procesos','mesrecibos/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                self.errores = err.data;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.meses = [];

    self.list_meses = function () {
        var dat = { 'ordering':'id'};
        CONEXP('comun','meses/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.meses = r;
        }, function (err) {
        });
    };
    self.list_meses();

    self.list_estados = function () {
        var dat = { 'ordering':'id'};
        CONEXP('procesos','choices/estadosmesrecibo').GET_LIST(dat).$promise.then(function (r) {
            self.estados = r;
        }, function (err) {
        });
    };
    self.list_estados();

    self.add_recomendacion = function(){
        self.list_recomendaciones.push({});
    };

    self.list_del_recomendaciones = [];
    self.del_recomendacion = function(list_recomendaciones, index, item){
        list_recomendaciones.splice(index, 1);
        if(item['id']){
            var obj ={ id: item['id']};
            self.list_del_recomendaciones.push(obj);
        };
    };

});