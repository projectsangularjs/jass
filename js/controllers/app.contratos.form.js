angular.module('app').controller('ContratosForm', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;

    self.dir = {};
    self.tiposvia = [];
    self.tiposnumerovia = [];
    self.tiposzona = [];

    self.direcciones = [];

    if (obj){
       CONEXP('procesos', 'contratos/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.dir['completa'] = true;
            if (r['predio_direcciones'].length > 0) {
                self.dir = r['predio_direcciones'][0];
                if (self.dir['completa'] == 'SI') {
                    self.dir['completa'] = true;
                }
            }
            self.obj = r;
            self.cargado = true;
        }, function (err) {
        });
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
        self.dir['completa'] = true;
    }

    self.save_or_update = function () {
        self.guardando = true;
        self.obj.zona_id = self.obj.zona.id;
        if(self.obj.duenio){
            self.obj.duenio_id = self.obj.duenio.id;
        }else{
            self.obj.duenio_id = null;
        }
        if(self.obj.representante){
            self.obj.representante_id = self.obj.representante.id;
        }else{
            self.obj.representante_id = null;
        }
        self.obj.estado_predio_id = self.obj.estado_predio.id;
        /*DIRECCIONES*/
        if(self.obj.pais){
            self.dir['pais_id'] = self.obj.pais.id;
        }else{
            self.dir['pais_id'] = null;
        }
        if(self.obj.ubigeo){
            self.dir['ubigeo_id'] = self.obj.ubigeo.id;
        }else{
            self.dir['ubigeo_id'] = null;
        }
        if (self.dir['completa']) {
            self.dir['completa'] = 'SI'
        }else{
            self.dir['completa'] = 'NO'
        }
        self.obj['direcciones'] = self.dir;
        console.log(self.obj['direcciones'])
        if(self.obj.id){
            CONEXP('procesos', 'contratos/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(self.obj);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('procesos', 'contratos/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    console.log(r)
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};

    self.BuscarPersona = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre_completo__icontains: search, persona_persona_documentos__numero__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('comun','personas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarPredio = function () {
        filtros = {};
        var dat = {'limit':20};
        CONEXP('procesos','predio/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposvia = r;
        }, function (err) {
        });
    }
    /*---------------------------------------------------------*/

    self.BuscarPais = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1', pais_ubigeos__ubigeo_zonas__nombre__icontains:''};
        return CONEXP('comun','paises/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarUbigeo = function(search, pais_id){
        filtros = {};
        filtros['q'] = {codigo__icontains: search, nombre__icontains:search};
        filtros['f'] = {pais_id:pais_id, tipo_ubigeo__codigo:'DIST', ubigeo_zonas__nombre__icontains:''};
        filtros['limit'] = 10;
        return CONEXP('comun','ubigeos/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarZona = function(search, ubigeo_id){
        filtros = {};
        filtros['q'] = {codigo__icontains: search, nombre__icontains:search};
        filtros['f'] = {ubigeo_id:ubigeo_id, estado:'1'};
        filtros['limit'] = 10;
        return CONEXP('procesos','zonas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarEstadoPredio = function (search) {
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('procesos','estadospredio/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.change_pais = function(obj) {
        self.obj['ubigeo'] = null;
        self.obj['zona'] = null;
    }

    self.change_ubigeo = function(obj) {
        self.obj['zona'] = null;
    }

    self.change_zona = function (zona) {
        if (zona) {
            self.dir['tipo_zona_id'] = null;
            self.dir['tipo_zona_id'] = zona.tipo_zona.id;
            self.dir['nombre_zona'] = zona.nombre;
        }
    }

    /* FORMULARIO DIRECCIÓN*/
    ;

    self.list_tiposnumerovia = function () {
        var dat = {'limit':50};
        CONEXP('comun','tiposnumerovia/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposnumerovia = r;
        }, function (err) {
        });
    };
    self.list_tiposnumerovia();

    self.change_tipo_numero_via = function () {
        self.dir['numero_via'] = null;
    }

    self.list_tiposzona = function () {
        var dat = {'limit':50};
        CONEXP('comun','tiposzona/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposzona = r;
        }, function (err) {
        });
    };
    self.list_tiposzona();

});