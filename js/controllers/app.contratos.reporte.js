angular.module('app').controller('Contratosreporte', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, $state, localStorageService, $scope) {

    var self = this;
    self.Indeterminado = true;
    self.listado = [];
    self.search = '';
    var entidad_id = localStorageService.get('limon_entidad');

    self.list_tipoafilicion = function(){
        var dat={'search':''};
        CONEXP('procesos','tiposafiliacion/searchform').GET_LIST().$promise.then(function(r){
            self.tiposafiliacion = r;
        },function(err){
        });
    };
    self.list_tipoafilicion();

    self.list_zonas = function(){
        var dat={'search':''};
        CONEXP('procesos','zonas/searchform').GET_LIST().$promise.then(function(r){
            self.zonas = r;
        },function(err){
        });
    };
    self.list_zonas();

    self.pagination = {};

    self.list_ordenar = [
        {id:'numero',titulo:'Número de contrato'},
        {id:'persona__nombre_completo',titulo:'Nombre persona'},
        {id:'persona__persona_persona_juridica__razon_social,persona__persona_persona_natural__papellido,persona__persona_persona_natural__sapellido',titulo:'Apellidos y nombres'},
        {id:'predio__zona__nombre,predio__predio_direcciones__direccion_detalle',titulo:'Zona y dirección'},
        {id:'persona__persona_afiliaciones__tipo_afiliacion__nombre,predio__zona__nombre,predio__predio_direcciones__direccion_detalle',titulo:'Afiliación, Zona y dirección'},
    ];

    self.ordering = self.list_ordenar[0]['id'];
    self.tipo_afiliacion_id = 'T';
    self.zona_id = 'T';

    self.paging = {
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
        estado__in:'2,4',
        entidad_id:entidad_id
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.paging.search = self.search||'';
        self.paging.ordering = self.ordering;

        if(self.tipo_afiliacion_id !=='T'){
            self.paging['persona__persona_afiliaciones__tipo_afiliacion_id'] = self.tipo_afiliacion_id;
        }else{
            delete self.paging['persona__persona_afiliaciones__tipo_afiliacion_id'];
        }

        if(self.zona_id !=='T'){
            self.paging['predio__zona_id'] = self.zona_id;
        }else{
            delete self.paging['predio__zona_id'];
        }

        CONEXP('procesos', 'contratosreporte').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];

            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    // self.listar();

    function loadPages() {
        self.listar();
    }

   self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
   };

   self.ordenar = function(id){
        self.ordering = id;
        self.listar_por();
   };

   self.descargar = function(tableId){
        var blob = new Blob([document.getElementById(tableId).innerHTML], {
            type: "text/plain;charset=utf-8;"
        });
        saveAs(blob, "Listado-contratos.xls");
   };

});