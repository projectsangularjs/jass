angular.module('app').controller('Perfil', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, config) {

    var self = this;
    self.cargado = false;
    self.listado = [];
    self.search = '';
    self.per = {};
    self.url_api_storage = config.url_api_storage;

    self.get_datos = function(){
         CONEXP('segur','functions/datosusuarioperson').GET().$promise.then(function (r) {
            self.obj = r;
            self.cargado = true;
        }, function (err) {
        }); 
     };
    
    self.get_datos();

    self.edit_datos = function (event) {
        $ocLazyLoad.load([
            'js/controllers/app.personas.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasForm",
                controllerAs: "form",
                templateUrl: 'views/personas/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: self.obj}
            }).then(function (result) {
                self.get_datos();
            }, function () {
            });
        });
    };

    self.changepass = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.perfil.change.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PerfilChange",
                controllerAs: "form",
                templateUrl: 'views/perfil/change.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: self.obj}
            }).then(function (result) {
            }, function () {
            });
        });
    };

    self.viewfoto = function (event, obj) {
        var imagen = obj.foto_logo;
        $ocLazyLoad.load([
            'js/controllers/app.utils.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "UtilsImag",
                controllerAs: "form",
                templateUrl: 'views/utils/viewimagen.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: true,
                locals: {obj: {tipo:'Foto o logo', 
                'descripcion':obj.nombre_completo,
                'imagen': imagen}}
            }).then(function (result) {
            }, function () {
            });
        });
    };

   
});