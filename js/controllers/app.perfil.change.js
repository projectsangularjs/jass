angular.module('app').controller('PerfilChange', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP, $ocLazyLoad) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;

    self.cargado = true;

    self.changepassword = function () {
        self.guardando = true;
        // self.obj['user_id'] = self.obj['id'];

        CONEXP('segur','functions/changepassword').POST(self.obj).$promise.then(function (r) {
            self.guardando = false;
            if(r.estado){
                toastr.success("Contraseña cambiada correctamente");
                $mdDialog.hide(r);
            }else{
                toastr.error(r.error,"Verifique los datos");
            }
        }, function (err) {
            self.guardando = false;
            toastr.error("No se pudo actualizar","Error");
        });

    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

});