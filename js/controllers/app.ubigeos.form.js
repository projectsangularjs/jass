angular.module('app').controller('UbigeosForm', function (obj, $scope, $mdDialog,
    CONEXP, toastr) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;

    if (obj){
       CONEXP('comun','ubigeos/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            self.cargado = true;
        }, function (err) {
        }); 
    }else{
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.guardando = true;
        self.obj['pais_id'] = self.obj.pais['id'];
        self.obj['tipo_ubigeo_id'] = self.obj.tipo_ubigeo['id'];

        if(self.obj.region_natural){
            self.obj['region_natural_id'] = self.obj.region_natural['id'];
        }else{
            self.obj['region_natural_id'] = null;
        }
        if(self.obj.padre){
            self.obj['padre_id'] = self.obj.padre['id'];
        }else{
            self.obj['padre_id'] = null;
        }

        if(self.obj.id){
            CONEXP('comun','ubigeos/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('comun','ubigeos/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};
    // self.BuscarTipoMovimiento = function (search) {
    //     filtros['search'] = search;
    //     filtros['limit'] = 10;
    //     return CONEXP('tiposmovimiento/list/form').GET_LIST(filtros).$promise.then(function (r) {
    //         return r;
    //     });
    // };

    self.BuscarPais = function (search) {
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        // filtros['ordering'] = "-nombre";
        return CONEXP('comun','paises/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarTipoUbigeo = function (search) {
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('comun','tiposubigeo/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarRegionNatural = function (search) {
        filtros['search'] = search;
        filtros['limit'] = 10;
        return CONEXP('regionesnatural/list/form').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

});