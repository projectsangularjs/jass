angular.module('app').controller('Personas', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, config) {

    var self = this;
    self.Indeterminado = true;
    self.listado = [];
    self.search = '';

    self.pagination = {};
    self.url_api_storage = config.url_api_storage;

    self.list_ordenar = [
        {id:'codigo',titulo:'Código'},
        {id:'nombre_completo',titulo:'Nombre completo'},
        {id:'-nacionalidad',titulo:'Nacionalidad'},
        {id:'tipo_persona',titulo:'Tipo de persona'}
    ];

    self.ordering = self.list_ordenar[0]['id'];

    self.paging = {
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.paging.search = self.search||'';
        self.paging.ordering = self.ordering;
        CONEXP('comun','personas').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];

            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    // self.listar();

    function loadPages() {
        self.listar();
    }

   self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
   };

   self.ordenar = function(id){
        self.ordering = id;
        self.listar_por();
   };

    self.new_edit = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.personas.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasForm",
                controllerAs: "form",
                templateUrl: 'views/personas/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj},
                fullscreen: true,
                // hasBackdrop: true
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };

    self.form_foto = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.personas.formfoto.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasFormfoto",
                controllerAs: "form",
                templateUrl: 'views/personas/form_foto.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj}
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };

    self.delete = function (event, obj) {
        var confirm = $mdDialog.confirm()
                .title("Eliminar persona")
                .textContent("¿Está seguro de eliminar la persona " + obj['nombre_completo'] + "?")
                .ariaLabel('delete')
                .targetEvent(event)
                .ok('SI')
                .cancel('NO');
        $mdDialog.show(confirm).then(function () {
            CONEXP('comun','personas/delete').DELETE({id: obj.id}).$promise.then(function (r) {
               if(r){
                     toastr.success('Eliminada correctamente', 'Eliminado');
                    self.listar();
                }
            }, function (err) {
                toastr.error('No se pudo eliminar esta persona', 'Error');
            });
        }, function () {
        });
    };

    self.viewfoto = function (event, obj) {
        var imagen = obj.foto_logo;
        $ocLazyLoad.load([
            'js/controllers/app.utils.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "UtilsImag",
                controllerAs: "form",
                templateUrl: 'views/utils/viewimagen.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: true,
                locals: {obj: {tipo:'Foto o logo', 
                'descripcion':obj.nombre_completo,
                'imagen': imagen}}
            }).then(function (result) {
            }, function () {
            });
        });
    };

});