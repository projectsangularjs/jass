angular.module('app').controller('ContratoPago', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, $state, config, localStorageService, $rootScope) {
	/*GENERAL*/
    var self = this;
    self.obj = {};
    
    $rootScope.paso = 1;
    $rootScope.listado = [];
    $rootScope.listado = [
    	{id:'1',titulo:'Registro del contrato', complete:true},
    	{id:'2',titulo:'Pago en caja', complete:false},
    	{id:'3',titulo:'Descargar contrato', complete:false},
    ];

    var entidad_id = localStorageService.get('limon_entidad');
    var contrato_id = $state.params.id;

    self.cargado_cronog = false;

    self.cronog = {};

    

    self.get_cronog = function(contrato_id){
        self.cargado_cronog = false;
        var filt = {
            f:{'contrato_id':contrato_id,'motivo_movimiento_cuenta__codigo':'SERINS'}
        };
        self.cronog = {};
        CONEXP('procesos','cronogramaspago/listcronogpagar').GET(filt).$promise.then(function (r) {
            // self.mispredios = r;
            if(r['contratos'][0].cronogramas.length>0){
                
                if(r['contratos'][0].cronogramas[0].pagado>=r['contratos'][0].cronogramas[0].monto){
                    $state.go("app.addcontrato.documento",{'id':contrato_id});
                }else{
                    self.cronog = r['contratos'][0].cronogramas[0];
                    self.obj['importe'] = self.cronog.monto-self.cronog.pagado;
                }
            }else{
                if(r.pagado>=r.monto_total){
                    $state.go("app.addcontrato.documento",{'id':contrato_id});
                }
            }
            self.cargado_cronog = true;
            
        }, function (err) {
            self.cargado_cronog = true;
        });
    };

    CONEXP('procesos','contratos/edit').GET({id: contrato_id}).$promise.then(function (r) {
        self.cont = r;
        self.get_cronog(contrato_id);
    }, function (err) {
    });

    self.ir_contrato = function(){
        $state.go("app.addcontrato.documento",{'id':contrato_id});
    };

    // self.list_tiposmediopago();

    self.list_cuentasempresa = function (obj) {
        var dat = { 'ordering':'tipo_cuenta_id,nombre',
            'f':{
                "estado":"1", 
                'tipo_cuenta_id': obj['tipo_cuenta_id'],
                'entidad_id':entidad_id,
                }
            };
        CONEXP('caja','cuentasentidad/searchformcaja').GET_LIST(dat).$promise.then(function (r) {
            self.cuentasentidad = r;
            if(r.length>0){
                self.obj['cuenta_entidad_id'] = r[0]['id'];
            }
        }, function (err) {
        });
    };

    self.list_tiposmediopago = function (tipo_cuenta_id) {
        var dat = { 'ordering':'id',
        'f':{"estado":"1","tipo_cuenta_id":tipo_cuenta_id}};
        CONEXP('caja','tiposmediopago/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposmediopago = r;
            if(r.length>0){
                self.obj['tipo_medio_pago_id'] = r[0]['id'];
            }
        }, function (err) {
        });
    };

    self.change_tipo_cuenta = function(obj){
        obj['cuenta_entidad_id'] = "";
        obj['tipo_medio_pago_id'] = "";
        self.list_tiposmediopago(obj['tipo_cuenta_id']);
        self.list_cuentasempresa(obj);
        if(self.obj['importe']){
            self.obj['importe'] = self.cronog.monto-self.cronog.pagado;
        }
    };

     self.list_tiposcuenta = function () {
        var dat = { 'ordering':'id',
        'f':{"estado":"1"}};
        CONEXP('caja','tiposcuenta/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposcuenta = r;
            if(r.length>0){
                self.obj['tipo_cuenta_id'] = r[0]['id'];
                self.change_tipo_cuenta(self.obj);
            }
        }, function (err) {
        });
    };
    self.list_tiposcuenta();


    self.get_datosentidad = function(){
        var dat = { 'id':entidad_id};
        CONEXP('comun','entidades/datosbasic').GET(dat).$promise.then(function (r) {
            self.entidad = r;
            if(r){
                self.obj['moneda_id'] = r['moneda_id'];
            }
        }, function (err) {
        });
    };

    self.monedas = [];
    self.list_monedas = function () {
        var dat = { 'ordering':'id',
        'f':{"estado":"1"}};
        CONEXP('comun','monedas/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.monedas = r;
            self.get_datosentidad();
        }, function (err) {
        });
    };
    self.list_monedas();


    self.get_cotizacion = function(){
        CONEXP('caja','functions/cotizaciondolar').GET({entidad_id: entidad_id}).$promise.then(function (r) {
            if(r){
                self.obj['tipo_cambio'] = r['venta'];
                self.change_importe(self.obj)
            }            
        }, function (err) {
        });
    };

    self.obj['tipo_cambio'] = 1;
    self.obj['comisiones'] = 0;
    self.obj['vuelto'] = 0;
    self.change_moneda = function(obj){
        obj['tipo_cambio'] = 1;
        angular.forEach(self.monedas, function(item) {
            if(item.id === obj.moneda_id ){
                if(item.codigo === 'USD'){
                    self.get_cotizacion();
                }
            }
        }, console.log);
        self.change_importe(self.obj)
    };

    self.change_recibido = function(obj){
        if(obj['importe'] && obj['recibido']){
            obj['vuelto'] = obj['recibido'] - obj['importe'];
        }else{
            self.obj['vuelto'] = 0;
        }
    };

    self.registrarpago = function(){

        self.errores = [];
        self.guardando = true;
        self.obj['entidad_id'] = entidad_id;
        self.obj['cronogramas'] = [];
        self.obj['motivo_mov_cuenta_id'] = self.cronog['motivo_movimiento_cuenta']['id'];
        self.obj['cronograma_pago_id'] = self.cronog['id'];
        self.obj['es_contrato'] = 'SI';
        self.obj['contrato_id'] = contrato_id;
        self.obj['persona_id'] = self.cont.persona.id;

        CONEXP('caja','movimientoscuenta/registrarpago').POST(self.obj).$promise.then(function (r) {
            self.guardando = false;
            if(r){
                self.get_cronog(contrato_id);
                toastr.success("Pago registrado correctamente","Registrado");
                 $mdDialog.hide(r);
            }
        }, function (err) {
            self.guardando = false;
            self.errores = err.data;
            toastr.error("No se pudo registrar","Error");
        });
        
    };

});