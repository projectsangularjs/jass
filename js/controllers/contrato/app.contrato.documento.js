angular.module('app').controller('ContratoDocumento', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, $state, config, localStorageService, $rootScope) {
	/*GENERAL*/
    var self = this;
    self.storage = config.url_api_storage;
    self.entidad_id = localStorageService.get('limon_entidad');

    $rootScope.paso = 2;
    $rootScope.listado = [];

    $rootScope.listado = [
    	{id:'1',titulo:'Registro del contrato', complete:true},
    	{id:'2',titulo:'Pago en caja', complete:true},
    	{id:'3',titulo:'Descargar contrato', complete:false},
    ];

    var entidad_id = localStorageService.get('limon_entidad');
    var contrato_id = $state.params.id;

    self.cargado_cont = false;
    CONEXP('procesos','contratos/edit').GET({id: contrato_id}).$promise.then(function (r) {
        self.cont = r;
        self.cargado_cont = true;
    }, function (err) {
    });

    self.ir_contratos = function(){
    	$state.go("app.contratos");
    };

    self.Indeterminado = false;

    self.descargar = function(opcion){
        self.Indeterminado = true;
        var dat = {entidad_id:entidad_id, 'contrato_id':contrato_id};
        CONEXP('procesos','contratos/pdfcontrato').POST(dat).$promise.then(function (r) {
                var win = window.open('', '_blank');
                pdfMake.createPdf(r).open({}, win);
                self.Indeterminado = false;
            }, function (err) {
                self.Indeterminado = false;               
            });
    };

    self.ver_pago_pdf = function(){
        self.Indeterminado = true;
        var dat = {entidad_id:entidad_id, 'contrato_id':contrato_id};
        CONEXP('procesos','contratos/pdfpago').POST(dat).$promise.then(function (r) {
                var win = window.open('', '_blank');
                pdfMake.createPdf(r).open({}, win);
                self.Indeterminado = false;
            }, function (err) {
                self.Indeterminado = false;               
            });
    };
        
});