
angular.module('app').controller('ContratoRegistro', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, $state, config, localStorageService, $rootScope) {
	/*GENERAL*/
    var self = this;
    self.obj = {};
    
    $rootScope.paso = 0;
    $rootScope.listado = [
    	{id:'1',titulo:'Registro del contrato', complete:false},
    	{id:'2',titulo:'Pago en caja', complete:false},
    	{id:'3',titulo:'Descargar contrato', complete:false},
    ];

    self.entidad_id = localStorageService.get('limon_entidad');

    self.BuscarPersona = function (search) {
        var filt = {
        	limit:10,
        	q:{'nombre_completo__icontains': search, 'persona_persona_documentos__numero__icontains': search},
        	f:{estado: '1'}
        };
        return CONEXP('comun','personas/searchform').GET_LIST(filt).$promise.then(function (r) {
            return r;
        });
    };

    self.mispredios = [];
    self.cargado_predios = true;
    self.get_predios = function(pers_id, predio_id){
        self.cargado_predios = false;
        var filt = {
            q:{'duenio_id':pers_id, 'representante_id':pers_id},
            f:{'estado':'1'}
        };
        CONEXP('procesos','predios/mispredios').GET_LIST(filt).$promise.then(function (r) {
            self.mispredios = r;
            if (predio_id){

                angular.forEach(r, function(item) {
                    if(item['id'] == predio_id){
                        self.obj['predio'] = item;
                    }
                  }, console.log);
                self.cargado_predios = true;
            }else{
                self.cargado_predios = true;
            }
            
        }, function (err) {
            self.cargado_predios = true;
        });
    };


    self.cargado_person = true;
    self.get_datos_persona = function(pers_id){
    	self.cargado_person = false;
        self.cargado_predios = false;
        CONEXP('comun','personas/viewinfo').GET({id: pers_id}).$promise.then(function (r) {
            self.obj.person = r;
             self.obj['persona'] = r;
            self.cargado_person = true;
            self.get_predios(pers_id, null);
        }, function (err) {
        	self.cargado_person = true;
        });
    };

    self.change_persona = function(pers_id) {
        self.get_datos_persona(pers_id);
    };

    self.cancel_persona = function(){
    	self.obj['persona'] = null;
    	self.obj['person'] = null;
    	self.searchTextPE = null;
        self.obj['predio'] = null;
        self.mispredios = [];
    };

    self.form_persona = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.personas.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasForm",
                controllerAs: "form",
                templateUrl: 'views/personas/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                multiple:false,
                locals: {obj: obj},
                fullscreen: true,
            }).then(function (result) {
            	self.get_datos_persona(result['id']);
            }, function () {
            });
        });
    };

    self.sel_predio = function(obj){
        self.obj['predio'] = obj;
    };

    self.cancel_predio = function(){
        self.obj['predio'] = null;
    };

    self.form_predio = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.predios.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PredioForm",
                controllerAs: "form",
                templateUrl: 'views/predios/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj},
                fullscreen: true,
            }).then(function (result) {
                self.obj['predio'] = null;
                self.get_predios(self.obj['persona']['id'], result['id']);
            }, function () {
            });
        });
    };

    self.tiposusoservicio = [];
    self.list_tiposusoservicio = function () {
        var filt = {estado: '1'};
        CONEXP('procesos','tiposusoservicio/searchform').GET_LIST(filt).$promise.then(function (r) {
            self.tiposusoservicio = r;
        }, function (err) {
        });
    };

    self.list_tiposusoservicio();

    self.formascobranza = [];
    self.list_formascobranza = function () {
        CONEXP('procesos','choices/formacobranza').GET_LIST().$promise.then(function (r) {
            // self.contrato['forma_cobranza'] = 'SME';
            self.formascobranza = r;
            if(r.length>0){
                self.obj['forma_cobranza'] = r[0]['id'];
            }
        }, function (err) {
        });
    };

    self.list_formascobranza();


    self.servicios = [];

    self.list_servicios = function () {
        var filt = {
            'f':{'codigo__in':['Inscrip','ServAguaPo','Electrobom'],'estado':'1'},
            'ordering':'id',
        };
        CONEXP('procesos','servicios/searchcontrato').GET(filt).$promise.then(function (r) {
            self.servicios = r['servicios'];
            self.obj['inscrip'] = r['inscripcion'];
        }, function (err) {
        });
    };

    self.list_servicios();

    self.total_mes = function(servicios){
        var total = 0;
        angular.forEach(servicios, function(item) {
            if(item['select'] && item['precio']){
                total += item['precio'];
            }
          }, console.log);
        return total;
    };


    

    self.save_next = function(){
        self.guardando = true;
        self.obj['persona_id'] = self.obj.person.id;
        self.obj['predio_id'] = self.obj.predio.id;
        self.obj['servicios'] = self.servicios;
        self.obj['entidad_id'] = self.entidad_id;
        self.obj['fecha_contrato'] = moment(self.obj['fecha_contrato_']).format("YYYY-MM-DD");

        CONEXP('procesos', 'contratos/add').POST(self.obj).$promise.then(function (r) {
            // self.guardando = false;
            toastr.success("Contrato registrado correctamente","Contrato registrado");
            $state.go("app.addcontrato.pago",{'id':r['id']});
        }, function (err) {
            self.guardando = false;
            toastr.error("No se pudo registrar","Error");
        });
    };

});