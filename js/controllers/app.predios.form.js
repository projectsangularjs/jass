angular.module('app').controller('PredioForm', function (obj, $mdDialog,
    CONEX, toastr, CONEXP, $ocLazyLoad, config, localStorageService) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;

    self.dir = {};
    self.tiposvia = [];
    self.tiposnumerovia = [];
    self.tiposzona = [];

    self.storage = config.url_api_storage;
    var entidad_id = localStorageService.get('limon_entidad');

    self.PaisPredeterminado = function () {
        filtros = {};
        filtros['f'] = {estado: '1', codigo:'PE'};
        return CONEXP('comun','paises/searchform').GET_LIST(filtros).$promise.then(function (r) {
            self.obj['pais'] = r[0];
            return r;
        });
    };

    self.cargar_ubi_existente = function(data_dir){

        self.ubicaciones = [{
          id: 0,
          name: 'Ubicación del predio',
          punto_lat:  parseFloat(self.dir['punto_lat']),
          punto_long: parseFloat(self.dir['punto_long'])
        }];

        self.options2 = {
          mapa: {
            center: new google.maps.LatLng(self.ubicaciones[0]['punto_lat'], self.ubicaciones[0]['punto_long']),
            zoom: 16,
            mapTypeId: 'hybrid'
          },
          marker: function(p) {
            return {
              clickable: false,
              draggable: false,
              title: p.name,
              // icon: 'recursos/imag/maps/person_maps.png'
            }
          }
        };
    };

    if (obj){
       CONEXP('procesos', 'predios/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.dir['completa'] = true;
            if (r['predio_direcciones'].length > 0) {
                self.dir = r['predio_direcciones'][0];
                if (self.dir['completa'] == 'SI') {
                    self.dir['completa_'] = true;
                }else{
                    self.dir['completa_'] = false;
                }
                if(self.dir['punto_lat']){
                    
                    self.cargar_ubi_existente(self.dir);
                }
            }
            self.obj = r;
            self.cargado = true;
        }, function (err) {
        });
    }else{
        self.PaisPredeterminado();
        self.obj['estado'] = '1';
        self.cargado = true;
        self.dir['completa'] = true;
    }

    self.save_or_update = function () {
        self.guardando = true;
        self.obj.duenio_id = self.obj.duenio.id;
        if(self.obj.representante){
             self.obj.representante_id = self.obj.representante.id;
        }else{
             self.obj.representante_id = null;
        }
       
        self.dir['ubigeo_id'] = self.dir.ubigeo.id;
        if (self.dir['completa_']) {
            self.dir['completa'] = 'SI';
        }else{
            self.dir['completa'] = 'NO';
        }
        self.obj['direcciones'] = self.dir;

        if(self.obj.id){
            CONEXP('procesos', 'predios/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(self.obj);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('procesos', 'predios/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};

    self.BuscarPersona = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre_completo__icontains: search, persona_persona_documentos__numero__icontains: search};
        filtros['f'] = {estado: '1'};
        filtros['ordering'] = ['nombre_completo'];
        return CONEXP('comun','personas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarPersonaRepres = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre_completo__icontains: search, persona_persona_documentos__numero__icontains: search};
        filtros['f'] = {estado: '1',tipo_persona__codigo:'01'};
        filtros['ordering'] = ['nombre_completo'];
        return CONEXP('comun','personas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    // self.BuscarPais = function (search) {
    //     filtros = {};
    //     filtros['limit'] = 10;
    //     filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
    //     filtros['f'] = {estado: '1', pais_ubigeos__ubigeo_zonas__nombre__icontains:''};
    //     return CONEXP('comun','paises/searchform').GET_LIST(filtros).$promise.then(function (r) {
    //         return r;
    //     });
    // };

    self.BuscarUbigeo = function(search){
        filtros = {};
        filtros['q'] = {codigo__icontains: search, nombre__icontains:search};
        filtros['f'] = {pais__codigo:'PE', tipo_ubigeo__codigo:'DIST', ubigeo_zonas__nombre__icontains:''};
        filtros['limit'] = 10;
        return CONEXP('comun','ubigeos/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    // self.BuscarZona = function(search){
        
    //     return CONEXP('procesos','zonas/searchform').GET_LIST(filtros).$promise.then(function (r) {
    //         return r;
    //     });
    // };

    self.zonas = [];
    self.list_zonas = function(){
        filtros = {};
        filtros['f'] = {entidad_id:entidad_id, estado:'1'};
        filtros['ordering'] = 'nombre';
        CONEXP('procesos', 'zonas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            self.zonas = r;
        }, function (err) {
        });
    };
    self.list_zonas();

    // self.BuscarEstadoPredio = function (search) {
    //     filtros = {};
    //     filtros['limit'] = 10;
    //     filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
    //     filtros['f'] = {estado: '1'};
    //     return CONEXP('procesos','estadospredio/searchform').GET_LIST(filtros).$promise.then(function (r) {
    //         return r;
    //     });
    // };

    self.estadospredio = [];
    self.list_estadospredio = function(){
        filtros = {};
        filtros['ordering'] = 'nombre';
        filtros['f'] = {estado: '1'};
        CONEXP('procesos', 'estadospredio/searchform').GET_LIST(filtros).$promise.then(function (r) {
            self.estadospredio = r;
        }, function (err) {
        });
    };
    self.list_estadospredio();

    self.change_estado_predio = function(estado_predio_id){
        angular.forEach(self.estadospredio, function(item) {
               if(item.id == estado_predio_id){
                self.obj.estado_predio_codigo = item['codigo'];
               }
          }, console.log);
    };

    self.change_pais = function(obj) {
        self.obj['ubigeo'] = null;
        self.obj['zona'] = null;
    }

    self.change_ubigeo = function(obj) {
        self.obj['zona'] = null;
    }

    self.change_zona = function (zona) {
        if (zona) {
            self.dir['tipo_zona_id'] = null;
            self.dir['tipo_zona_id'] = zona.tipo_zona.id;
            self.dir['nombre_zona'] = zona.nombre;
        }
    }

    /* FORMULARIO DIRECCIÓN*/
    self.list_tiposvia = function () {
        var dat = {'limit':20};
        CONEXP('comun','tiposvia/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposvia = r;
        }, function (err) {
        });
    };

    self.list_tiposvia();

    self.list_tiposnumerovia = function () {
        var dat = {'limit':50};
        CONEXP('comun','tiposnumerovia/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposnumerovia = r;
        }, function (err) {
        });
    };
    self.list_tiposnumerovia();

    self.change_tipo_numero_via = function () {
        self.dir['numero_via'] = null;
    }

    self.list_tiposzona = function () {
        var dat = {'limit':50};
        CONEXP('comun','tiposzona/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposzona = r;
        }, function (err) {
        });
    };

    self.visible = true;

    self.list_tiposzona();
    self.add_punto = function(event, dir){
        self.ubicaciones = [];
        self.options2 = {};
        self.visible = false;

        $ocLazyLoad.load([
            'js/controllers/app.predios.puntodir.js',
        ]).then(function () {
            $mdDialog.show({
                controller: "PrediosPuntodir",
                controllerAs: "form",
                templateUrl: 'views/predios/puntodir.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: dir},
                multiple:true,
                fullscreen: true,
            }).then(function (result) {
                self.visible = true;
                dir['punto_lat'] = result['punto_lat'];
                dir['punto_long'] = result['punto_long'];
                self.cargar_ubi_existente(result);
            }, function () {
                self.visible = true;
                self.cargar_ubi_existente(self.dir);
            });
        });
    };

    self.del_punto = function(){
        self.dir['punto_lat'] = null;
        self.dir['punto_long'] = null;
        self.ubicaciones = [];
        self.options2 = {};
    };

    self.form_duenio = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.personas.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasForm",
                controllerAs: "form",
                templateUrl: 'views/personas/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                multiple:true,
                locals: {obj: obj},
                fullscreen: true,
            }).then(function (result) {
                self.obj['duenio'] = result;
            }, function () {
            });
        });
    };

    self.form_repres = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.personas.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasForm",
                controllerAs: "form",
                templateUrl: 'views/personas/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                multiple:true,
                locals: {obj: obj},
                fullscreen: true,
            }).then(function (result) {
                self.obj['representante'] = result;
            }, function () {
            });
        });
    };

});