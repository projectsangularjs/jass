angular.module('app').controller('EventosForm', function (obj, $scope, $mdDialog,
    CONEXP, $ocLazyLoad, toastr, config, localStorageService) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;
    var entidad_id = localStorageService.get('limon_entidad');
    /*console.log(obj.unidad_tiempo);*/

    if (obj){
       CONEXP('procesos','eventos/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            if(r.fecha){
                self.obj['fecha'] = moment(r.fecha, 'YYYY-MM-DD').toDate();
                self.obj['hora_inicio'] = moment(r.hora_inicio, 'HH:mm:ss').toDate();
                if(r.hora_fin){
                    self.obj['hora_fin'] = moment(r.hora_fin, 'HH:mm:ss').toDate();
                }else{
                    self.obj['hora_fin'] = null;
                }
                if(r.hora_tolerancia){
                    self.obj['hora_tolerancia'] = moment(r.hora_tolerancia, 'HH:mm:ss').toDate();
                }else{
                    self.obj['hora_tolerancia'] = null;
                }
            }
            self.cargado = true;
        }, function (err) {
        });
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.guardando = true;
        self.obj['entidad_id'] = entidad_id;
        self.obj['fecha'] = moment(self.obj['fecha']).format("YYYY-MM-DD");
        if(self.obj['hora_inicio']){
            self.obj['hora_inicio'] = moment(self.obj['hora_inicio']).format("HH:mm:ss");
        }else{
            self.obj['hora_inicio'] = null;
        }
        if(self.obj['hora_fin']){
            self.obj['hora_fin'] = moment(self.obj['hora_fin']).format("HH:mm:ss");
        }else{
            self.obj['hora_fin'] = null;
        }
        if(self.obj['hora_tolerancia']){
            /*console.log(self.obj['hora_tolerancia']);*/
            self.obj['hora_tolerancia'] = moment(self.obj['hora_tolerancia']).format("HH:mm:ss");
            /*console.log(self.obj['hora_tolerancia']);*/
        }else{
            self.obj['hora_tolerancia'] = null;
        }
        self.obj['tipo_evento_id'] = self.obj.tipo_evento['id'];
        self.obj['tipo_asistencia_id'] = self.obj.tipo_asistencia['id'];

        if(self.obj.id){
            CONEXP('procesos','eventos/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('procesos','eventos/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};

    self.BuscarTipoEvento = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('procesos','tiposevento/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    /*self.list_unidadesmedida = function () {
        CONEXP('procesos','choices/unidadestiempo').GET_LIST().$promise.then(function (r) {
            self.unidad_tiempo = r;
        }, function (err) {
        });
    };*/
    /*self.list_unidadesmedida();*/

    self.BuscarTipoAsistencia = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('procesos','tiposasistencia/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.form_persona = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.personas.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasForm",
                controllerAs: "form",
                templateUrl: 'views/personas/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                multiple:true,
                locals: {obj: obj},
                fullscreen: true,
            }).then(function (result) {
                self.obj['persona'] = result;
            }, function () {
            });
        });
    };

});