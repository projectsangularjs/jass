angular.module('app').controller('ZonaForm', function (obj, $scope, $mdDialog,
    toastr, CONEXP, localStorageService ) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;
    var entidad_id = localStorageService.get('limon_entidad');

    if (obj){
       CONEXP('procesos', 'zonas/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            self.cargado = true;
            self.edit = true;
        }, function (err) {
        });
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.guardando = true;
        self.obj.entidad_id = entidad_id;
        if(self.obj.id){
            CONEXP('procesos', 'zonas/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(self.obj);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('procesos', 'zonas/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};

    self.BuscarTipoZona = function (search) {
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('comun','tiposzona/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarPais = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1', pais_ubigeos__nombre__icontains:''};
        return CONEXP('comun','paises/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.change_pais = function(obj) {
        self.obj['ubigeo'] = null;
    }

    self.BuscarUbigeo = function(search, pais_id){
        filtros = {};
        filtros['q'] = {codigo__icontains: search, nombre__icontains:search};
        filtros['f'] = {pais_id:pais_id, tipo_ubigeo__codigo:'DIST'};
        filtros['limit'] = 10;
        return CONEXP('comun','ubigeos/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

});