angular.module('app').controller('Motivosmovimientocuenta', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog) {

    var self = this;
    self.Indeterminado = true;
    self.listado = [];
    self.search = '';
    self.tiposmovimientocuenta = [];
    self.tipo_movimiento_cuenta_id = 'T';

    self.pagination = {};

    self.paging = {
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.paging.search = self.search||'';
        self.paging.ordering = 'tipo_movimiento_cuenta_id, codigo';
        if(self.tipo_movimiento_cuenta_id !=='T'){
            self.paging['tipo_movimiento_cuenta_id'] = self.tipo_movimiento_cuenta_id;
        }else{
            delete self.paging['tipo_movimiento_cuenta_id'];
        }
        
        
        CONEXP('procesos','motivosmovimientocuenta').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];

            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    // self.listar();

    function loadPages() {
        self.listar();
    }

   self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
   };

    self.new_edit = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.motivosmovimientocuenta.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "MotivosmovimientocuentaForm",
                controllerAs: "form",
                templateUrl: 'views/motivosmovimientocuenta/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj}
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };

    self.delete = function (event, obj) {
        var confirm = $mdDialog.confirm()
                .title("Eliminar motivo")
                .textContent("¿Está seguro de eliminar el motivo " + obj['nombre'] + "?")
                .ariaLabel('delete')
                .targetEvent(event)
                .ok('SI')
                .cancel('NO');
        $mdDialog.show(confirm).then(function () {
            CONEXP('procesos','motivosmovimientocuenta/delete').DELETE({id: obj.id}).$promise.then(function (r) {
               if(r){
                     toastr.success('Eliminado correctamente', 'Eliminado');
                    self.listar();
                }
            }, function (err) {
                toastr.error('No se pudo eliminar', 'Error');
            });
        }, function () {
        });
    };

        self.list_tiposmovimientocuenta = function () {
        var dat = {'search':''};
        CONEXP('procesos','tiposmovimientocuenta/searchform').GET_LIST().$promise.then(function (r) {
            self.tiposmovimientocuenta = r;
        }, function (err) {
        });
    };
    self.list_tiposmovimientocuenta();

});