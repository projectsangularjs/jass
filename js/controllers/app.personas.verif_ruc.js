angular.module('app').controller('PersonasVerifRuc', function (obj, $scope, $mdDialog,
    toastr, $sce) {
    var self = this;

    self.ver_imagen = true;

    self.obj = obj;

    self.url_imagen = 'http://www.sunat.gob.pe/cl-ti-itmrconsruc/captcha?accion=image';

     self.cancel = function () {
        $mdDialog.cancel();
    };


    self.sunat_url_datos = function (codigo) {
    	var url = 'http://www.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias?nroRuc=' + obj.numero + '&accion=consPorRuc&actReturn=1&codigo=' + codigo;
        return $sce.trustAsResourceUrl(url);
    };

    self.change_imagen = function(){
    	var fecha = moment().format('MM/DD/YYYYHH:MI:SS');;
    	self.url_imagen = 'http://www.sunat.gob.pe/cl-ti-itmrconsruc/captcha?accion=image&'+fecha;
    };


});