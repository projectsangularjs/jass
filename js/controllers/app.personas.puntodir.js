
angular.module('app').controller('PersonasPuntodir', function (obj, $mdDialog,
    toastr, CONEXP, $ocLazyLoad) {
    var self = this;

    self.obj = {};
    self.cargado = true;

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.cargar_ini = function(punto_lat, punto_long){
        self.options = {
          map: {
            center: new google.maps.LatLng(punto_lat, punto_long),
            zoom: 17,
            mapTypeId: 'hybrid'
          },
          marker: function(p) {
            return {
              clickable: true,
              draggable: false,
              title: p.name,
              icon: 'recursos/imag/maps/person_maps.png'
            }
          }
        };
  };

    if(obj && obj['punto_lat']){
      self.ubicaciones = [{
          id: 0,
          name: 'Ubicación de la persona',
          punto_lat:  parseFloat(obj['punto_lat']),
          punto_long: parseFloat(obj['punto_long'])
        }];
        self.cargar_ini(parseFloat(obj['punto_lat']), parseFloat(obj['punto_long']));
    }else{
      self.ubicaciones = [{
          id: 0,
          name: 'Ubicación',
          punto_lat:  -6.473407058061334,
          punto_long: -76.39507067203522
        }];
      self.cargar_ini(-6.4734, -76.3950);
    }
  
    

  self.clickSelect = function(event) {
        self.ubicaciones = [{
          id: 0,
          name: 'Ubicación de la persona',
          punto_lat:  event.lat(),
          punto_long: event.lng()
        }
      ];
  };

  self.aceptar = function(){
      $mdDialog.hide(self.ubicaciones[0]);
  };

});
