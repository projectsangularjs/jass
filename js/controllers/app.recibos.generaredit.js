angular.module('app').controller('RecibosGenerarEdit', function ($mdDialog, toastr, CONEXP, $ocLazyLoad,
    localStorageService, obj, mes_recibo) {
    var self = this;

    var entidad_id = localStorageService.get('limon_entidad');
    self.obj = {};
    self.mes_recibo = mes_recibo;

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.cargado = false;
    self.new_listado = [];

   CONEXP('procesos','contratosrecibos/inforecibo').GET({id: obj['id']}).$promise.then(function (r) {
        self.obj = r;

        angular.forEach(r.listado, function(item) {
            item['cantidad'] = 1;
            item['total'] = item['precio'];
            self.new_listado.push(item);
        }, console.log);

        self.cargado = true;
    }, function (err) {
    }); 
    
    self.change_precios = function(){
        var total = 0;
        angular.forEach(self.new_listado, function(item) {
           total = total + (item['cantidad'] * item['precio']);
        }, console.log);
        self.obj['monto_cobranza'] = total;
    };

    self.creando = false;
    self.recibo = {};
    self.generar_recibo = function(){

        self.creando_mensaje = 'Generando recibo: '+ self.mes_recibo.mes.abreviatura+'-'+self.mes_recibo.anio+' / '+self.obj.persona.nombre_completo;

        self.obj['listado'] = self.new_listado;
        self.obj.entidad_id = entidad_id;
        self.obj.mes_recibo_id = self.mes_recibo.id;
        self.creando = true;

        CONEXP('procesos','recibos/addone').POST(self.obj).$promise.then(function (r) {
                toastr.success("Creado correctamente el recibo","Creado correctamente");
                self.recibo = r;
                self.creando = false;
            }, function (err) {
                self.creando = false;
                self.errores = err.data;
                toastr.error("No se pudo crear recibo","Error");
            });
    };

    self.opcion_pdf = function(opcion){
        var new_listado = [];
        new_listado.push(self.recibo['id']);

        var dat = {entidad_id:entidad_id, 'listado':new_listado};
        CONEXP('procesos','recibos/pdfrecibos').POST(dat).$promise.then(function (r) {
                // var win = window.open('', '_blank');
                r.content[0][0].pageBreak = 'none';
                if (opcion == 'ver'){
                    var win = window.open('', '_blank');
                    pdfMake.createPdf(r).open({}, win);
                }
                if (opcion == 'print'){
                    var win = window.open('', '_blank');
                    pdfMake.createPdf(r).print({}, win);
                }
                if (opcion == 'descargar'){
                    var nombre_file = moment().format('YYYYMMDDhmmssSSS')+".pdf";
                    pdfMake.createPdf(r).download(nombre_file);
                }

            }, function (err) {
               
            });
    };

});