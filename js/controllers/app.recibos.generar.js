angular.module('app').controller('RecibosGenerar', function ($mdDialog, toastr, CONEXP, $ocLazyLoad,
    localStorageService) {
    var self = this;

    var entidad_id = localStorageService.get('limon_entidad');

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.cargado = true;

   self.listado = [];
    self.search = '';

    self.pagination = {};

    self.paging = {
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
        estado:'2',
        ordering:'contrato'
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000','2000'];

    self.listar = function () {
        self.listado = [];
        self.Indeterminado = true;
        self.paging.search = self.search||'';
        self.paging.forma_cobranza = self.obj['forma_cobranza'];
        self.paging.entidad_id = entidad_id;
        self.paging.exclude_mes_recibo = self.obj.mes_recibo_id;
        
        angular.forEach(self.mesrecibos, function(item) {
                if(item['id'] === self.obj.mes_recibo_id){
                    self.paging.fecha_contrato_menor = item['fecha_emision'];
                }
          }, console.log);

        CONEXP('procesos','contratosrecibos').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.obj['all'] = false;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];
            self.creando = false;
            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    function loadPages() {
        self.listar();
    }

   self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
   };

   self.obj = {};
   self.formascobranza = [];

   self.list_formascobranza = function () {
        CONEXP('procesos','choices/formacobranza').GET_LIST().$promise.then(function (r) {
            if(r.length>0){
                self.obj['forma_cobranza'] = r[0]['id'];
            }
            self.formascobranza = r;
        }, function (err) {
        });
    };

    self.list_formascobranza();


    self.list_mesrecibos = function () {
        var dat = {
          'f':{'estado__in':['HAB']},
          'ordering':'mes_id,anio'};
        CONEXP('procesos','mesrecibos/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.mesrecibos = r;
            if(r.length >0 ){
              self.obj['mes_recibo_id'] = r[0]['id'];
            }
        }, function (err) {
        });
    };
    self.list_mesrecibos();

    self.change_all = function(obj){
        angular.forEach(self.listado, function(item) {
                if(obj['all']){
                    item['select'] = true;
                }else{
                    item['select'] = false;
                }
          }, console.log);
    };

    self.existe_alguno = function(listado){
        var existe = false;
        for (var i = 0, len = listado.length; i < len; i++) {
          if (listado[i]['select']) {
            existe = true;
            break;
          }
        }
        return existe;
    };

    self.cantidad_existe = function(listado){
        var cantidad = 0;
        for (var i = 0, len = listado.length; i < len; i++) {
          if (listado[i]['select']) {
            cantidad++;
          }
        }
        return cantidad;
    };

    self.creando = false;
    self.generar_recibos = function(listado){
        var new_listado = [];
        angular.forEach(listado, function(item) {
            if(item['select']){
                new_listado.push(item['id']);
            }
        }, console.log);
        self.obj['listado'] = new_listado;
        self.obj.entidad_id = entidad_id;
        self.creando = true;

        CONEXP('procesos','recibos/add').POST_LIST(self.obj).$promise.then(function (r) {
                toastr.success("Registrado correctamente","Registrado");
                self.listar_por();

            }, function (err) {
                self.creando = false;
                self.errores = err.data;
                toastr.error("No se pudo registrar","Error");
            });

    };

    self.generar_recibo = function(event, obj){
        var mes_recibo = {};

        angular.forEach(self.mesrecibos, function(item) {
            if(item['id'] == self.obj.mes_recibo_id){
                mes_recibo = item;
            }
        }, console.log);


        $ocLazyLoad.load([
            'js/controllers/app.recibos.generaredit.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "RecibosGenerarEdit",
                controllerAs: "form",
                templateUrl: 'views/recibos/generar_edit.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                fullscreen: true,
                multiple:true,
                locals:{obj: obj, mes_recibo: mes_recibo}
            }).then(function (result) {
                self.listar_por();
            }, function () {
                self.listar_por();
            });
        });
    };

});