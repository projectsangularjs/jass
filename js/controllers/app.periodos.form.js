angular.module('app').controller('PeriodosForm', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;
    self.errores = [];

    if (obj){
       CONEXP('comun','periodos/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            fi = Date.parse(self.obj['fecha_inicio']);
            ff = Date.parse(self.obj['fecha_fin']);
            fecha_inicio = new Date(fi);
            fecha_fin = new Date(ff);
            self.obj['fecha_inicio'] = fecha_inicio;
            self.obj['fecha_fin'] = fecha_fin;
            self.cargado = true;
        }, function (err) {
        });
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.errores = [];
        self.guardando = true;
        self.obj['fecha_inicio'] = moment(self.obj['fecha_inicio']).format("YYYY-MM-DD");

        if(self.obj['fecha_fin']){
            self.obj['fecha_fin'] = moment(self.obj['fecha_fin']).format("YYYY-MM-DD");
        }
        console.log(self.obj);
        if(self.obj.id){
            CONEXP('comun','periodos/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('comun','periodos/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                self.errores = err.data;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

});