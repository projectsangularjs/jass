angular.module('app').controller('Misrecibos', function (
    toastr, CONEXP, $ocLazyLoad, $mdDialog, config, localStorageService) {
	var self = this;
	self.storage = config.url_api_storage;
    var entidad_id = localStorageService.get('limon_entidad');
    
	self.listado = [];
    self.Indeterminado = true;

    self.listar = function () {
        self.Indeterminado = true;
        CONEXP('procesos','recibos/misrecibos').GET_LIST({entidad_id:entidad_id}).$promise.then(function (r) {
            self.listado = r;
            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    self.listar();

    self.opcion_pdf = function(opcion, recibo_id){
        self.Indeterminado = true;
        var dat = {entidad_id:entidad_id, 'listado':[recibo_id]};
        CONEXP('procesos','recibos/pdfrecibos').POST(dat).$promise.then(function (r) {
                r.content[0][0].pageBreak = 'none';
                if (opcion == 'ver'){
                    var win = window.open('', '_blank');
                    pdfMake.createPdf(r).open({}, win);
                    self.Indeterminado = false;
                }
                if (opcion == 'print'){
                    var win = window.open('', '_blank');
                    pdfMake.createPdf(r).print({}, win);
                    self.Indeterminado = false;
                }
                if (opcion == 'descargar'){
                    var nombre_file = moment().format('YYYYMMDDhmmssSSS')+".pdf";
                    pdfMake.createPdf(r).download(nombre_file);
                    self.Indeterminado = false;
                }

            }, function (err) {
               
            });
    };

});