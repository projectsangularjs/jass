angular.module('app').controller('Pedidos', function (CONEX, $stateParams) {

    var self = this;
    self.params = $stateParams;
    self.cargado = false;

    CONEX('reportes/pedidos').GET({id: self.params['id']}).$promise.then(function (r) {
        self.obj = r['data'];
        self.cargado = true;
    }, function (err) {
    }); 

    
 });