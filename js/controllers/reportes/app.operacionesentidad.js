angular.module('app').controller('Operacionesentidad', function (CONEXP, $stateParams,localStorageService) {

    var self = this;
    self.params = $stateParams;
    // self.id = self.params['id'];
    self.obj = {};
    self.cargado = false;
    self.obj = {};

    var entidad_id = localStorageService.get('limon_entidad');

    var data = {
        'entidad_id':entidad_id,
        'fecha_inicial':self.params['fecha_inicial'],
        'fecha_final':self.params['fecha_final']
    }

     CONEXP('caja','movimientoscuenta/informemovimientos').GET(data).$promise.then(function (r) {
        self.obj = r;
        self.cargado = true;
         // self.listar();
    }, function (err) {
    }); 

 });