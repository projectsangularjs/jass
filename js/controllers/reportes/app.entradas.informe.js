angular.module('app').controller('EntradasInforme', function (CONEX, $stateParams) {

    var self = this;
    
    self.mov = {};
    self.search = '';
    self.params = $stateParams;
    self.id = self.params['id'];
    self.guias = [];
    self.conceptos = [];
    self.tributos = [];
    self.detalles = [];

    self.list_conceptos = function(){
        CONEX('movimientosconcepto/list/movimiento').GET_LIST({movimiento_id: self.params.id}).$promise.then(function (r) {
            self.conceptos = r;
            self.Indeterminado = true;
        }, function (err) {
        });
    };

    self.list_guias = function(){
        CONEX('movimientosguia/list/movimiento').GET_LIST({movimiento_id: self.params.id}).$promise.then(function (r) {
            self.guias = r;
            self.list_conceptos();
        }, function (err) {
        });
    };

    self.list_tributos = function(){
        CONEX('movimientosdetalletributo/list/tributos').GET_LIST({movimiento_id: self.params.id}).$promise.then(function (r) {
            self.tributos = r;
        }, function (err) {
        });
    };

    self.list_detalles = function () {
        CONEX('movimientosdetalle/list/entradas').GET_LIST({movimiento_id: self.params.id}).$promise.then(function (r) {
            self.detalles = r;
        }, function (err) {
        });
    };

    self.get_movimiento = function () {
        CONEX('movimientos').GET({id: self.params.id}).$promise.then(function (r) {
            self.mov = r['data'];
            self.cargado = true;
        }, function (err) {
        });
    };
    
    self.get_movimiento();
    self.list_detalles();
    self.list_tributos();
    self.list_guias();
    
});