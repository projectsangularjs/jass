angular.module('app').controller('CronogramascobranzaForm', function (obj, $scope, $mdDialog,
    CONEXP, $ocLazyLoad, toastr, config, localStorageService) {
    var self = this;
    self.storage = config.url_api_storage;       

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;
    var entidad_id = localStorageService.get('limon_entidad');

    if (obj){
       CONEXP('procesos','cronogramaspago/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            if(r.fecha_cobranza){
                self.obj['fecha_cobranza_'] = moment(r.fecha_cobranza, 'YYYY-MM-DD').toDate();
            }
            if(r.monto){
                self.obj['monto'] = parseFloat(r.monto)
            }
            self.cargado = true;
        }, function (err) {
        }); 
    }else{
        self.cargado = true;
    }

    
    self.save_or_update = function () {
        self.guardando = true;
        self.obj['entidad_id'] = entidad_id;
        self.obj['persona_id'] = self.obj.persona['id'];
        self.obj['motivo_movimiento_cuenta_id'] = self.obj.motivo_movimiento_cuenta['id'];
        self.obj['estado_cronograma_id'] = self.obj.estado_cronograma['id'];

        if (self.obj['fecha_cobranza_']){
            self.obj['fecha_cobranza'] = moment(self.obj['fecha_cobranza_']).format("YYYY-MM-DD");
        }else{
            self.obj['fecha_cobranza'] = null;
        };
        
        if(self.obj.contrato){
            self.obj['contrato_id'] = self.obj.contrato['id'];
        }else{
            self.obj['contrato_id'] = null;
        }           
        if(self.obj.padre){
            self.obj['padre_id'] = self.obj.padre['id'];
        }else{
            self.obj['padre_id'] = null;
        }

        if(self.obj.id){
            CONEXP('procesos','cronogramaspago/update').PUT(self.obj).$promise.then(function (r) {
                
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('procesos','cronogramaspago/add').POST(self.obj).$promise.then(function (r) {
                console.log(r);
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};
    self.BuscarMotivoMovCuenta = function (search) {
        filtros = {};
        filtros['search'] = search;
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1',tipo_movimiento_cuenta: '1'};
        filtros['ordering'] = "-nombre";
        return CONEXP('procesos','motivosmovimientocuenta/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    var filtros = {};
    self.BuscarPersona = function (search) {
        filtros = {};
        filtros['search'] = search;
        filtros['limit'] = 10;
        filtros['q'] = {nombre_completo__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        filtros['ordering'] = "-nombre_completo";
        return CONEXP('comun','personas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };
    self.BuscarEstadoCronograma = function (search) {
        filtros = {};
        filtros['search'] = search;
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        filtros['ordering'] = "-nombre";
        return CONEXP('procesos','estadoscronograma/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarContrato = function (search, persona_id) {
        filtros = {};
        filtros['search'] = search;
        filtros['q'] = {numero__icontains: search};
        filtros['f'] = {estado: '2',persona_id: persona_id};
        filtros['ordering'] = "numero";
        return CONEXP('procesos','contratos/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.form_persona = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.personas.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasForm",
                controllerAs: "form",
                templateUrl: 'views/personas/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                multiple:true,
                locals: {obj: obj},
                fullscreen: true,
            }).then(function (result) {
                self.obj['persona'] = result;
            }, function () {
            });
        });
    };

    self.change_persona = function(){
        self.obj['contrato'] = null;
    };

    
});