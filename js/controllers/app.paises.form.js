angular.module('app').controller('PaisesForm', function (obj, $scope, $mdDialog,
    CONEXP, toastr) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;

    if (obj){
       CONEXP('comun','paises/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            self.cargado = true;
        }, function (err) {
        }); 
    }else{
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.guardando = true;
        if(self.obj.continente){
            self.obj['continente_id'] = self.obj.continente['id'];
        }else{
            self.obj['continente_id'] = null;
        } 

        if(self.obj.id){
            CONEXP('comun','paises/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('comun','paises/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};
    self.BuscarContinente = function (search) {
        filtros['search'] = search;
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        filtros['ordering'] = "-nombre";
        return CONEXP('comun','continentes/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    
});