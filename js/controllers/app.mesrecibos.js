angular.module('app').controller('MesRecibos', function ($scope, $mdDialog, toastr, CONEXP, $ocLazyLoad) {
    var self = this;

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.seleccionar = function(obj){
        $mdDialog.hide(obj);
    };

    self.listado = [];
    self.search = '';

    self.pagination = {};

    self.paging = {
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000','2000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.paging.search = self.search||'';
        self.paging.ordering = '-anio,-mes';
        CONEXP('procesos','mesrecibos').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];

            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    function loadPages() {
        self.listar();
    }

   self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
   };

   self.new_edit = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.mesrecibos.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "MesRecibosForm",
                controllerAs: "form",
                templateUrl: 'views/mesrecibos/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj},
                multiple: true
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };

    self.delete = function (event, obj) {
        var confirm = $mdDialog.confirm({multiple: true})
                .title("Eliminar mes recibo")
                .textContent("¿Está seguro de eliminar el mes de " + obj['mes']['nombre'] + "?")
                .ariaLabel('delete')
                .targetEvent(event)
                .ok('SI')
                .cancel('NO');
        $mdDialog.show(confirm).then(function () {
            CONEXP('procesos','mesrecibos/delete').DELETE({id: obj.id}).$promise.then(function (r) {
               if(r){
                     toastr.success('Eliminado correctamente', 'Eliminado');
                    self.listar();
                }
            }, function (err) {
                toastr.error('No se pudo eliminar', 'Error');
            });
        }, function () {
        });
    };

});