angular.module('app').controller('MotivosmovimientocuentaForm', function (obj, $scope, $mdDialog,
    CONEXP, toastr) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;
    self.tiposmovimientocuenta = [];

    if (obj){
       CONEXP('procesos','motivosmovimientocuenta/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            self.cargado = true;
        }, function (err) {
        }); 
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.guardando = true;
        if(self.obj.tipo_movimiento_cuenta){
            self.obj['tipo_movimiento_cuenta_id'] = self.obj.tipo_movimiento_cuenta['id'];
        }else{
            self.obj['tipo_movimiento_cuenta_id'] = null;
        } 

        if(self.obj.id){
            CONEXP('procesos','motivosmovimientocuenta/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('procesos','motivosmovimientocuenta/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};
    self.BuscarTipoMovimientoCuenta = function (search) {
        filtros['search'] = search;
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        filtros['ordering'] = "-nombre";
        return CONEXP('procesos','tiposmovimientocuenta/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    
});