angular.module('app').controller('EntidadForm', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;

    if (obj){
       CONEXP('comun', 'entidades/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            self.cargado = true;
            self.edit = true;
        }, function (err) {
        });
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.guardando = true;
        if(self.obj.id){
            CONEXP('comun', 'entidades/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(self.obj);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('comun', 'entidades/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};

    self.BuscarTipoEntidad = function (search) {
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('comun','tiposentidad/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarPersona = function (search) {
        filtros['limit'] = 10;
        filtros['q'] = {nombre_completo__icontains: search, persona_persona_documentos__numero__icontains: search};
        filtros['f'] = {estado: '1', tipo_persona__codigo:'02'};
        return CONEXP('comun','personas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    /*self.change_pais = function(obj) {
        self.obj['ubigeo'] = null;
    }*/

    self.BuscarPadre = function(search, persona_id){
        filtros = {};
        filtros['q'] = {persona__nombre_completo__icontains: search};
        filtros['f'] = {estado:'1'};
        filtros['limit'] = 10;
        return CONEXP('comun','entidades/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarMoneda = function(search, pais_id){
        filtros = {};
        filtros['q'] = {codigo__icontains: search, nombre__icontains:search};
        filtros['f'] = {estado:'1'};
        filtros['limit'] = 10;
        return CONEXP('comun','monedas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

});