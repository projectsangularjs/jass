angular.module('app').controller('Selentidad', function ($mdDialog,
    CONEXP, toastr, localStorageService) {
    var self = this;

    self.obj = {};
    self.cargado = false;
    self.entidad_id = localStorageService.get('limon_entidad');

    self.entidades = [];

    CONEXP('segur','functions/misentidades').GET_LIST().$promise.then(function (r) {
    	self.entidades = r;
    	self.cargado = true;
    }, function (err) {
    	self.cargado = true;
    });

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.guardar = function(){
    	$mdDialog.hide(self.entidad_id);
    };

    self.selentidad = function(id){
    	self.entidad_id = id;
    };

});