angular.module('app').controller('EstadosCuenta', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, $state, localStorageService) {

    var self = this;
    self.Indeterminado = true;
    self.listado = [];
    self.search = '';
    var entidad_id = localStorageService.get('limon_entidad');

    self.list_ordenar = [
        {id:'-deuda',titulo:'Deuda mayor'},
        {id:'deuda',titulo:'Deuda menor'},
        {id:'persona__nombre_completo',titulo:'Nombre completo'},
        {id:'persona__persona_persona_juridica__razon_social,persona__persona_persona_natural__papellido,persona__persona_persona_natural__sapellido',titulo:'Apellidos y nombres'},
    ];

    self.ordering = self.list_ordenar[0]['id'];

    self.pagination = {};

    self.paging = {
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
        entidad_id: entidad_id
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.paging.search = self.search||'';
        self.paging.ordering = self.ordering;
        CONEXP('caja', 'estadoscuenta').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];

            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    // self.listar();

    function loadPages() {
        self.listar();
    }

    self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
    };

    self.ordenar = function(id){
        self.ordering = id;
        self.listar_por();
   };

    self.movimientos = function($event, obj){
        $ocLazyLoad.load([
            'js/controllers/app.estadoscuenta.movimientos.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "EstadosCuentaMovimiento",
                controllerAs: "form",
                templateUrl: 'views/estadoscuenta/movimientos.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {persona: obj},
                fullscreen: true,
            }).then(function (result) {
                
            }, function () {
            });
        });
    };

    // self.new_contrato = function (id) {
    //     $state.go('app.addcontrato.registro');
    // };

});