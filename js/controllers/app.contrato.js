angular.module('app').controller('Contrato', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, $state, config, localStorageService) {
	/*GENERAL*/
    var self = this;
    self.storage = config.url_api_storage;
    var filtros = {};
    self.obj = {};
    /* ENTIDAD*/
    var entidad_id = localStorageService.get('limon_entidad');
    /*******************************/
    self.contrato = {};
    self.persona_contrato = {};
    var codigo_servicio = 'ServAguaPo';
    self.forma_cobranza = [];
    self.forma_cobranza_ = {};
    self.agua_potable = {};
    self.electrobomba = {};
    self.inscripcion = {};
    self.precio_sin_med;
    self.tipo_uso_servicio = [];
    self.nro_cuota = 1;
    /*self.predios = [];*/
    self.predios = [];
    self.servicios = [];
    self.servicio_1 = {};
    self.servicio_2 = {};
    self.servicio_3 = {};
    self.crono_pago = [];
    self.cronograma_pago = [];
    /*var fecha_cobranza;*/
    /*******************************/

    self.BuscarPersona = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre_completo__icontains: search, persona_persona_documentos__numero__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('comun','personas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.change_persona = function(pers_id) {
        self.get_datos_persona(pers_id);
        self.personaContrato(pers_id);
        self.list_predios_usuario(pers_id);
        self.contrato['persona_id'] = pers_id;
    }

    self.get_datos_persona = function(pers_id){
        CONEXP('comun','personas/viewinfo').GET({id: pers_id}).$promise.then(function (r) {
            self.persona = r;
            self.cargado = true;
        }, function (err) {
        });
    };
    /*********** PREDIOS ******************************************/
    self.personaContrato = function (pers_id) {
        filtros = {};
        filtros['f'] = {estado: '1', persona_id: pers_id};
        return CONEXP('procesos','contratos/searchform').GET_LIST(filtros).$promise.then(function (r) {
            self.persona_contrato = r;
        });
    };

    self.list_predios_usuario = function(pers_id){
        filtros = {};
        filtros['q'] = {duenio_id: pers_id, representante_id: pers_id} ;
        filtros['f'] = {estado: '1'};
        CONEXP('procesos','predios/searchform').GET_LIST(filtros).$promise.then(function (r) {
            console.log('predio.id = '+self.persona_contrato);
            console.log('predio.id = '+self.persona_contrato[0]);
            console.log('predio.id = '+self.persona_contrato[0].predio);
            console.log('predio.id = '+self.persona_contrato[0].predio['id']);
            self.predios = r;
            for (var i = 0; i < r.length; i++) {
                if (r[i]['id'] === self.persona_contrato[0].predio.id) {
                    self.predios[i]['disable'] = true;
                } else {
                    self.predios[i]['disable'] = false;
                }
            }
            console.log(self.predios);
            var fechacobranza, monto;
            var fecha_cobranza = new Date(fechacobranza);
            var crono = {"monto": monto, "fecha_cobranza": fecha_cobranza};
            self.crono_pago.push(crono);
        }, function (err) {
        });
    };

    self.change_predios = function (pred_id) {
    	self.get_datos_predio(pred_id);
        if (codigo_servicio == 'ServAguaPo') {
            self.contrato['entidad_id'] = entidad_id;
            self.contrato['nro_cuotas'] = self.nro_cuota;
            self.BuscarTipoUsoServicio();
            self.list_forma_cobranza();
            self.ListarServiciosGenerales();
        }
    }

    self.get_datos_predio = function(pred_id){
    	filtros = {};
    	filtros['f'] = {id: pred_id};
        CONEXP('procesos','predios/searchform').GET_LIST(filtros).$promise.then(function (r) {
	        self.predio = r[0];
	        self.cargado = true;
        }, function (err) {
        });
    };


    /*self.BuscarOtrosPredios = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {partida_registral__predio_direcciones__direccion_detalle__icontains: search};
        filtros['f'] = {estado: '1', duenio_id: null, representante_id: null};
        return CONEXP('procesos','predios/searchform').GET_LIST(filtros).$promise.then(function (r) {
        	console.log(r);

            return r;
        });
    };*/

    /*** TIPO DE SERVICIO Y PRECIOS:    **********************************/
    self.BuscarTipoUsoServicio = function () {
        filtros = {};
        filtros['f'] = {estado: '1'};
        return CONEXP('procesos','tiposusoservicio/searchform').GET_LIST(filtros).$promise.then(function (r) {
            self.tipo_uso_servicio = r;
            return r;
        });
    };

    self.list_forma_cobranza = function () {
        CONEXP('procesos','choices/formacobranza').GET_LIST().$promise.then(function (r) {
            self.contrato['forma_cobranza'] = 'SME';
            self.forma_cobranza = r;
        }, function (err) {
        });
    };

    self.ListarServiciosGenerales = function(){
        filtros = {};
        filtros['f'] = {estado:'1', codigo__in:['Electrobom','ServAguaPo','Inscrip']};
        return CONEXP('procesos','servicios/searchform').GET_LIST(filtros).$promise.then(function (r) {
            for (var i = r.length - 1; i >= 0; i--) {
                if (r[i]['codigo'] === 'ServAguaPo') {
                    self.agua_potable = r[i];
                    self.agua_potable.check = true;
                    self.precio_sin_med = r[i]['precio'];
                } else if (r[i]['codigo'] === 'Electrobom') {
                    self.electrobomba = r[i];
                    self.electrobomba.check = false;
                } else if (r[i]['codigo'] === 'Inscrip') {
                    self.inscripcion = r[i];
                }
            }
            //self.inscripcion['precio'] = null;
            return r;
        });
    };

    self.change_forma_cobranza = function(forma_cobranza) {
    	self.contrato['forma_cobranza'] = forma_cobranza;
        if (forma_cobranza === 'CME') {
            self.agua_potable.precio = null;
        } else if (forma_cobranza === 'SME'){
            self.agua_potable.precio = self.precio_sin_med;
        }
    }

    self.change_nro_cuotas = function() {
         self.crono_pago = [];
        for (var i = 0; i < self.contrato['nro_cuotas']; i++) {
            var fechacobranza, monto;
            var fecha_cobranza = new Date(fechacobranza);
            var crono = {"monto": monto, "fecha_cobranza": fecha_cobranza};
            self.crono_pago.push(crono);
        }
    }

    /* GUARDAR O ACTUALIZAR */
    self.save_or_update = function () {
        self.guardando = true;
        if (self.agua_potable.check) {
            self.servicio_1.servicio_id =self.agua_potable.id;
            self.servicio_1.precio = self.agua_potable.precio;
            self.servicios[0] = self.servicio_1;
        }
        if (self.electrobomba.check) {
            self.servicio_2.servicio_id =self.electrobomba.id;
            self.servicio_2.precio = self.electrobomba.precio;
            self.servicios[1] = self.servicio_2;
            self.servicio_3.servicio_id =self.inscripcion.id;
            self.servicio_3.precio = self.inscripcion.precio;
            self.servicios[2] = self.servicio_3;
        } else {
            self.servicio_3.servicio_id =self.inscripcion.id;
            self.servicio_3.precio = self.inscripcion.precio;
            self.servicios[1] = self.servicio_3;
        }


        if(self.obj.id){
        	console.log('ACTUALIZAR');
        	console.log(self.obj);
            /*CONEXP('comun', 'entidades/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(self.obj);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });*/
        }else{
            self.cronograma_pago = self.crono_pago;
            if (self.crono_pago.length > 0) {
                for (var i = 0; i < self.crono_pago.length; i++) {
                    var date_fecha = self.crono_pago[i]['fecha_cobranza'];
                    self.cronograma_pago[i]['monto'] =self.crono_pago[i]['monto'];
                    self.cronograma_pago[i]['fecha_cobranza'] = moment(date_fecha).format("YYYY-MM-DD");
                }
            }
            /*moment(self.cronograma_pago['fecha_cobranza']).format("YYYY-MM-DD");*/
            self.obj['contrato'] = self.contrato;
            self.obj['servicios'] = self.servicios;
            self.obj['cronograma_pago'] = self.cronograma_pago;
            CONEXP('procesos', 'contratos/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

});
/*

*/

