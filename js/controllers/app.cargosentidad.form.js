angular.module('app').controller('CargosEntidadForm', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP, config, localStorageService, $ocLazyLoad) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.storage = config.url_api_storage;
    var entidad_id = localStorageService.get('limon_entidad');
    self.entidad = {"id": entidad_id};
    self.cargado = false;
    self.guardando = false;

    if (obj){
       CONEXP('comun', 'cargosentidad/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            self.cargado = true;
            self.edit = true;
        }, function (err) {
        });
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.obj['entidad'] = self.entidad;
        self.guardando = true;
        if(self.obj.id){
            CONEXP('comun', 'cargosentidad/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(self.obj);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('comun', 'cargosentidad/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};

    self.BuscarPeriodo = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('comun','periodos/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarPersona = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre_completo__icontains: search, persona_persona_documentos__numero__icontains: search};
        filtros['f'] = {estado: '1', tipo_persona__codigo:'01'}
        return CONEXP('comun','personas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarCargo = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('comun','cargos/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.form_persona = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.personas.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasForm",
                controllerAs: "form",
                templateUrl: 'views/personas/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                multiple:false,
                locals: {obj: obj},
                fullscreen: true,
                multiple: true
            }).then(function (result) {
                self.obj['persona'] = result;
            }, function () {
            });
        });
    };

});