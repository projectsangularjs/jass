angular.module('app').controller('UsersRoles', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP) {
    var self = this;

    self.obj = obj;
    self.cargado = false;
    self.guardando = false;

    self.obj['roles'] = [];
    self.obj['entidades'] = [];

    CONEXP('segur','usuarios/editroles').GET({user_id: obj['id']}).$promise.then(function (r) {
        self.obj['roles'] = r['groups'];
        self.obj['entidades'] = r['entidades'];
        self.cargado = true;
    }, function (err) {
    });

    self.save_or_update = function () {
        self.guardando = true;
        var dat = {
            'user_id':obj['id'],
            'groups':self.obj['roles']||[],
            'entidades':self.obj['entidades']||[]
        };

        CONEXP('segur','usuarios/updateroles').PUT_LIST(dat).$promise.then(function (r) {
            self.guardando = false;
            toastr.success(r.length+" roles asignados ","Registrado");
             $mdDialog.hide(r);
        }, function (err) {
            self.guardando = false;
            toastr.error("No se pudo registrar","Error");
        });
    };

    self.return_val = function(listado){
        var new_array = [];
        angular.forEach(listado, function(item) {
          new_array.push(item['id']);
        }, console.log);
        return new_array;
    }

    self.BuscarRoles = function (search, roles) {
        var data = {
            'q': {'name__icontains': search},
            'limit':20,
            'e':{'id__in':self.return_val(roles||[])}
        }
        return CONEXP('segur','groups/searchform').GET_LIST(data).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarEntidades = function (search, entidades) {
        var data = {
            'q': {'persona__nombre_completo__icontains': search,
                'persona__persona_persona_documentos__numero__icontains':search},
            'limit':10,
            'e':{'id__in':self.return_val(entidades||[])}
        };

        return CONEXP('comun','entidades/searchform').GET_LIST(data).$promise.then(function (r) {
            return r;
        });
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

});