angular.module('app').controller('UsersChangepass', function (obj, $scope, $mdDialog,
    toastr, CONEXP, $ocLazyLoad) {
    var self = this;

    self.obj = obj;
    self.cargado = false;
    self.guardando = false;

    self.cargado = true;

    self.save_or_update = function () {
        self.guardando = true;
        self.obj['user_id'] = self.obj['id'];

        CONEXP('segur','usuarios/changepassword').POST(self.obj).$promise.then(function (r) {
            self.guardando = false;
            if(r){
                toastr.success("Contraseña cambiada correctamente","Actualizado");
                 $mdDialog.hide(r.data);
            }
        }, function (err) {
            self.guardando = false;
            toastr.error("No se pudo actualizar","Error");
        });

    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

});