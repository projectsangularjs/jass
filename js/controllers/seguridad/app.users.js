angular.module('app').controller('Users', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog) {

    var self = this;
    self.Indeterminado = true;
    self.listado = [];
    self.search = '';

    self.pagination = {};

    self.paging = {
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.paging.search = self.search||'';
        CONEXP('segur','usuarios').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];

            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    // self.listar();

    function loadPages() {
        self.listar();
    }

   self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
   };

    self.new_edit = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/seguridad/app.users.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "UsersForm",
                controllerAs: "form",
                templateUrl: 'views/users/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj}
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };

    self.changepass = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/seguridad/app.users.changepass.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "UsersChangepass",
                controllerAs: "form",
                templateUrl: 'views/users/changepass.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj}
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };

    self.roles = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/seguridad/app.users.roles.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "UsersRoles",
                controllerAs: "form",
                templateUrl: 'views/users/roles.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj}
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };

    self.permisos = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/seguridad/app.users.permisos.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "UsersPermisos",
                controllerAs: "form",
                templateUrl: 'views/users/permisos.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj}
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };

    self.delete = function (event, obj) {
        var confirm = $mdDialog.confirm()
                .title("Eliminar usuario")
                .textContent("¿Está seguro de eliminar el usuario " + obj['username'] + "?")
                .ariaLabel('delete')
                .targetEvent(event)
                .ok('SI')
                .cancel('NO');
        $mdDialog.show(confirm).then(function () {
            CONEXP('segur','usuarios/delete').DELETE({id: obj.id}).$promise.then(function (r) {
               if(r){
                     toastr.success('Eliminado correctamente', 'Eliminado');
                    self.listar();
                }
            }, function (err) {
                toastr.error('No se pudo eliminar', 'Error');
            });
        }, function () {
        });
    };

});