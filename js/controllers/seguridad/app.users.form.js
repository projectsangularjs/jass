angular.module('app').controller('UsersForm', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP, $ocLazyLoad, config) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;
    self.storage = config.url_api_storage;

    if (obj){
       CONEXP('segur','usuarios/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            self.cargado = true;
        }, function (err) {
        }); 
    }else{
        self.obj['is_active'] = '1';
        self.obj['is_staff'] = '0';
        self.obj['is_superuser'] = '0';
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.guardando = true;
        self.obj['persona_id'] = self.obj['persona']['id'];
        if(self.obj.id){
            CONEXP('segur','usuarios/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('segur','usuarios/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};
    self.BuscarPersona = function (search, type) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre_completo__icontains: search, 
            persona_persona_documentos__numero__icontains: search};
        filtros['f'] = {estado: '1',tipo_persona__codigo:'01'};
        filtros['ordering'] = 'nombre_completo';
        return CONEXP('comun','personas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.form_persona = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.personas.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasForm",
                controllerAs: "form",
                templateUrl: 'views/personas/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                multiple:true,
                locals: {obj: obj},
                fullscreen: true,
            }).then(function (result) {
                self.obj['persona'] = result;
            }, function () {
            });
        });
    };

});