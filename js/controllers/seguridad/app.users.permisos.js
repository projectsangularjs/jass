angular.module('app').controller('UsersPermisos', function (obj, $scope, $mdDialog,
    toastr, CONEXP) {
    var self = this;

    self.obj = obj;
    self.cargado = false;
    self.guardando = false;

    self.obj['permisos'] = [];

    CONEXP('segur','usuarios/editpermissions').GET_LIST({user_id: obj['id']}).$promise.then(function (r) {
        self.obj['permisos'] = r;
        self.cargado = true;
    }, function (err) {
    });

    self.save_or_update = function () {
        self.guardando = true;
        var dat = {
            'user_id':obj['id'],
            'permisos':self.obj['permisos']||[]
        }

        CONEXP('segur','usuarios/updatepermissions').PUT_LIST(dat).$promise.then(function (r) {
            self.guardando = false;
            toastr.success(r.length+" permisos asignados ","Registrado");
             $mdDialog.hide(r);
        }, function (err) {
            self.guardando = false;
            toastr.error("No se pudo registrar","Error");
        });
    };

    self.return_val = function(permisos){
        var new_array = [];
        angular.forEach(permisos, function(item) {
          new_array.push(item['id']);
        }, console.log);
        return new_array;
    }

    self.BuscarPermisos = function (search, permisos) {
        var data = {
            'q': {'name__icontains': search, 'codename__icontains': search},
            'search': search, 
            'limit':20,
            'e':{'id__in':self.return_val(permisos||[])}
        }
        return CONEXP('segur','permissions/searchform').GET_LIST(data).$promise.then(function (r) {
            return r;
        });
    };


    self.cancel = function () {
        $mdDialog.cancel();
    };

});