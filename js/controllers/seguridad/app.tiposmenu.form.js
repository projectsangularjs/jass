angular.module('app').controller('TiposmenuForm', function (obj, $scope, $mdDialog,
    CONEXP, toastr) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;

    if (obj){
       CONEXP('segur','tiposmenu/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            self.cargado = true;
        }, function (err) {
        }); 
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.guardando = true;
        if(self.obj.id){
            CONEXP('segur','tiposmenu/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('segur','tiposmenu/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

});