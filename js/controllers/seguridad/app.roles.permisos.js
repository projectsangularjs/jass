angular.module('app').controller('RolesPermisos', function (obj, $scope, $mdDialog,
    CONEX, toastr, CONEXP) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;

    // $scope.keys_separ = [$mdConstant.KEY_CODE.ENTER, $mdConstant.KEY_CODE.COMMA];


    if (obj){
       CONEXP('segur','groups/editpermission').GET_LIST({rol_id: obj['id']}).$promise.then(function (r) {
            self.obj['permisos'] = r;
            self.cargado = true;
        }, function (err) {
        }); 
    }else{
        self.obj['permisos'] = [];
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.guardando = true;
        var dat = {
            'rol_id':obj['id'],
            'permisos':self.obj['permisos']||[]
        }

        CONEXP('segur','groups/updatepermission').PUT_LIST(dat).$promise.then(function (r) {
            self.guardando = false;
            toastr.success(r.length+" permisos asignados a rol ","Registrado");
             $mdDialog.hide(r);
        }, function (err) {
            self.guardando = false;
            toastr.error("No se pudo registrar","Error");
        });
    };

    self.return_val = function(permisos){
        var new_array = [];
        angular.forEach(permisos, function(item) {
          new_array.push(item['id']);
        }, console.log);
        return new_array;
    }

    self.BuscarPermisos = function (search, permisos) {
        var data = {
            'q': {'name__icontains': search, 'codename__icontains': search},
            'search': search, 
            'limit':20,
            'e':{'id__in':self.return_val(permisos||[])}
        }
        return CONEXP('segur','permissions/searchform').GET_LIST(data).$promise.then(function (r) {
            return r;
        });
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

});