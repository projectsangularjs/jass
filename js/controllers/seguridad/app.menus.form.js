angular.module('app').controller('MenusForm', function (obj, $scope, $mdDialog,
    CONEXP, toastr) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;

    if (obj){
       CONEXP('segur','menus/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            self.cargado = true;
        }, function (err) {
        }); 
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }

    self.save_or_update = function () {
        self.guardando = true;
        self.obj['modulo_id'] = self.obj.modulo['id'];
        self.obj['tipo_menu_id'] = self.obj.tipo_menu['id'];

        if(self.obj['padre']){
            self.obj['padre_id'] = self.obj.padre['id'];
        }else{
            self.obj['padre_id'] = null;
        }

        if(self.obj['permiso']){
            self.obj['permiso_id'] = self.obj.permiso['id'];
        }else{
            self.obj['permiso_id'] = null;
        }

        if(self.obj.id){
            CONEXP('segur','menus/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('segur','menus/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    var filtros = {};
    self.BuscarModulo = function (search) {
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('segur','modulos/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarTipoMenu = function (search) {
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        return CONEXP('segur','tiposmenu/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarPadre = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1',tipo_menu__codigo:'toggle'};
        filtros['ordering'] = 'orden';
        return CONEXP('segur','menus/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarPermiso = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {name__icontains: search, codename__icontains: search};
        return CONEXP('segur','permissions/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

});