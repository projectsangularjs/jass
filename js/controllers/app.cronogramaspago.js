angular.module('app').controller('cronogramaspago', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog) {

    var self = this;
    self.Indeterminado = true;
    self.listado = [];
    self.search = '';
    self.motivosmovimientocuenta = [];
    self.cronogramaspago = [];
    self.motivo_movimiento_cuenta_id = 'T';

    self.pagination = {};

    self.paging = {
        motivo_movimiento_cuenta__tipo_movimiento_cuenta__codigo: 'EGR',
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.paging.search = self.search||'';
        self.paging.ordering = 'persona__nombre';

        if(self.date){
                self.paging['fecha_cobranza'] = moment(self.date).format("YYYY-MM-DD");
        }else{
            delete self.paging['fecha_cobranza'];
        }

        if(self.motivo_movimiento_cuenta_id !=='T'){
            self.paging['motivo_movimiento_cuenta_id'] = self.motivo_movimiento_cuenta_id;
        }else{
            delete self.paging['motivo_movimiento_cuenta_id'];
        }

        CONEXP('procesos','cronogramaspago').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];

            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    // self.listar();

    function loadPages() {
        self.listar();
    }

   self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
   };

    self.new_edit = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.cronogramaspago.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "CronogramaspagoForm",
                controllerAs: "form",
                templateUrl: 'views/cronogramaspago/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj}
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };

    self.delete = function (event, obj) {
        var confirm = $mdDialog.confirm()
                .title("Eliminar Cronograma")
                .textContent("¿Está seguro de eliminar el cronograma" + obj['nombre'] + "?")
                .ariaLabel('delete')
                .targetEvent(event)
                .ok('SI')
                .cancel('NO');
        $mdDialog.show(confirm).then(function () {
            CONEXP('procesos','cronogramaspago/delete').DELETE({id: obj.id}).$promise.then(function (r) {
               if(r){
                     toastr.success('Eliminado correctamente', 'Eliminado');
                    self.listar();
                }
            }, function (err) {
                toastr.error('No se pudo eliminar', 'Error');
            });
        }, function () {
        });
    };
    self.list_motivosmovimientocuenta = function () {
        var dat = {'search':'',f:{tipo_movimiento_cuenta_id__in:['2']}};
        CONEXP('procesos','motivosmovimientocuenta/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.motivosmovimientocuenta = r;
        }, function (err) {
        });
    };
    self.list_motivosmovimientocuenta();

    self.viewfoto = function (event, obj) {
        var imagen = obj.persona.foto_logo_rec;
        var nombre = obj.persona.nombre_completo;
        var nacionalidad = obj.persona.nacionalidad.nombre
        $ocLazyLoad.load([
            'js/controllers/app.persona.info.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonaInf",
                controllerAs: "form",
                templateUrl: 'views/cronogramascobranza/viewpersona.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: true,
                locals: {obj: {tipo:'Persona', 
                'descripcion':obj.persona.nombre_completo,
                'imagen': imagen,
                'nombre': nombre,
                'nacionalidad': nacionalidad}}
            }).then(function (result) {
            }, function () {
            });
        });
    };

});