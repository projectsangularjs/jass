angular.module('app').controller('AfiliacionesForm', function (obj, $scope, $mdDialog,
    CONEXP, $ocLazyLoad, toastr, config, localStorageService) {
    var self = this;
    self.storage = config.url_api_storage;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;
    var entidad_id = localStorageService.get('limon_entidad');
    self.tiposafiliacion = [];
    self.list_representantes = [];
    self.list_del_representantes = [];
    var filtros = {};

    self.add_representante = function(){
        self.list_representantes.push({});
    };

    if (obj){
       CONEXP('procesos','afiliaciones/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            if(r.fecha_ingreso){
                self.obj['fecha_ingreso'] = moment(r.fecha_ingreso, 'YYYY-MM-DD').toDate();
            }
            if(r.fecha_afiliacion){
                self.obj['fecha_afiliacion'] = moment(r.fecha_afiliacion, 'YYYY-MM-DD').toDate();
            }

            self.list_representantes = r['representantes'];
            self.cargado = true;
        }, function (err) {
        });
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }


    self.del_representante = function(list_representantes, index, item){
        list_representantes.splice(index, 1);
        if(item['representante']['id']){
            var obj ={ id: item['representante']['id']};
            self.list_del_representantes.push(obj);
        }
    };

    self.save_or_update = function () {
        self.guardando = true;
        self.obj['fecha_ingreso'] = moment(self.obj['fecha_ingreso_']).format("YYYY-MM-DD");
        self.obj['fecha_afiliacion'] = moment(self.obj['fecha_afiliacion_']).format("YYYY-MM-DD");

        if(self.obj.persona){
            self.obj['persona_id'] = self.obj.persona['id'];
        }else{
            self.obj['persona_id'] = null;
        }

        if(self.obj.tipo_afiliacion){
            self.obj['tipo_afiliacion_id'] = self.obj.tipo_afiliacion['id'];
        }else{
            self.obj['tipo_afiliacion_id'] = null;
        }

        self.obj['del_representantes'] = self.list_del_representantes||[];
        self.obj['representantes'] = self.list_representantes||[];
        self.obj['entidad_id'] = entidad_id;
        if(self.obj.id){
            CONEXP('procesos','afiliaciones/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            CONEXP('procesos','afiliaciones/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };


    self.BuscarPersona = function (search) {
        filtros = {};
        filtros['search'] = search;
        filtros['limit'] = 10;
        filtros['q'] = {nombre_completo__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        filtros['ordering'] = "-nombre_completo";
        return CONEXP('comun','personas/searchformafiliacion').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarRepresentante = function(search){
        filtros = {};
        filtros['search'] = search;
        filtros['limit'] = 10;
        filtros['q'] = {nombre_completo__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1', tipo_persona__codigo:'01'};
        filtros['ordering'] = "-nombre_completo";
        return CONEXP('comun','personas/searchformrepre').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.BuscarTipoAfiliacion = function (search) {
        filtros = {};
        filtros['search'] = search;
        filtros['limit'] = 10;
        filtros['q'] = {nombre__icontains: search, codigo__icontains: search};
        filtros['f'] = {estado: '1'};
        filtros['ordering'] = "-nombre";
        return CONEXP('procesos','tiposafiliacion/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.form_persona = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.personas.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasForm",
                controllerAs: "form",
                templateUrl: 'views/personas/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                multiple:true,
                locals: {obj: obj},
                fullscreen: true,
            }).then(function (result) {
                self.obj['persona'] = result;
            }, function () {
            });
        });
    };

});