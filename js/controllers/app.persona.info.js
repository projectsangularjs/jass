angular.module('app').controller('PersonaInf', function (obj, $mdDialog,
	toastr, CONEXP,$ocLazyLoad, config)  {

	var self = this;
    self.obj = obj;

    self.close = function() {
        $mdDialog.cancel();
    };

    self.datetime = new Date().getTime();
    self.tamanio = 20;

	self.change_persona = function(pers_id) {
        self.get_datos_persona(pers_id);
    }

    self.get_datos_persona = function(pers_id){ /*console.log(pers_id);*/
        CONEXP('comun','personas/viewinfo').GET({id: pers_id}).$promise.then(function (r) {
            self.obj = r;
            self.cargado = true;
        }, function (err) {
        });
     };

	
});