angular.module('app').controller('Contratos', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, $state, localStorageService) {

    var self = this;
    self.Indeterminado = true;
    self.listado = [];
    self.search = '';
    var entidad_id = localStorageService.get('limon_entidad');

    self.pagination = {};

    self.paging = {
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
        ordering:'numero'
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.paging.search = self.search||'';
        CONEXP('procesos', 'contratos').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];

            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    // self.listar();

    function loadPages() {
        self.listar();
    }

   self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
   };

    self.new_contrato = function (id) {
        $state.go('app.addcontrato.registro');
    };

    self.ver_contrato = function(obj){
        $state.go('app.addcontrato.pago', {id: obj['id']});
    };

    self.delete = function (event, obj) {
        var confirm = $mdDialog.confirm()
                .title("Eliminar contrato")
                .textContent("¿Está seguro de eliminar el contrato " + obj['nombre'] + "?")
                .ariaLabel('delete')
                .targetEvent(event)
                .ok('SI')
                .cancel('NO');
        $mdDialog.show(confirm).then(function () {
            CONEXP('procesos', 'contratos/delete').DELETE({id: obj.id}).$promise.then(function (r) {
               if(r){
                     toastr.success('Eliminado correctamente', 'Eliminado');
                    self.listar();
                }
            }, function (err) {
                toastr.error('No se pudo eliminar', 'Error');
            });
        }, function () {
        });
    };

    self.pdfcontrato = function(obj){
        self.Indeterminado = true;
        var dat = {entidad_id:entidad_id, 'contrato_id':obj.id};
        CONEXP('procesos','contratos/pdfcontrato').POST(dat).$promise.then(function (r) {
            var win = window.open('', '_blank');
            pdfMake.createPdf(r).open({}, win);
            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;               
        });
    };


});