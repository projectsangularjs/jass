angular.module('app').controller('Recibos', function (
    toastr, CONEXP, $ocLazyLoad, $mdDialog, config, localStorageService) {
	var self = this;
	self.storage = config.url_api_storage;
    var entidad_id = localStorageService.get('limon_entidad');
    self.obj = {};
    self.obj['mes_recibo_id'] = '';

    self.list_zonas = function(){
        var dat={'search':''};
        CONEXP('procesos','zonas/searchform').GET_LIST().$promise.then(function(r){
            self.zonas = r;
        },function(err){
        });
    };
    self.list_zonas();

    self.list_ordenar = [
        {id:'mes_recibo_id,numero',titulo:'Mes y número de recibo'},
        {id:'contrato__predio__zona__nombre,contrato__predio__predio_direcciones__direccion_detalle',titulo:'Zona y dirección'},
        {id:'contrato__persona__nombre_completo',titulo:'Nombre completo de persona'},
        {id:'contrato__persona__persona_persona_juridica__razon_social,contrato__persona__persona_persona_natural__papellido,contrato__persona__persona_persona_natural__sapellido',titulo:'Apellidos y nombres'},
        
    ];

    self.ordering = self.list_ordenar[0]['id'];
    self.zona_id = 'T';

	self.listado = [];
    self.search = '';

    self.pagination = {};

    self.paging = {
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000','2000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.paging.search = self.search||'';
        // self.paging.forma_cobranza = self.obj['forma_cobranza'];
        self.paging.contrato__entidad_id = entidad_id;
        if (self.obj['mes_recibo_id'] !== 'T'){
            self.paging.mes_recibo_id = self.obj.mes_recibo_id;
        }else{
            delete self.paging.mes_recibo_id;
        }

        // self.paging.ordering = 'mes_recibo,numero';
        self.paging.ordering = self.ordering;

        if(self.zona_id !=='T'){
            self.paging['contrato__predio__zona_id'] = self.zona_id;
        }else{
            delete self.paging['contrato__predio__zona_id'];
        }

        CONEXP('procesos','recibos').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.obj['all'] = false;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];
            self.creando = false;
            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    function loadPages() {
        self.listar();
    }

    self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
    };

    self.ordenar = function(id){
        self.ordering = id;
        self.listar_por();
    };

    self.mes_recibo = function(event){
        $ocLazyLoad.load([
            'js/controllers/app.mesrecibos.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "MesRecibos",
                controllerAs: "form",
                templateUrl: 'views/mesrecibos/index.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false
            }).then(function (result) {
                self.list_mesrecibos();
            }, function () {
                self.list_mesrecibos();
            });
        });
    };

    self.generar = function(event){
        $ocLazyLoad.load([
            'js/controllers/app.recibos.generar.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "RecibosGenerar",
                controllerAs: "form",
                templateUrl: 'views/recibos/generar.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                fullscreen: true,
            }).then(function (result) {
                self.listar_por();
                self.list_mesrecibos();
            }, function () {
                self.listar_por();
                self.list_mesrecibos();
            });
        });
    };

    self.mesrecibos = [];

    self.list_mesrecibos = function () {
        var dat = {
          'f':{'estado__in':['HAB','GEN']},
          'ordering':'-anio,-mes_id'};
        CONEXP('procesos','mesrecibos/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.mesrecibos = r;
            if(r.length > 0 && !self.obj['mes_recibo_id']){
                self.obj['mes_recibo_id'] = r[0]['id'];
            }
        }, function (err) {
        });
    };
    self.list_mesrecibos();


    self.change_all = function(obj){
        angular.forEach(self.listado, function(item) {
                if(obj['all']){
                    item['select'] = true;
                }else{
                    item['select'] = false;
                }
          }, console.log);
    };

    self.existe_alguno = function(listado){
        var existe = false;
        for (var i = 0, len = listado.length; i < len; i++) {
          if (listado[i]['select']) {
            existe = true;
            break;
          }
        }
        return existe;
    };

    self.opcion_pdf = function(opcion){
        self.Indeterminado = true;
        var new_listado = [];
        angular.forEach(self.listado, function(item) {
                if(item['select']){
                    new_listado.push(item['id']);
                }
          }, console.log);

        var dat = {entidad_id:entidad_id, 'listado':new_listado};
        CONEXP('procesos','recibos/pdfrecibos').POST(dat).$promise.then(function (r) {
                // var win = window.open('', '_blank');
                r.content[0][0].pageBreak = 'none';
                if (opcion == 'ver'){
                    var win = window.open('', '_blank');
                    pdfMake.createPdf(r).open({}, win);
                    self.Indeterminado = false;
                }
                if (opcion == 'print'){
                    var win = window.open('', '_blank');
                    pdfMake.createPdf(r).print({}, win);
                    self.Indeterminado = false;
                }
                if (opcion == 'descargar'){
                    var nombre_file = moment().format('YYYYMMDDhmmssSSS')+".pdf";
                    pdfMake.createPdf(r).download(nombre_file);
                    self.Indeterminado = false;
                }

            }, function (err) {
               
            });
    };

});