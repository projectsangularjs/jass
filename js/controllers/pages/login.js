angular.module('app').controller('Login', function (toastr, Oauth2, $state, config) {
    var self = this;

    self.obj = {};
    self.cargando = false;

    self.login = function(){
        self.cargando = true;
        
        Oauth2.oauth2_login(self.obj).then(function (r) {
            toastr.success("Acceso correcto, bienvenido");
            window.location.href = config.url_web;   
        }, function (errors) {
            self.cargando = false;
            toastr.error(errors.data.error_description, errors.data.error);          
        });
    };

});