angular.module('app').controller('CajaRegistropagosRegistroOps', function ($scope, $mdDialog,
    toastr, CONEXP, localStorageService, $ocLazyLoad, config, persona, cronog) {
    var self = this;

    self.obj = {};
    self.obj['persona_id'] = persona['id'];


    if(cronog){
        self.obj['motivo_mov_cuenta_id'] = cronog['motivo_movimiento_cuenta']['id'];
        self.obj['cronograma_pago_id'] = cronog['id'];
    }else{
        
        self.cronogramas = [];
        self.monto_total = 0;
        self.pagado = 0;

        self.cargando_cronog = true;
        var dat = {'ordering':'fecha_cobranza', 
            f:{
                'motivo_movimiento_cuenta__tipo_movimiento_cuenta__codigo': 'ING',
                'persona_id':persona['id'],
                'estado_cronograma__codigo':'HAB',
                'entidad_id': entidad_id
            }
        };
        CONEXP('procesos','cronogramaspago/listcronogpagar').GET(dat).$promise.then(function (r) {
            self.cronogramas = r['cronogramas'];
            self.monto_total = r['monto_total'];
            self.pagado = r['pagado'];
            self.cargando_cronog = false;
            self.calcular_pago(self.obj, r['cronogramas']);
        }, function (err) {
            self.cargando_cronog = false;
            self.monto_total = 0;
        });
    }

    
    self.cronog = cronog;
    self.persona = persona;
    self.cargado = true;
    self.guardando = false;
    self.errores = [];
    var entidad_id = localStorageService.get('limon_entidad');
    self.tiposmediopago = [];
    self.tiposcuenta = [];

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.list_tiposcuenta = function () {
        var dat = { 'ordering':'id',
        'f':{"estado":"1"}};
        CONEXP('caja','tiposcuenta/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposcuenta = r;
            if(r.length>0){
                self.obj['tipo_cuenta_id'] = r[0]['id'];
                self.change_tipo_cuenta(self.obj);
            }
        }, function (err) {
        });
    };
    self.list_tiposcuenta();

    // self.list_tiposmediopago();

    self.list_cuentasempresa = function (obj) {
        var dat = { 'ordering':'tipo_cuenta_id,nombre',
            'f':{
                "estado":"1", 
                'tipo_cuenta_id': obj['tipo_cuenta_id'],
                'entidad_id':entidad_id,
                }
            };
        CONEXP('caja','cuentasentidad/searchformcaja').GET_LIST(dat).$promise.then(function (r) {
            self.cuentasentidad = r;
            if(r.length>0){
                self.obj['cuenta_entidad_id'] = r[0]['id'];
            }
        }, function (err) {
        });
    };

    self.list_tiposmediopago = function (tipo_cuenta_id) {
        var dat = { 'ordering':'id',
        'f':{"estado":"1","tipo_cuenta_id":tipo_cuenta_id}};
        CONEXP('caja','tiposmediopago/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposmediopago = r;
            if(r.length>0){
                self.obj['tipo_medio_pago_id'] = r[0]['id'];
            }
        }, function (err) {
        });
    };

    self.change_tipo_cuenta = function(obj){
        obj['cuenta_entidad_id'] = "";
        obj['tipo_medio_pago_id'] = "";
        self.list_tiposmediopago(obj['tipo_cuenta_id']);
        self.list_cuentasempresa(obj);
    };


    self.get_datosentidad = function(){
        var dat = { 'id':entidad_id};
        CONEXP('comun','entidades/datosbasic').GET(dat).$promise.then(function (r) {
            self.entidad = r;
            if(r){
                self.obj['moneda_id'] = r['moneda_id'];
            }
        }, function (err) {
        });
    };

    self.monedas = [];
    self.list_monedas = function () {
        var dat = { 'ordering':'id',
        'f':{"estado":"1"}};
        CONEXP('comun','monedas/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.monedas = r;
            self.get_datosentidad();
        }, function (err) {
        });
    };
    self.list_monedas();


    self.get_cotizacion = function(){
        CONEXP('caja','functions/cotizaciondolar').GET({entidad_id: entidad_id}).$promise.then(function (r) {
            if(r){
                self.obj['tipo_cambio'] = r['venta'];
                self.change_importe(self.obj)
            }            
        }, function (err) {
        });
    };

    self.obj['tipo_cambio'] = 1;
    self.obj['comisiones'] = 0;
    self.obj['vuelto'] = 0;
    self.change_moneda = function(obj){
        obj['tipo_cambio'] = 1;
        angular.forEach(self.monedas, function(item) {
            if(item.id === obj.moneda_id ){
                if(item.codigo === 'USD'){
                    self.get_cotizacion();
                }
            }
        }, console.log);
        self.change_importe(self.obj)
    };

    self.change_recibido = function(obj){
        if(obj['importe'] && obj['recibido']){
            obj['vuelto'] = obj['recibido'] - obj['importe'];
        }else{
            self.obj['vuelto'] = 0;
        }
    };

    self.new_cronogramas = [];
    self.calcular_pago = function(obj, listado){
        var importe = 0;
        if(obj['importe'] && obj['tipo_cambio']){
            importe = obj['importe'] * obj['tipo_cambio'];
        };

        self.new_cronogramas = [];
        angular.forEach(listado, function(item) {
            var deuda = 0;
            if(importe > 0){
                deuda = item['monto'] - item['pagado'];
                if(deuda<=importe){
                    item['completo'] = 'SI';
                    item['pagar'] = deuda;
                }else{
                    item['completo'] = 'NO';
                    item['pagar'] = importe;
                }
                importe = importe - deuda;
            }else{
                item['completo'] = null;
                item['pagar'] = null;
            }
            self.new_cronogramas.push(item);
        }, console.log);
    };


    self.change_importe = function(obj){
        self.change_recibido(obj);
        self.calcular_pago(obj, self.cronogramas);
    };

    self.registrarpago = function(){

        self.errores = [];
        self.guardando = true;
        self.obj['entidad_id'] = entidad_id;
        self.obj['cronogramas'] = self.new_cronogramas;

        CONEXP('caja','movimientoscuenta/registrarpago').POST(self.obj).$promise.then(function (r) {
            self.guardando = false;
            if(r){
                toastr.success("Pago registrado correctamente","Registrado");
                 $mdDialog.hide(r);
            }
        }, function (err) {
            self.guardando = false;
            self.errores = err.data;
            toastr.error("No se pudo registrar","Error");
        });
        
    };

});