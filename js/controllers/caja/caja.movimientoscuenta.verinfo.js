angular.module('app').controller('CajaVerinfo', function (obj, $mdDialog,
    toastr, CONEXP, localStorageService, $ocLazyLoad, config, $sce) {
    var self = this;

    self.pag = {};
    self.cargado = false;
    var entidad_id = localStorageService.get('limon_entidad');

    CONEXP('caja','functions/depositoinfo').GET({id: obj['id']}).$promise.then(function (r) {
        self.pag = r;
        self.cargado = true;
    }, function (err) {
        self.cargado = true;
    }); 

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.url_frame = '';
    self.guardando = false;
    self.imprimir_ticket = function(_id){
        self.guardando = true;
        self.url_frame = '';
        CONEXP('caja','functions/printdeposito').GET({id:obj['id']}).$promise.then(function (r) {
            self.url_frame = r['url'];
            self.guardando = false;
        }, function (err) {
            toastr.error("No se pudo imprimir","Error");
            self.guardando = false;
        });
    };

    self.frame_url = function (url_frame) {
        return $sce.trustAsResourceUrl(url_frame);
    };

});