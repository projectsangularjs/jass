angular.module('app').controller('CajaAnularpagos', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, config, localStorageService) {
	var self = this;
	self.storage = config.url_api_storage;
    var entidad_id = localStorageService.get('limon_entidad');
    self.obj = {};

    self.Indeterminado = true;
    self.listado = [];
    self.search = '';
    self.motivosmovimientocuenta = [];
    self.cronogramaspago = [];
    self.motivo_movimiento_cuenta_id = 'T';

    self.pagination = {};
    self.paging = {
        // motivo_movimiento_cuenta__tipo_movimiento_cuenta__codigo: 'ING',
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
        estado:'1',
        ordering:'numero',
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.paging.search = self.search||'';        
        CONEXP('caja','movimientoscuenta').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];

            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    // self.listar();

    function loadPages() {
        self.listar();
    }

   self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
   };

   self.anular = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/caja/caja.anularpagos.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "CajaAnularpagosForm",
                controllerAs: "form",
                templateUrl: 'views/caja/movimientoscuenta/anularpagos.form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj}
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };


});