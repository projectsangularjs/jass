angular.module('app').controller('CajaMisoperaciones', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, config, localStorageService) {
	var self = this;
	self.storage = config.url_api_storage;
    var entidad_id = localStorageService.get('limon_entidad');
    var per_id = localStorageService.get('limon_per_id');
    self.obj = {};

    self.Indeterminado = true;
    self.listado = [];
    self.search = '';
    self.motivosmovimientocuenta = [];
    self.cronogramaspago = [];
    self.motivo_movimiento_cuenta_id = 'T';

    self.list_ordenar = [
        {id:'fecha,hora,numero',titulo:'Fecha, Hora y número'},
        {id:'-importe',titulo:'Mayor importe'},
        {id:'importe',titulo:'Menor importe'},
        {id:'-motivo_mov_cuenta__tipo_movimiento_cuenta__nombre',titulo:'Tipo:Ingreso/Egreso'},
        {id:'motivo_mov_cuenta__nombre',titulo:'Cuenta motivo'}
    ];

    self.ordering = self.list_ordenar[0]['id'];


    self.pagination = {};
    self.paging = {
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
        cuenta_entidad__entidad_id:entidad_id,
        usuario_id:per_id,
    };

    self.fecha_actual = moment(self).format("YYYY-MM-DD");

    self.estado = '1';

    self.list_per_pagina = ['5','10','25','50','100','500','1000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.CargandoMas = true;
        self.paging.search = self.search||'';
        // var fecha_inicial = moment(self.fecha_inicial).format("YYYY-MM-DD");
        // var fecha_final = moment(self.fecha_final).format("YYYY-MM-DD");
        // var new_inicial = moment(fecha_inicial, "YYYY-MM-DD").add(-1, 'days');
        // var new_final = moment(fecha_final, "YYYY-MM-DD").add(1, 'days');
        self.paging.fecha_inicial = moment(self.fecha_inicial).format("YYYY-MM-DD");
        self.paging.fecha_final = moment(self.fecha_final).format("YYYY-MM-DD");
        self.paging.estado = self.estado;
        self.paging.ordering = self.ordering;

        CONEXP('caja','movimientoscuenta').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];
            self.Indeterminado = false;
            self.mas_informacion(self.paging);
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    // self.listar();

    function loadPages() {
        self.listar();
    }

   self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
   };

   self.ordenar = function(id){
        self.ordering = id;
        self.listar_por();
   };

   self.info = {};
   self.mas_informacion = function(paging){
        self.CargandoMas = true;
        var filtros = {
            'f':{
                'cuenta_entidad__entidad_id':paging['cuenta_entidad__entidad_id'],
                'fecha__gte':paging['fecha_inicial'],
                'fecha__lte':paging['fecha_final'],
                'usuario_id':paging['usuario_id']
            }
        };
        if (paging['search'] === ""){
            CONEXP('caja','movimientoscuenta/masinformacion').GET(filtros).$promise.then(function (r) {
                self.CargandoMas = false;
                self.info = r;
            }, function (err) {
                self.CargandoMas = false;
            });
        }else{
            self.CargandoMas = false;
        }
   };

   self.anular = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/caja/caja.anularpagos.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "CajaAnularpagosForm",
                controllerAs: "form",
                templateUrl: 'views/caja/movimientoscuenta/anularpagos.form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj}
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };

    self.verinfo = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/caja/caja.movimientoscuenta.verinfo.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "CajaVerinfo",
                controllerAs: "form",
                templateUrl: 'views/caja/movimientoscuenta/verinfo.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj}
            }).then(function (result) {
            }, function () {
            });
        });
    };

     self.opcion_pdf = function(opcion){
        self.Indeterminado = true;

        var dat = {
            'entidad_id': entidad_id,
            'fecha_inicial':self.paging['fecha_inicial'],
            'fecha_final':self.paging['fecha_final'],
            'usuario_id':self.paging['usuario_id'],
            'fecha_inicial_':moment(self.fecha_inicial).format("DD/MM/YYYY"),
            'fecha_final_':moment(self.fecha_final).format("DD/MM/YYYY")
        };

        CONEXP('caja','movimientoscuenta/pdfinforme').GET(dat).$promise.then(function (r) {
                if (opcion == 'ver'){
                    var win = window.open('', '_blank');
                    pdfMake.createPdf(r).open({}, win);
                    self.Indeterminado = false;
                }
                if (opcion == 'print'){
                    var win = window.open('', '_blank');
                    pdfMake.createPdf(r).print({}, win);
                    self.Indeterminado = false;
                }
                if (opcion == 'descargar'){
                    var nombre_file = moment().format('YYYYMMDDhmmssSSS')+".pdf";
                    pdfMake.createPdf(r).download(nombre_file);
                    self.Indeterminado = false;
                }
            }, function (err) {
               
        });
    };

    
});