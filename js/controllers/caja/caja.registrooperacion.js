angular.module('app').controller('CajaRegistrooperacion', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, config, localStorageService) {
	var self = this;
	self.storage = config.url_api_storage;
    var entidad_id = localStorageService.get('limon_entidad');
    self.obj = {};

	var filtros = {};

	self.list_tiposmovim_cue = function () {
        var dat = { 'ordering':'-id',
        'f':{"estado":"1"}};
        CONEXP('procesos','tiposmovimientocuenta/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposmovimientocuenta = r;
            self.obj['tipo_movimiento_cuenta_id'] = r[0]['id'];
        }, function (err) {
        });
    };
    self.list_tiposmovim_cue();


    self.change_tipo_movim = function(tipo){
    	self.obj['motivo_mov_cuenta'] = null;
    	self.searchMotivo = '';
    };

    self.BuscarMotivo = function (search, tipo) {
        var dat = { 
        	'q': {'nombre__icontains': search||'', 'codigo__icontains': search||''},
        	'f': {"estado":"1","tipo_movimiento_cuenta_id":tipo},
        	'e': {"codigo__in":['DEUDAN','PGSER','RECIBO','SERINS','00006','00007']},
        	'ordering':'codigo',
    	};

        return CONEXP('procesos','motivosmovimientocuenta/searchform').GET_LIST(dat).$promise.then(function (r) {
            return r;
        });
    };

    self.tiposmediopago = [];
    self.tiposcuenta = [];

    self.list_tiposcuenta = function () {
        var dat = { 'ordering':'id',
        'f':{"estado":"1"}};
        CONEXP('caja','tiposcuenta/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposcuenta = r;
            if(r.length>0){
                self.obj['tipo_cuenta_id'] = r[0]['id'];
                self.change_tipo_cuenta(self.obj);
            }
        }, function (err) {
        });
    };
    self.list_tiposcuenta();
    self.max_importe = 0;
    self.change_cuenta_entidad = function(cuenta){
    	// Consultar saldo
    	CONEXP('caja','cuentasentidad/show').GET({id:cuenta}).$promise.then(function (r) {
           	self.max_importe = r['saldo'];
        }, function (err) {
        });
    };

    self.list_cuentasempresa = function (obj) {
        var dat = { 'ordering':'tipo_cuenta_id,nombre',
            'f':{
                "estado":"1", 
                'tipo_cuenta_id': obj['tipo_cuenta_id'],
                'entidad_id':entidad_id,
                }
            };
        CONEXP('caja','cuentasentidad/searchformcaja').GET_LIST(dat).$promise.then(function (r) {
            self.cuentasentidad = r;
            if(r.length>0){
                self.obj['cuenta_entidad_id'] = r[0]['id'];
                self.change_cuenta_entidad(r[0]['id']);
            }
        }, function (err) {
        });
    };

    self.list_tiposmediopago = function (tipo_cuenta_id) {
        var dat = { 'ordering':'id',
        'f':{"estado":"1","tipo_cuenta_id":tipo_cuenta_id}};
        CONEXP('caja','tiposmediopago/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposmediopago = r;
            if(r.length>0){
                self.obj['tipo_medio_pago_id'] = r[0]['id'];
            }
        }, function (err) {
        });
    };

    self.change_tipo_cuenta = function(obj){
        obj['cuenta_entidad_id'] = "";
        obj['tipo_medio_pago_id'] = "";
        self.list_tiposmediopago(obj['tipo_cuenta_id']);
        self.list_cuentasempresa(obj);
        self.obj['importe'] = self.monto_total - self.pagado;
    };

    self.list_cuentasempresa_des = function (obj) {
        var dat = { 'ordering':'tipo_cuenta_id,nombre',
            'f':{
                "estado":"1", 
                'tipo_cuenta_id': obj['des_tipo_cuenta_id'],
                'entidad_id':entidad_id,
                },
            'e':{
                'id':obj['cuenta_entidad_id']
                }
            };
        CONEXP('caja','cuentasentidad/searchformcaja').GET_LIST(dat).$promise.then(function (r) {
            self.cuentasentidad_des = r;
            if(r.length>0){
                self.obj['des_cuenta_entidad_id'] = r[0]['id'];
            }
        }, function (err) {
        });
    };

    self.change_tipo_cuenta_des = function(obj){
        obj['des_cuenta_entidad_id'] = "";
        self.list_cuentasempresa_des(obj);        
    };


    self.get_datosentidad = function(){
        var dat = { 'id':entidad_id};
        CONEXP('comun','entidades/datosbasic').GET(dat).$promise.then(function (r) {
            self.entidad = r;
            if(r){
                self.obj['moneda_id'] = r['moneda_id'];
            }
        }, function (err) {
        });
    };

    self.monedas = [];
    self.list_monedas = function () {
        var dat = { 'ordering':'id',
        'f':{"estado":"1",'codigo__in':['PEN']}};
        CONEXP('comun','monedas/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.monedas = r;
            self.get_datosentidad();
        }, function (err) {
        });
    };
    self.list_monedas();

    self.get_cotizacion = function(){
        CONEXP('caja','functions/cotizaciondolar').GET({entidad_id: entidad_id}).$promise.then(function (r) {
            if(r){
                self.obj['tipo_cambio'] = r['venta'];
                self.change_importe(self.obj)
            }            
        }, function (err) {
        });
    };

    self.obj['tipo_cambio'] = 1;
    self.obj['comisiones'] = 0;
    self.obj['vuelto'] = 0;
    self.change_moneda = function(obj){
        obj['tipo_cambio'] = 1;
        angular.forEach(self.monedas, function(item) {
            if(item.id === obj.moneda_id ){
                if(item.codigo === 'USD'){
                    self.get_cotizacion();
                }
            }
        }, console.log);
        self.change_importe(self.obj)
    };

    self.change_recibido = function(obj){
        if(obj['importe'] && obj['recibido']){
            obj['vuelto'] = obj['recibido'] - obj['importe'];
        }else{
            self.obj['vuelto'] = 0;
        }
    };

    self.storage = config.url_api_storage;
    self.BuscarPersona = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre_completo__icontains: search, persona_persona_documentos__numero__icontains: search};
        filtros['f'] = {estado: '1'};
        filtros['ordering'] = ['nombre_completo'];
        return CONEXP('comun','personas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.form_persona = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/app.personas.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "PersonasForm",
                controllerAs: "form",
                templateUrl: 'views/personas/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                multiple:true,
                locals: {obj: obj},
                fullscreen: true,
            }).then(function (result) {
                self.obj['persona'] = result;
            }, function () {
            });
        });
    };

    self.registraroperacion = function(){

        self.errores = [];
        
        self.obj['entidad_id'] = entidad_id;
        self.obj['persona_id'] = self.obj.persona.id;
        self.obj['motivo_mov_cuenta_id'] = self.obj.motivo_mov_cuenta.id;
        self.obj['motivo_mov_cuenta_codigo'] = self.obj.motivo_mov_cuenta.codigo;

        if(self.obj.importe > 0){
            self.guardando = true;
            CONEXP('caja','movimientoscuenta/registraroperacion').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                self.obj['persona'] = null;
                self.obj['motivo_mov_cuenta'] = null;
                self.obj['detalle'] = null;
                delete self.obj['importe'];
                delete self.obj['recibido'];
                self.change_cuenta_entidad(self.obj['cuenta_entidad_id']);
                if(r){
                    toastr.success("La Operación ha sido registrada correctamente","Operación");
                }
            }, function (err) {
                self.guardando = false;
                self.errores = err.data;
                toastr.error("No se pudo registrar","Error");
            });
        }else{
            toastr.error("El importe no debe ser menor que 0","Error");
        }       
        
    };


});