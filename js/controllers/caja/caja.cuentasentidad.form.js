angular.module('app').controller('CajaCuentasentidadForm', function (obj, $scope, $mdDialog,
    toastr, CONEXP, localStorageService, $ocLazyLoad, config) {
    var self = this;

    self.obj = {};
    self.obj_ = obj;
    self.cargado = false;
    self.guardando = false;
    self.errores = [];
    var entidad_id = localStorageService.get('limon_entidad');
    self.list_usuarios = [];



    self.transfor_get = function(listado){
        var new_array = [];
        angular.forEach(listado, function(item) {
            if(item.fecha_inicio){
                item['fecha_inicio_'] = moment(item.fecha_inicio, 'YYYY-MM-DD').toDate();
            }
            if(item.fecha_fin){
                item['fecha_fin_'] = moment(item.fecha_fin, 'YYYY-MM-DD').toDate();
            }
            new_array.push(item);
        }, console.log);
        return new_array;
    }

    if (obj){
       CONEXP('caja','cuentasentidad/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
            self.list_usuarios = self.transfor_get(r['usuarios']);
            self.cargado = true;
        }, function (err) {
        }); 
    }else{
        self.obj['estado'] = '1';
        self.cargado = true;
    }

    self.tiposcuenta = [];
    self.list_tiposcuenta = function () {
        var dat = {f:{'estado':'1'}};
        CONEXP('caja','tiposcuenta/searchform').GET_LIST(dat).$promise.then(function (r) {
            self.tiposcuenta = r;
        }, function (err) {
        });
    };

    self.change_tipo_cuenta = function(obje){
        if(obje.tipo_cuenta_id){
            angular.forEach(self.tiposcuenta, function(item) {
                if(item.id ==obje.tipo_cuenta_id ){
                    obje['tipo_cuenta_codigo'] = item['codigo'];
                }
            }, console.log);
        }else{
            obje['tipo_cuenta_codigo'] = null;
            obje['tipo_cuenta_id'] = null;
        }
    };

    self.list_tiposcuenta();


    self.transfor_data = function(listado){
        var new_array = [];
        angular.forEach(listado, function(item) {
            item['persona_id'] = item['persona']['id'];
            if(item['fecha_inicio_']){
                item['fecha_inicio'] = moment(item['fecha_inicio_']).format("YYYY-MM-DD");
            }else{
                item['fecha_inicio'] = null;
            }
            if(item['fecha_fin_']){
                item['fecha_fin'] = moment(item['fecha_fin_']).format("YYYY-MM-DD");
            }else{
                item['fecha_fin'] = null;
            }
            new_array.push(item);
        }, console.log);
        return new_array;
    }

    self.save_or_update = function () {

        self.obj['del_usuarios'] = self.list_del_usuarios||[];
        self.obj['usuarios'] = self.transfor_data(self.list_usuarios);

        if(self.obj.tipo_cuenta_codigo=='BANCOS'){
            self.obj.persona_cuenta_id = self.obj.persona_cuenta['id'];
        }else{
            self.obj.persona_cuenta_id = null;
        }

        self.errores = [];
        self.guardando = true;
        if(self.obj.id){
            CONEXP('caja','cuentasentidad/update').PUT(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Actualizado correctamente","Actualizado");
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                toastr.error("No se pudo actualizar","Error");
            });
        }else{
            self.obj['entidad_id'] = entidad_id;
            CONEXP('caja','cuentasentidad/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                     $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                self.errores = err.data;
                toastr.error("No se pudo registrar","Error");
            });
        }
    };

    self.BuscarPersonaCuenta = function (search, permisos) {
        var data = {
            'q': {'numero__icontains': search, 'entidad_financiera__nombre__icontains': search},
            'limit':20,
            'f':{'persona__entidad_persona__id':entidad_id},
            'e':{'persona_cuenta_cuenta_entidades__entidad_id':entidad_id}
        }
        return CONEXP('caja','personascuenta/searchform').GET_LIST(data).$promise.then(function (r) {
            return r;
        });
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.cuentaexterno = function(event){
        $ocLazyLoad.load([
            'js/controllers/caja/caja.cuentasentidad.cuentaexterno.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "CajaCuentasentidadCuentaexterno",
                controllerAs: "form",
                templateUrl: 'views/caja/cuentasentidad/cuentaexterno.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                multiple: true
            }).then(function (result) {
                self.obj.persona_cuenta = result;
            }, function () {
            });
        });
    };

    self.storage = config.url_api_storage;


    self.return_val = function(listado){
        var new_array = [];
        angular.forEach(listado, function(item) {
          new_array.push(item['persona']['id']);
        }, console.log);
        return new_array;
    }

    var filtros = {};
    self.BuscarPersonaUsuario = function (search, type) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre_completo__icontains: search, 
            persona_persona_documentos__numero__icontains: search};
        filtros['f'] = {estado: '1','persona_usuario__entidades__in':[entidad_id]};
        filtros['e'] = {'id__in':self.return_val(self.list_usuarios||[])};
        filtros['ordering'] = 'nombre_completo';
        return CONEXP('comun','personas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.searchTextUS = "";
    self.change_susuarios = function(obj){
        if(obj){
            if(obj.foto_logo_rec){
                obj.foto_logo_rec = self.storage+obj.foto_logo_rec;
            }
            var data = {};
            data['persona'] = obj;
            data['estado'] = '1';
            data['fecha_inicio_'] = new Date();
            self.list_usuarios.push(data);
            self.susuarios = null;
            self.searchTextUS = "";
        }
    };

    self.list_del_usuarios = [];
    self.del_usuarios = function(usuarios, index, item){
        usuarios.splice(index, 1);
        if(item['id']){
            var obj ={ id: item['id']};
            self.list_del_usuarios.push(obj);
        };
    };

});