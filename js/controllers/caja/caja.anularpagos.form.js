angular.module('app').controller('CajaAnularpagosForm', function (obj, $mdDialog,
    toastr, CONEXP, localStorageService, $ocLazyLoad, config) {
    var self = this;

    self.obj = {};
    self.pag = obj;
    self.cargado = true;
    var entidad_id = localStorageService.get('limon_entidad');

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.anularpago = function(){

        self.errores = [];
        self.guardando = true;
        self.obj['id'] = obj['id'];
        self.obj['entidad_id'] = entidad_id;

        CONEXP('caja','movimientoscuenta/anularpago').PUT(self.obj).$promise.then(function (r) {
            self.guardando = false;
            if(r){
                toastr.success("Pago anulado correctamente","Anulado");
                 $mdDialog.hide(r);
            }
        }, function (err) {
            self.guardando = false;
            self.errores = err.data;
            toastr.error("No se pudo registrar","Error");
        });
        
    };

});