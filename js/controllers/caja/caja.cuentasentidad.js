angular.module('app').controller('CajaCuentasentidad', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, config) {
	var self = this;
	self.storage = config.url_api_storage;

    self.Indeterminado = true;
    self.listado = [];
    self.search = '';

    self.pagination = {};

    self.paging = {
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
        ordering:'tipo_cuenta_id,nombre'
    };

    self.total_saldo = 0;
    self.calcular_total = function(listado){
        var total_saldo = 0;
        angular.forEach(listado, function(item) {
            total_saldo = total_saldo + parseFloat(item['saldo']);
        }, console.log);
        self.total_saldo = total_saldo;
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.paging.search = self.search||'';
        CONEXP('caja','cuentasentidad').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];
            self.calcular_total(r.data);
            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    // self.listar();

    function loadPages() {
        self.listar();
    }

   self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
   };

    self.new_edit = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/caja/caja.cuentasentidad.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "CajaCuentasentidadForm",
                controllerAs: "form",
                templateUrl: 'views/caja/cuentasentidad/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj}
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };

    self.delete = function (event, obj) {
        var confirm = $mdDialog.confirm()
                .title("Eliminar cuenta")
                .textContent("¿Está seguro de eliminar la cuenta " + obj['nombre'] + "?")
                .ariaLabel('delete')
                .targetEvent(event)
                .ok('SI')
                .cancel('NO');
        $mdDialog.show(confirm).then(function () {
            CONEXP('caja','cuentasentidad/delete').DELETE({id: obj.id}).$promise.then(function (r) {
               if(r){
                     toastr.success('Eliminado correctamente', 'Eliminado');
                    self.listar();
                }
            }, function (err) {
                toastr.error('No se pudo eliminar', 'Error');
            });
        }, function () {
        });
    };

	// var filtros = {};
 //    self.BuscarPersona = function (search) {
 //        filtros = {};
 //        filtros['limit'] = 10;
 //        filtros['q'] = {nombre_completo__icontains: search, 
 //            persona_persona_documentos__numero__icontains: search};
 //        filtros['f'] = {estado: '1'};
 //        filtros['ordering'] = 'nombre_completo';
 //        return CONEXP('comun','personas/searchform').GET_LIST(filtros).$promise.then(function (r) {
 //            return r;
 //        });
 //    };


});