angular.module('app').controller('CajaCuentasentidadCuentaexterno', function ($scope, $mdDialog, 
    toastr, CONEXP, localStorageService) {
    var self = this;
    var entidad_id = localStorageService.get('limon_entidad');

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.seleccionar = function(obj){
        $mdDialog.hide(obj);
    };

    self.listado = [];
    self.search = '';

    self.pagination = {};

    self.paging = {
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
        exclude_entidad: entidad_id
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000','2000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.paging.search = self.search||'';
        CONEXP('caja','personascuenta').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];

            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    function loadPages() {
        self.listar();
    }

   self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
   };

});