angular.module('app').controller('CajaRegistropagos', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, config, localStorageService) {
	var self = this;
	self.storage = config.url_api_storage;
    var entidad_id = localStorageService.get('limon_entidad');
    self.obj = {};

	var filtros = {};
    self.BuscarPersona = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {nombre_completo__icontains: search, 
            persona_persona_documentos__numero__icontains: search};
        filtros['f'] = {estado: '1'};
        filtros['ordering'] = 'nombre_completo';
        return CONEXP('comun','personas/searchform').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.contratos = [];
    self.cargando_cronog = false;
    self.monto_total = 0;
    self.pagado = 0;
    self.error_carga = false;
    self.change_persona = function(persona){
        
        if(persona){
            self.afiliacion = "";
            self.contratos = [];
            self.monto_total = 0;
            self.pagado = 0;
            self.cargando_cronog = true;
            var dat = {'ordering':'contrato__numero,fecha_cobranza', 
                f:{
                    'motivo_movimiento_cuenta__tipo_movimiento_cuenta__codigo': 'ING',
                    'persona_id':persona['id'],
                    'estado_cronograma__codigo':'HAB',
                    'entidad_id': entidad_id
                },
                persona_id:persona['id'],
                entidad_id:entidad_id
            };
            CONEXP('procesos','cronogramaspago/listcronogpagar').GET(dat).$promise.then(function (r) {
                self.contratos = r['contratos'];
                self.monto_total = r['monto_total'];
                self.pagado = r['pagado'];
                self.afiliacion = r['afiliacion'];
                self.cargando_cronog = false;
                self.error_carga = false;
            }, function (err) {
                self.cargando_cronog = false;
                self.monto_total = 0;
                self.error_carga = true;
            });
        }else{
            self.contratos = [];
            self.monto_total = 0;
        }
    };

    self.registro = function (event, persona, cronog, contrato_id) {
        $ocLazyLoad.load([
            'js/controllers/caja/caja.registropagos.registro.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "CajaRegistropagosRegistro",
                controllerAs: "form",
                templateUrl: 'views/caja/movimientoscuenta/registro.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {persona: persona, cronog: cronog, contrato_id: contrato_id}
            }).then(function (result) {
                self.change_persona(self.obj.persona);
            }, function () {
                self.change_persona(self.obj.persona);
            });
        });
    };

    // http://pdfmake.org/playground.html
    self.pdfcronogramas = function(){
        var win = window.open('', '_blank');
        var dat = {persona_id: self.obj.persona['id'], entidad_id:entidad_id};
        CONEXP('caja','estadoscuenta/estadocuentacrong').GET(dat).$promise.then(function (r) {
                pdfMake.createPdf(r).open({}, win);
            }, function (err) {
               
            });
    };

    self.movimientos = function($event, obj){

        var person = {};
        person['id'] = obj['id']
        person['nombre_completo'] = obj['nombre_completo']
        person['persona_persona_documentos'] = obj['persona_persona_documentos'];

        if(obj['foto_logo_rec']){
            person['foto_logo_rec'] = self.storage+obj.foto_logo_rec;
        };

        $ocLazyLoad.load([
            'js/controllers/app.estadoscuenta.movimientos.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "EstadosCuentaMovimiento",
                controllerAs: "form",
                templateUrl: 'views/estadoscuenta/movimientos.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {persona: person},
                fullscreen: true,
            }).then(function (result) {
                
            }, function () {
            });
        });
    };

    

});