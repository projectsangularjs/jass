angular.module('app').controller('CajaOperacionesentidad', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, config) {
	var self = this;
    self.obj = {};

    self.Indeterminado = false;
    self.existe_url = "";


    // self.fecha_inicial = moment(self).format("YYYY-MM-DD");
    // self.fecha_final = moment(self).format("YYYY-MM-DD");

    self.listar = function(id){
        self.url_reporte = "";
        var fecha_inicial_ = moment(self.fecha_inicial).format("YYYY-MM-DD");
        var fecha_final_ = moment(self.fecha_final).format("YYYY-MM-DD");

        self.url_reporte = 'reportes.html#/app/operacionesentidad/'+fecha_inicial_+'/'+fecha_final_;
    };

    self.listar();
    
});