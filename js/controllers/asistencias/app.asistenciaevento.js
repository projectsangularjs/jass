
angular.module('app').controller('AsistenciaEvento', function (
    toastr, CONEXP,$ocLazyLoad, $mdDialog, $state, config, localStorageService, $rootScope, $stateParams) {
    /*// Recibir un objeto como parametro:
    console.log($stateParams);
    console.log($stateParams.evento.id);*/
	/*GENERAL*/
    var self = this;
    self.ctrl = {};
    self.obj = {};
    var filtros = {};
    self.evento = [];
    self.tipo_afiliacion = []
    self.estado_asistencia = [];

    self.storage = config.url_api_storage;
    self.entidad_id = localStorageService.get('limon_entidad');
    var evento_id = $state.params.id;
    var estado_asistencia_codigo;
    var fecha_a_validar;
    var hora_a_validar;

    /*var estado_asistencia_codigo = '';*/

    self.Indeterminado = true;
    self.listado = [];
    self.search = '';
    self.pagination = {};
    self.paging = {
        /*evento__id: evento_id,*/ // motivo_movimiento_cuenta__tipo_movimiento_cuenta__codigo: 'ING',
        search:'',
        per_page: "10",
        last_page: 1,
        current_page: 1,
        onPageChanged: loadPages,
    };

    self.list_per_pagina = ['5','10','25','50','100','500','1000'];

    self.listar = function () {
        self.Indeterminado = true;
        self.paging.search = self.search||'';
        self.paging.ordering = '-id';
        self.paging['evento_id'] = evento_id;
        self.getEvento(evento_id);

        CONEXP('procesos','asistencias').GET(self.paging).$promise.then(function (r) {
            self.listado = r.data;
            self.pagination = r.pagination;
            self.paging['last_page'] = r.pagination['last_page'];
            self.paging['current_page'] = r.pagination['current_page'];
            self.Indeterminado = false;
        }, function (err) {
            self.Indeterminado = false;
        });
    };

    // self.listar();

    function loadPages() {
        self.listar();
    }

    self.listar_por = function(){
        self.paging['current_page'] = 1;
        self.listar();
    };

    self.getEvento = function(evento_id){
        filtros = {};
        filtros['f'] = {estado: '1', id: evento_id};
        return CONEXP('procesos','eventos/searchform').GET_LIST(filtros).$promise.then(function (r) {
            self.evento = r[0];
            if (self.evento['tipo_asistencia']['codigo'] === 'USUARIOS') {
                self.tipo_afiliacion['codigo'] = 'USUARIO';
            } else if(self.evento['tipo_asistencia']['codigo'] === 'SOCIOS'){
                self.tipo_afiliacion['codigo'] = 'SOCIO';
            } else {
                self.tipo_afiliacion['codigo'] = '';
            }
            fecha_a_validar = self.evento['fecha'];
            if (self.evento['hora_tolerancia']) {
                hora_a_validar = moment(self.evento['hora_tolerancia'], 'HH:mm:ss a');
                /*console.log('hora_tolerancia = '+hora_a_validar);*/
            } else{
                hora_a_validar = moment(self.evento['hora_inicio'], 'HH:mm:ss a');
                /*console.log('hora_inicio = '+hora_a_validar);*/
            }
            self.ValidarHora(fecha_a_validar, hora_a_validar);
            return r;
        });
    }

    self.ValidarHora = function (fecha_a_validar, hora_a_validar) {
        var date_now = moment().format('YYYY-MM-DD');
        var date_evento = moment(fecha_a_validar).format('YYYY-MM-DD');
        var fecha_hoy = new Date(date_now);
        var fecha_evento = new Date(date_evento);
        /*console.log('fecha_hoy = '+fecha_hoy);
        console.log('fecha_evento = '+fecha_evento);*/
       if (fecha_hoy > fecha_evento) {
            estado_asistencia_codigo = 'T';
            /*console.log('fecha_hoy > fecha_evento :: '+estado_asistencia_codigo);*/
        }  else if (fecha_hoy < fecha_evento) {
            estado_asistencia_codigo = 'P';
            /*console.log('fecha_hoy < fecha_evento :: '+estado_asistencia_codigo);*/
        } else {
            /*console.log('fecha_hoy === fecha_evento');
            console.log('hora_a_validar = '+hora_a_validar);*/
            var hora_sistema= moment();
            var duration = moment.duration(hora_sistema.diff(hora_a_validar));
            var hours = parseInt(duration.asHours());
            var minutes = parseInt(duration.asMinutes())-hours*60;
            /*console.log('La diferencia es de ' + hours + " Hrs and " + minutes + " Mns");*/
            if (minutes>0) {
                estado_asistencia_codigo = 'T';
                /*console.log('estado_asistencia_codigo_>0 :: '+estado_asistencia_codigo);*/
            } else {
                estado_asistencia_codigo = 'P';
                /*console.log('estado_asistencia_codigo_<0 :: '+estado_asistencia_codigo);*/
            }
        }
    }

    self.BuscarPersonaEvento =  function(search){
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {persona__nombre_completo__icontains: search, persona__persona_persona_documentos__numero__icontains: search};
        filtros['f'] = {tipo_afiliacion__codigo: self.tipo_afiliacion['codigo'], persona__estado: '1'}
        filtros['ordering'] = 'persona__nombre_completo';
        return CONEXP('procesos','afiliaciones/searchformperasist').GET_LIST(filtros).$promise.then(function (r) {
            self.list_estados_asistencia();
            return r;
        });
    }

    self.list_estados_asistencia = function () {
        filtros = {};
        filtros['ordering'] = 'id';
        filtros['f'] = {estado: '1'};
        CONEXP('procesos', 'estadosasistencia/searchform').GET_LIST(filtros).$promise.then(function (r) {
            self.estado_asistencia = r;
            /*console.log('estado_asistencia_codigo :: '+estado_asistencia_codigo);*/
            for (var i = 0; i < self.estado_asistencia.length; i++) {
                if (self.estado_asistencia[i]['codigo'] == estado_asistencia_codigo) {
                    self.obj.estado_asistencia = self.estado_asistencia[i];
                }
            }
        }, function (err) {
        });
    };
    /*self.list_estados_asistencia();*/

    self.register_persona = function(persona){
        self.save_or_update(persona);
    }

    self.save_or_update = function (persona) {
        self.errores = [];
        self.guardando = true;
        self.obj['entidad_id'] = self.entidad_id;
        self.obj['persona_id'] = persona.id;
        self.obj['evento_id'] = evento_id;
        self.obj['persona_asistente'] = 'USU';
        self.obj['estado'] = 1;

        if (self.evento['tipo_asistencia']['codigo'] === 'USUARIOS') {
            self.obj['persona_id'] = persona.id;
            /*self.obj['representante_id'] = null;
            self.obj['persona_asistente'] = '';*/
        } else if(self.evento['tipo_asistencia']['codigo'] === 'SOCIOS'){
            self.obj['persona_id'] = persona.id;
            /*self.obj['representante_id'] = null;*/
            self.obj['persona_asistente'] = 'SOC';
        } else {
        }

            CONEXP('procesos','asistencias/add').POST(self.obj).$promise.then(function (r) {
                self.guardando = false;
                if(r){
                    toastr.success("Registrado correctamente","Registrado");
                    self.afiliacion = null;
                    self.listar();
                    $mdDialog.hide(r);
                }
            }, function (err) {
                self.guardando = false;
                self.errores = err.data;
                toastr.error("No se pudo registrar","Error");
            });
    };


    /*******************************************************************************************************/
    self.add_representante = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/asistencias/app.asistenciaevento.form.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "AsistenciaEventoForm",
                controllerAs: "form",
                templateUrl: 'views/eventos/asistencias/form.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj}
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };

    self.delete = function (event, obj) {
        var confirm = $mdDialog.confirm()
                .title("Eliminar persona")
                .textContent("¿Está seguro de eliminar la asistencia de " + obj.persona['nombre_completo'] + "?")
                .ariaLabel('delete')
                .targetEvent(event)
                .ok('SI')
                .cancel('NO');
        $mdDialog.show(confirm).then(function () {
            CONEXP('procesos','asistencias/delete').DELETE({id: obj.id}).$promise.then(function (r) {
               if(r){
                     toastr.success('Eliminada correctamente', 'Eliminado');
                    self.listar();
                }
            }, function (err) {
                toastr.error('No se pudo eliminar esta persona', 'Error');
            });
        }, function () {
        });
    };

    /**************************************************************************************************************/

    self.new_edit = function (event, obj) {
        $ocLazyLoad.load([
            'js/controllers/asistencias/app.update.estadoasistencia.js'
        ]).then(function () {
            $mdDialog.show({
                controller: "EstadoAsistenciaUpdate",
                controllerAs: "form",
                templateUrl: 'views/eventos/asistencias/estadoasistencia.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: false,
                locals: {obj: obj}
            }).then(function (result) {
                self.listar();
            }, function () {
            });
        });
    };


});