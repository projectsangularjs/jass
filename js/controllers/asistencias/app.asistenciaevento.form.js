angular.module('app').controller('AsistenciaEventoForm', function (obj, $scope, $mdDialog,
    CONEXP, $ocLazyLoad, toastr, config, localStorageService) {

    var self = this;
    self.storage = config.url_api_storage;

    self.obj = {};
    self.cargado = false;
    self.guardando = false;
    self.cargado = true;

    var filtros = {};
    var entidad_id = localStorageService.get('limon_entidad');
    self.estado_asistencia = [];
    var fecha_a_validar;
    var hora_a_validar;
    var estado_asistencia_codigo = '';

    self.ValidarHora = function () {
        fecha_a_validar = obj.fecha;
        /*console.log('fecha_a_validar_form = '+ obj.fecha);*/
        var date_now = moment().format('YYYY-MM-DD');
        var date_evento = moment(fecha_a_validar).format('YYYY-MM-DD');
        var fecha_hoy = new Date(date_now);
        var fecha_evento = new Date(date_evento);
        if (fecha_hoy > fecha_evento) {
            estado_asistencia_codigo = 'T';
            /*console.log('fecha_hoy > fecha_evento :: '+estado_asistencia_codigo);*/
        }  else if (fecha_hoy < fecha_evento) {
            estado_asistencia_codigo = 'P';
            /*console.log('fecha_hoy < fecha_evento :: '+estado_asistencia_codigo);*/
        } else {
            /*console.log('fecha_hoy === fecha_evento');*/
            if (obj.hora_tolerancia) {
                /*console.log('hora_tolerancia == '+obj.hora_tolerancia);*/
                hora_a_validar = moment(obj.hora_tolerancia, 'HH:mm:ss a');
            }else{
                /*console.log('sin tolerancia');
                console.log('hora_inicio == '+obj.hora_inicio);*/
                hora_a_validar = moment(obj.hora_inicio, 'HH:mm:ss a');
            }
            var hora_sistema= moment();
            var duration = moment.duration(hora_sistema.diff(hora_a_validar));
            var hours = parseInt(duration.asHours());
            var minutes = parseInt(duration.asMinutes())-hours*60;
            /*console.log('La diferencia es de ' + hours + " Hrs and " + minutes + " Mns");*/
            if (minutes>0) {
                estado_asistencia_codigo = 'T';
            } else {
                estado_asistencia_codigo = 'P';
            }
        }
    }
    self.ValidarHora();

    self.BuscarRepresentante = function (search) {
        filtros = {};
        filtros['limit'] = 10;
        filtros['q'] = {persona__nombre_completo__icontains: search, persona__persona_persona_documentos__numero__icontains: search};
        filtros['f'] = {persona__estado: '1'}
        return CONEXP('procesos','representantes/searchformrepreasist').GET_LIST(filtros).$promise.then(function (r) {
            return r;
        });
    };

    self.list_estados_asistencia = function () {
        filtros = {};
        filtros['ordering'] = 'id';
        filtros['f'] = {estado: '1'};
        CONEXP('procesos', 'estadosasistencia/searchform').GET_LIST(filtros).$promise.then(function (r) {
            self.estado_asistencia = r;
            for (var i = 0; i < self.estado_asistencia.length; i++) {
                /*console.log('estado_asistencia_codigo = '+estado_asistencia_codigo);*/
                if (self.estado_asistencia[i]['codigo'] == estado_asistencia_codigo) {
                    self.obj.estado_asistencia = self.estado_asistencia[i];
                }
            }
        }, function (err) {
        });
    };
    self.list_estados_asistencia();

    self.change_representante = function(persona_id){
        /*console.log('socio = ' + persona_id);*/
        self.obj['persona_id'] = persona_id;
        self.obj['persona_asistente'] = 'REP';
    };

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.register_representante = function () {
        self.guardando = true;
        self.obj['entidad_id'] = entidad_id;
        self.obj['evento_id'] = obj.id;
        self.obj['estado'] = 1;

        CONEXP('procesos','asistencias/add_repre').POST(self.obj).$promise.then(function (r) {
            self.guardando = false;
            if(r){
                toastr.success("Registrado correctamente","Registrado");
                 $mdDialog.hide(r);
            }
        }, function (err) {
            self.guardando = false;
            self.errores = err.data;
            toastr.error("No se pudo registrar","Error");
        });
    };

});