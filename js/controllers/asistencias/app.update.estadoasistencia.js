angular.module('app').controller('EstadoAsistenciaUpdate', function (obj, $scope, $mdDialog,
    CONEXP, $ocLazyLoad, toastr, config, localStorageService) {

    var self = this;
    self.storage = config.url_api_storage;

    self.obj = {};
    self.guardando = false;
    self.cargado = true;

    self.dni = "";
    self.estado_asistencia = [];
    var filtros = {};
    self.obj['id'] = obj.id;
    self.obj['persona'] = obj.persona;

    if (obj.representante) {
        self.obj['representante'] = obj.representante;
        if(obj.representante.persona_persona_documentos.length > 0){
            for (var i = 0; i < obj.representante.persona_persona_documentos.length; i++) {
                var tipo_documento = obj.representante.persona_persona_documentos[i]['tipo_documento'];
                if(tipo_documento['nombre'] === 'DNI' || tipo_documento['nombre'] === 'dni'){
                    self.dni = obj.representante.persona_persona_documentos[i]['numero'];
                }
            }
        }
    } else {
        if(obj.persona.persona_persona_documentos.length > 0){
            for (var i = 0; i < obj.persona.persona_persona_documentos.length; i++) {
                var tipo_documento = obj.persona.persona_persona_documentos[i]['tipo_documento'];
                if(tipo_documento['nombre'] === 'DNI' || tipo_documento['nombre'] === 'dni'){
                    self.dni = obj.persona.persona_persona_documentos[i]['numero'];
                }
            }
        }
    }

    if (obj.comentario !== null) {
        self.obj['comentario'] = obj.comentario;
    }

    self.list_estados_asistencia = function () {
        filtros = {};
        filtros['ordering'] = 'id';
        filtros['f'] = {estado: '1'};
        CONEXP('procesos', 'estadosasistencia/searchform').GET_LIST(filtros).$promise.then(function (r) {
            self.estado_asistencia = r;
            for (var i = 0; i < self.estado_asistencia.length; i++) {
                if (self.estado_asistencia[i]['codigo'] == obj.estado_asistencia.codigo) {
                    self.obj.estado_asistencia = self.estado_asistencia[i];
                }
            }
        }, function (err) {
        });
    };
    self.list_estados_asistencia();

    self.cancel = function () {
        $mdDialog.cancel();
    };

    self.update_estado_asistencia = function () {
        self.guardando = true;
        if(self.obj['estado_asistencia']['codigo'] !== 'C'){
            self.obj['comentario'] = '';
        }

        CONEXP('procesos','asistencias/updateestaasist').PUT(self.obj).$promise.then(function (r) {
            self.guardando = false;
            if(r){
                toastr.success("Registrado correctamente","Registrado");
                 $mdDialog.hide(r);
            }
        }, function (err) {
            self.guardando = false;
            self.errores = err.data;
            toastr.error("No se pudo registrar","Error");
        });
    };

});