angular.module('app').controller('AppCtrlReportes', function ($scope, config, data_app,
        $document, $window, $state, $timeout, $log, localStorageService, $rootScope, $http,
        CONEX) {

    $scope.$on('ocLazyLoad.moduleLoaded', function (e, params) {
    });

    $scope.$on('ocLazyLoad.componentLoaded', function (e, params) {
    });

    $scope.$on('ocLazyLoad.fileLoaded', function (e, file) {
    });

    $rootScope.state = $state;
    $rootScope.Indeterminado = true;
    var data_user = localStorageService.get('data_user');
    $scope.data_user = data_user;

    $scope.IniciarSesion = function () {
        $window.location = 'login/';
    };
    
    $scope.app = data_app;

});