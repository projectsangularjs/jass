
var data_app = {
    name: 'Presente 7',
    eslogan: 'Proyecto de procesos comerciales',
    version: '1.0.1',
    setting: {
        theme: {
            primary: 'indigo',
            accent: 'purple',
            warn: 'amber'
        },
        asideFolded: false
    },
    search: {
        content: '',
        show: false
    }
};

angular.module('app').value('data_app', data_app);

angular.module('app').factory('BearerAuth2Int', function ($window, config, $q, localStorageService, $injector) {
    var temp = null;

    var Bearer = function (configs) {
        configs.headers = configs.headers || {};

        if (localStorageService.isSupported) {
            var authLL = localStorageService.get('limon_acctok');
            if (authLL) {
                configs.headers.Authorization = 'Bearer ' + authLL;
            } else {
                $window.location = config.url_login;
            }
        }
        return configs;
    };

    var BearerError = function (rejection) {
        var deferred = $q.defer();
        if (rejection.status == 401 || rejection.status == 403) {
            $window.location = config.url_login;
        }else{
            deferred.reject(rejection);
        }
        return deferred.promise;
    };

    return {
        request: Bearer,
        responseError: BearerError,
    };
});


angular.module('app').config(function ($resourceProvider, $httpProvider) {

    $httpProvider.interceptors.push('BearerAuth2Int');
    $httpProvider.useApplyAsync(true);
    $resourceProvider.defaults.stripTrailingSlashes = false;
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

    $resourceProvider.defaults.actions = {
        POST: {method: 'POST'},
        POST_LIST: {method: 'POST', isArray: true},
        GET: {method: 'GET', isArray: false},
        GET_LIST: {method: 'GET', isArray: true},
        PUT: { method: 'PUT' },
        PUT_LIST: { method: 'PUT', isArray: true},
        DELETE: { method: 'DELETE' },
        DELETE_LIST: { method: 'DELETE', isArray: true},
        POST_FILE: {method: 'POST', isArray: false,
            headers: {
                "Content-Type": undefined
            },
            transformRequest: transformRequest
        },
        POST_OAUTH: {method: 'POST', isArray: false,
            headers: {
                "Content-Type": undefined
            },
            transformRequest: transformRequest
        },
        PATCH: {method: 'PATCH', isArray: false},
        PUT_FILE: {method: 'PUT', isArray: false,
            headers: {
                "Content-Type": undefined
            },
            transformRequest: transformRequest
        },
    };

    function transformRequest(data) {
        if (undefined === data)
            return data;
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
            if (value instanceof FileList) {
                if (value.length === 1)
                    formData.append(key, value[0]);
                else {
                    angular.foreach(value, function (file, index) {
                        formData.append(key + '_' + index, file);
                    });
                }
            } else {
                formData.append(key, value);
            }
        });
        return formData;
    }
});

angular.module('app').factory('CONEX', function ($resource, config) {
    return function (app, recurso) {
        return $resource(config['url_api_'+app] + recurso+'/:id/', {'id':'@id'});
    };
});
angular.module('app').factory('CONEXP', function ($resource, config) {
    return function (app, recurso) {
        return $resource(config['url_api_'+app] + recurso+'/');
    };
});

angular.module('app').config(function($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function(date) {
       return moment(date).format('DD/MM/YYYY');
    };
});

angular.module('dialogDemo3', ['ngMaterial'])
  .config(function ($mdThemingProvider) {
    $mdThemingProvider.theme('red')
      .primaryPalette('red');

    $mdThemingProvider.theme('blue')
      .primaryPalette('blue');

  });


angular.module('app').factory('Excel',function($window){
    var uri='data:application/vnd.ms-excel;base64,',
        template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
        format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
    return {
        tableToExcel:function(tableId,worksheetName){
            var table=$(tableId),
                ctx={worksheet:worksheetName,table:table.html()},
                href=uri+base64(format(template,ctx));
            return href;
        }
    };
});