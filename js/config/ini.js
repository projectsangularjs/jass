var oauth2 = {
    name_app:'WebAngulajs',
}

var config = {
    url_api_segur: 'http://localhost:8000/api/segur/',
    url_api_comun: 'http://localhost:8000/api/comun/',
    url_api_procesos: 'http://localhost:8000/api/procesos/',
    url_api_caja: 'http://localhost:8000/api/caja/',
    url_api_aouth2: 'http://localhost:8000/outh2limon/',
    url_api_storage: 'http://localhost:8000',
    oauth2: oauth2,
    url_login:'http://localhost:8080/jass/start.html',
    url_web:'http://localhost:8080/jass/',
};

angular.module('app').value('config', config).run(run_index);

function run_index($rootScope, $state, $q, $http) {
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
    $rootScope.tokenSocket = $q.defer();
    $rootScope.$userPromise = $q.defer();
    $rootScope.$state = $state;
};

var data_app = {
    name: 'Limon Live',
    eslogan: '....',
    version: '1.0',
    setting: {
        theme: {
            primary: 'blue',
            accent: 'purple',
            warn: 'amber'
        },
        asideFolded: false
    },
    search: {
        content: '',
        show: false
    }
};

angular.module('app').value('data_app', data_app);