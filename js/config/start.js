
angular.module('app').config(function ($resourceProvider, $httpProvider) {

    // $httpProvider.useApplyAsync(true);
    $resourceProvider.defaults.stripTrailingSlashes = false;
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

    $resourceProvider.defaults.actions = {
        POST_AUTH: {method: 'POST', isArray: false,
            headers: {
                "Content-Type": undefined
            },
            transformRequest: transformRequest
        },
        POST: {method: 'POST'},
    };

    function transformRequest(data) {
        if (undefined === data)
            return data;
        var formData = new FormData();
        angular.forEach(data, function (value, key) {
            if (value instanceof FileList) {
                if (value.length === 1)
                    formData.append(key, value[0]);
                else {
                    angular.foreach(value, function (file, index) {
                        formData.append(key + '_' + index, file);
                    });
                }
            } else {
                formData.append(key, value);
            }
        });
        return formData;
    }
});

angular.module('app').factory('CONEXO', function ($resource, config) {
    return function (recurso) {
        return $resource(config['url_api_aouth2'] + recurso);
    };
});

angular.module('app').factory('CONEXS', function ($resource, config) {
    return function (recurso) {
        return $resource(config['url_api_segur'] + recurso);
    };
});

angular.module('app').factory('Oauth2', function($http, $q, config, localStorageService, 
    CONEXO,CONEXS, $interval) {

    var oauth2_login = function(data){
        var deferred = $q.defer();
        CONEXS('functions/clienteapp/').POST(config.oauth2).$promise.then(function (r) {
                data['grant_type'] = r.grant_type;
                data['client_id'] = r.client_id;
                data['client_secret'] = r.client_secret;
                CONEXO('token/').POST_AUTH(data).$promise.then(function (res) {
                        if (localStorageService.isSupported) {
                            localStorageService.set('limon_acctok', res.access_token);
                        }
                        deferred.resolve(r);
                    }, function (err) {
                        deferred.reject(err);
                });
            }, function (err) {
                deferred.reject(err);
            });

        // var result = {};
        // data['grant_type'] = config.oauth2.grant_type;
        // data['client_id'] = config.oauth2.client_id;
        // data['client_secret'] = config.oauth2.client_secret;
        
        return deferred.promise;
        
    };

    return {
        oauth2_login: oauth2_login
    };

});