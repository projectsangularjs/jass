angular.module('app').config(function ($mdThemingProvider) {
    $mdThemingProvider.definePalette('amazingPaletteName', {
        '50': 'ffebee',
        '100': 'ffcdd2',
        '200': 'ef9a9a',
        '300': 'e57373',
        '400': 'ef5350',
        '500': 'f44336',
        '600': 'e53935',
        '700': 'd32f2f',
        '800': 'c62828',
        '900': 'b71c1c',
        'A100': 'ff8a80',
        'A200': 'ff5252',
        'A400': 'ff1744',
        'A700': 'd50000',
        'contrastDefaultColor': 'light',
        'contrastDarkColors': ['50', '100',
            '200', '300', '400', 'A100'],
        'contrastLightColors': undefined
    });

    $mdThemingProvider.definePalette('erp-color', {
        '50': 'e8d0dd',
        '100': 'ddb9cc',
        '200': 'd2a1bb',
        '300': 'c78aaa',
        '400': 'bb7399',
        '500': 'b05b88',
        '600': 'a54477',
        '700': '9a2d66',
        '800': '8f1656', // cambia el toolbar main
        '900': '80134d', // La parte superior del menú
        'A100': 'F1B3EC',
        'A200': 'E572DD',
        'A400': '9D1D93',
        'A700': '4F0F4A',
        'contrastDefaultColor': 'light',
        'contrastDarkColors': ['50', '100', '200', '300', '400', 'A100'],
        'contrastLightColors': undefined
    });

    $mdThemingProvider.theme('default').primaryPalette('amazingPaletteName', {'default': '900'});

    $mdThemingProvider.theme('docs-dark').primaryPalette('blue').dark();
});

angular.module('app').config(function ($mdThemingProvider, /*ssSideNavSectionsProvider*/) {

    $mdThemingProvider.theme('Indigo')
            .primaryPalette('indigo');

    $mdThemingProvider.theme('Green')
            .primaryPalette('green');

    $mdThemingProvider.theme('Purple')
            .primaryPalette('purple');

    $mdThemingProvider.theme('Red')
            .primaryPalette('red');

    $mdThemingProvider.theme('DeepPurple')
            .primaryPalette('deep-purple');

    $mdThemingProvider.theme('Indigo')
            .primaryPalette('indigo');

    $mdThemingProvider.theme('Blue')
            .primaryPalette('blue');

    $mdThemingProvider.theme('LightBlue')
            .primaryPalette('light-blue');

    $mdThemingProvider.theme('Cyan')
            .primaryPalette('cyan');

    $mdThemingProvider.theme('Teal')
            .primaryPalette('teal');

    $mdThemingProvider.theme('LightGreen')
            .primaryPalette('light-green');

    $mdThemingProvider.theme('Lime')
            .primaryPalette('lime');

    $mdThemingProvider.theme('Yellow')
            .primaryPalette('yellow');

    $mdThemingProvider.theme('Amber')
            .primaryPalette('amber');

    $mdThemingProvider.theme('Orange')
            .primaryPalette('orange');

    $mdThemingProvider.theme('DeepOrange')
            .primaryPalette('deep-orange');

    $mdThemingProvider.theme('Brown')
            .primaryPalette('brown');

    $mdThemingProvider.theme('Grey')
            .primaryPalette('grey');

    $mdThemingProvider.theme('BlueGrey')
            .primaryPalette('blue-grey');

    $mdThemingProvider.setDefaultTheme('Blue');
    $mdThemingProvider.alwaysWatchTheme(true);
});

angular.module('app').config(function ($mdToastProvider) {
    $mdToastProvider.addPreset('deleteSuccess', {
        options: function () {
            return {
                template:
                        '<md-toast>' +
                        '<div class="md-toast-content">' +
                        '<ng-md-icon icon="delete" aria-label="s" style="margin: 0px; margin-right: 2px;"></ng-md-icon> Eliminado correctamente' +
                        '</div>' +
                        '</md-toast>',
                controllerAs: 'toast',
                bindToController: true
            };
        }
    });
    $mdToastProvider.addPreset('updateSuccess', {
        options: function () {
            return {
                template:
                        '<md-toast>' +
                        '<div class="md-toast-content">' +
                        '<ng-md-icon icon="save" aria-label="s" style="margin: 0px; margin-right: 2px;"></ng-md-icon> Datos actualizados correctamente' +
                        '</div>' +
                        '</md-toast>',
                controllerAs: 'toast',
                bindToController: true
            };
        }
    });
    $mdToastProvider.addPreset('addListReprod', {
        options: function () {
            return {
                template:
                        '<md-toast>' +
                        '<div class="md-toast-content">' +
                        '<ng-md-icon icon="playlist_add" aria-label="s" style="margin: 0px; margin-right: 2px;"></ng-md-icon> Agregado correctamente a la lista' +
                        '</div>' +
                        '</md-toast>',
                controllerAs: 'toast',
                bindToController: true
            };
        }
    });
    $mdToastProvider.addPreset('ErrorAddListReprod', {
        options: function () {
            return {
                template:
                        '<md-toast>' +
                        '<div class="md-toast-content" style="background: red; color: white;">' +
                        '<ng-md-icon icon="audiotrack" aria-label="s" style="margin: 0px; margin-right: 2px;"></ng-md-icon> No se han encontrado recursos' +
                        '</div>' +
                        '</md-toast>',
                controllerAs: 'toast',
                bindToController: true
            };
        }
    });
});
