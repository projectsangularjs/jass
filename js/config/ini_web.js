var oauth2 = {
    name_app:'WebAngulajs',
}

var config = {
    url_api_segur: 'http://django.misionlive.com/api/segur/',
    url_api_comun: 'http://django.misionlive.com/api/comun/',
    url_api_procesos: 'http://django.misionlive.com/api/procesos/',
    url_api_caja: 'http://django.misionlive.com/api/caja/',
    url_api_aouth2: 'http://django.misionlive.com/outh2limon/',
    url_api_storage: 'http://django.misionlive.com/',
    oauth2: oauth2,

    url_login:'http://djangoweb.misionlive.com/start.html',
    url_web:'http://djangoweb.misionlive.com/',
};

angular.module('app').value('config', config).run(run_index);

function run_index($rootScope, $state, $q, $http) {
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
    $rootScope.tokenSocket = $q.defer();
    $rootScope.$userPromise = $q.defer();
    $rootScope.$state = $state;
};

var data_app = {
    name: 'Limon Live',
    eslogan: '....',
    version: '1.0',
    setting: {
        theme: {
            primary: 'blue',
            accent: 'purple',
            warn: 'amber'
        },
        asideFolded: false
    },
    search: {
        content: '',
        show: false
    }
};

angular.module('app').value('data_app', data_app);