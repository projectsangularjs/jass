ngDevhres.directive('pqmLoading', function ()
{
    return{
        restrict: 'E',
        template: '<div class="cargando_fondo" ng-show="indeterminado"></div>' +
                '<div class="cargando_valores" ng-show="indeterminado">' +
                '<center><p><md-progress-circular class="md-primary" md-mode="indeterminate" md-diameter="40"></md-progress-circular>' +
                '</p></center>' +
                '</div>',
        scope: {
            indeterminado: "="
        },
        link: function (scope) {
        }
    };
});

ngDevhres.directive('pqmLoadingGeneralModal', function ()
{
    return{
        restrict: 'E',
        template: '<div class="cargando_fondo_modal" ng-show="indeterminado"></div>' +
                '<div class="cargando_valores_modal" ng-show="indeterminado">' +
                '<center><p><md-progress-circular class="md-primary" md-mode="indeterminate"  md-diameter="70px"></md-progress-circular>' +
                '</p><span class="md-body-1" ng-show="mensaje" style="color: white; background: #2196F3; padding: 6px; border-radius: 6px;">{{mensaje}}</span></center>' +
                '</div>',
        scope: {
            indeterminado: "=",
            mensaje: "="
        },
        link: function (scope) {
        }
    };
});


ngDevhres.directive('onKeyup', function() {
    return function(scope, elm, attrs) {
      var allowedKeys = scope.$eval(attrs.keys);
      elm.bind('keydown', function(evt) {           
        angular.forEach(allowedKeys, function(key) {
          if (key == evt.which) {
             evt.preventDefault(); // Doesn't work at all
             window.stop(); // Works in all browsers but IE    
             document.execCommand("Stop"); // Works in IE
             return false; // Don't even know why it's here. Does nothing.                     
          }
        });
      });
    };
});